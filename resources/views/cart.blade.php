@extends('layouts.app')


{{-- @section('title', 'Shopping Cart')
 --}}


@section('content')




            {{-- @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
            {{-- @component('components.breadcrumbs')
                <a href="#">Home</a>
                <i class="fa fa-chevron-right breadcrumb-separator"></i>
                <span>Shopping Cart</span>
            @endcomponent

 --}}




   <!--================Cart Area =================-->


<div class="container">
        <a class="btn btn-info" href="{{ URL::previous() }}">back</a>
        @if (Cart::count() > 0)
        <section class="cart_area">
        <div class="cart_inner">
                <div class="table-responsive">
        <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                  
                  </tr>
                </thead>
                <tbody>
                        @foreach (Cart::content() as $item)

                      
                  <tr>
                    <td> <p>{{ $item->name }}</p></td>
                    <td>  <h5>{{ presentPrice($item->price) }}</h5></td>
                    <td>  <div class="product_count">

                            <select class=" quantity" data-id="{{ $item->rowId }}" data-productQuantity="{{ $item->model->quantity }}">
                                    @for ($i = 1; $i < $item->model->quantity + 1 ; $i++)
                                        <option {{ $item->qty == $i ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                            </select>
                      </div></td>
                    <td> <h4>{{ presentPrice($item->subtotal) }} </h4></td>
                    
<td>

        <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST" class="pull-right">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i>Remove</button>
            </form>
</td>

                  </tr>
                  @endforeach


                </tbody>
              </table>
        </div>

        <hr>
        <div class="row">
<div class="col-md-6">
    Subtotal:    ₦ {{   Cart::subtotal() }}
</div>

<div class="col-md-6">
        Tax :     {{ presentPrice($newTax) }}
    </div>

        </div>
<hr>
        <div class="row">
<div class="col-md-3">



        @if (Session()->has('coupon'))
        Code ({{$code}})
        <form action="{{ route('coupon.destroy') }}" method="POST" style="display:block">
            {{ csrf_field() }}
            {{ method_field('delete') }}
            <button type="submit" class="btn btn-danger" style="font-size:14px;">Remove</button>
        </form> </th>      @endif

</div>

<div class="col-md-3">

        @if (session()->has('coupon'))

        Discount - {{ presentPrice($value) }} <br>&nbsp;<br>

        {{-- Total: {{ presentPrice($newTotal) }} --}}




      @endif
    </div>

    <div class="col-md-6">

            <form action="{{ route('coupon.store') }}" method="POST">
                    {{ csrf_field() }}
                      <div class="input-group">

                          <input type="text" class="form-control" name="coupon_code" id="coupon_code">

                          <span class="input-roup-btn" style="margin-top:10px">

    <button class="main_btn" type="submit">Apply</button>

      </span>
                      </div>
                      <!-- /input-group -->
                  </form>

        </div>

        </div>

        <hr>
        <div class="row">
<div class="col-md-5">


</div>

<div class="col-md-4">
        <h4> Total : <b>{{ presentPrice($newTotal) }}</b></h4>

    </div>

    <div class="col-md-3">
            {{-- <script type="text/javascript">

                function yesnoCheck() {

                    if (document.getElementById('yesCheck').checked) {
                        document.getElementById('ifYes').style.display = 'block';
                    }
                    else document.getElementById('ifYes').style.display = 'none';

                }

                </script>
    <form action="{{ route('checkout.delivery') }}" method="POST">
            {{ csrf_field() }}

                 <input type="radio"  onclick="javascript:yesnoCheck();" name="yesno" id="yesCheck" value="1" checked > Deliver <br>
                 <input type="radio" onclick="javascript:yesnoCheck();" name="yesno" id="noCheck" value="0"> Dont Deliver <br>
                    <div id="ifYes" style="display:block">
                            <h4> +Delivery Charge:  ₦ {{  $shiping }}</h4>
                    </div>

                </form> --}}
        </div>

        </div>
        <hr>
<br><br><br>
        <div class="row">

                    <div class="col-md-3">
                        @if (!empty($urllink))
                        <a class="btn btn-warning" href="{{$urllink}}">Continue Shopping</a>

                        @else
                        <a class="btn btn-warning" href="{{URL::previous()}}">Continue Shopping</a>
                        @endif
                       

                    </div>
                    <div class="col-md-6">

                    </div>

                        <div class="col-md-3">

                                <a class="btn btn-success"  href="{{ route('checkout.index') }}">Proceed to checkout</a>

                            </div>

                    


                        </div>

                      <!-- /input-group -->


    </div>
        </section>
              @else
              <br><br /><br /><br />
                   @if (!empty($urllink))
                        <a class="btn btn-warning" href="{{$urllink}}">Back</a>

                        @else
                        <a class="btn btn-warning" href="{{URL::previous()}}">Back</a>
                        @endif
           
              
              <br />
                    <h3 class="text-center">No items in Cart!</h3>
                    <div class="spacer"></div><br /><br />
                  {{-- <a href="{{ route('shop.index') }}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Continue shopping</a> --}}

              <br /><br />
                    <div class="spacer"></div>


        @endif
</div>


@endsection
<!-- important for ajax quantity to work-->


    <!-- important -->
@section('extra-js')
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}

<script src="{{ asset('js/app.js') }}"></script>

<script>
        (function(){
            const classname = document.querySelectorAll('.quantity')

            Array.from(classname).forEach(function(element) {
                element.addEventListener('change', function() {

                    const id = element.getAttribute('data-id')
                    const productQuantity = element.getAttribute('data-productQuantity')

                    axios.patch(`/cart/${id}`, {

                        quantity: this.value,
                        productQuantity: productQuantity
                    })
                    .then(function (response) {
                        console.log(response);
                        window.location.href = '{{ route('cart.index') }}'
                    })
                    .catch(function (error) {
                        // console.log(error);
                        window.location.href = '{{ route('cart.index') }}'
                    });
                })
            })
        })();
    </script>

@endsection
