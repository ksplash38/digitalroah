
@foreach($comments as $comment)
<div class="display-comment">
      
         <strong>{{ $comment->user->name }}</strong>
            <p>{{ $comment->body }} - <small style="font-size:8px">{{$comment->created_at->diffForHumans()}}</small></p>
       
    <a href="" id="reply"></a>
    <form method="post" action="{{ route('reply.add') }}">
        @csrf
        <div class="form-group">
            <input type="text" name="comment_body" class="form-control" />
            <input type="hidden" name="post_id" value="{{ $post_id }}" />
            <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
        </div>
        
        <div class="form-group">
            <input type="submit" class="btn btn-warning" value="Reply Comment" />

        </div>

        {{-- {!!Form::open(['action' => ['CommentController@deleteWithReplies', $news->id], 'method' => 'POST','onsubmit' => 'return ConfirmDelete()'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger btn-md pull-right'])}}
        {!!Form::close()!!} --}}
    </form>
    @include('news-folder._comment_replies', ['comments' => $comment->replies])
</div>
@endforeach
