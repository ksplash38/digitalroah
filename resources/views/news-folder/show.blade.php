@extends('layouts.app')
<style>
    .display-comment .display-comment {
        margin-left: 40px
    }

    .video-container {
position: relative;
padding-bottom: 25%;
padding-top: 30px; height: 0; overflow: hidden;
}

.video-container iframe,
.video-container object,
.video-container embed {
position: absolute;
top: 0;
left: 0;
width: 60%;
height: 100%;
}
.advert {
  position: relative;
  width: 100%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.advert:hover .image {
  opacity: 0.3;
  filter: blur(1px);
  -webkit-filter: blur(1px);
}

.advert:hover .middle {
  opacity: 1;
}

.text {
  background-color: #46dbb7;
  color: white;
  font-size: 16px;
  padding: 10px 28px;
}

</style>

@section('og')
<meta property="og:url"     itemprop="url"      content="{{Request::url()}}"/>
<meta property="og:site_name" content="Digitalroah services">
<meta property="og:type"   itemprop="type"         content="website" />
<meta property="og:title"   itemprop="title"        content="{{$news->title}}" />
<meta property="og:description" itemprop="description"    content="{{$news->body}}" />
@if (!empty($news->image))

<meta property="og:image"   itemprop="image"      content="{{ productImages($news->image) }}" />
                      {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
@endif
@endsection



@section('content')


<br><br>
	<!-- Page Header -->
    <div id="page-header">
        <!-- section background -->
        <div class="section-bg" style="background-image: url({{asset('charity/img/background-2.jpg')}});"></div>
        <!-- /section background -->

        <!-- page header content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-content">
                            <h1>News & Events</h1>
                            <div class="row">
                        <div class="col-md-12">

                                <ul class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    /
                                  <a href="{{URL::previous()}}"> <li > List </li></a> /
                                    <li class="active"> News & Events</li>
                                </ul>
                        </div>
{{--
                        <div class="col-md-8">

                                <ul class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    /
                                    <li class="active">News & Events</li>
                                </ul> --}}
                        </div>
                    </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /page header content -->
    </div>
    <!-- /Page Header -->
</header>




<!-- start Banner Area -->
{{-- <section class="home-banner-area relative banner-area">
    <div class="container-fluid">
        <div class="row  d-flex align-items-center justify-content-center">
            <div class="header-left col-lg-5 col-md-6 ">
                <h6 class="text-white ">the Royal Essence of Journey</h6>
                <h2>Blog details</h2>
                <p class="pt-10 pb-20">
                    If you are looking at blank cassettes on the web, you may be very confused at the.
                </p>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-8 header-right">
                <div class="">
                    <img class="img-fluid w-100" src="{{asset('theme/img/banner-img.jpg')}}" alt="">
                </div>
                <div class="form-wrap about-content">
                    <p class="text-white m-0">
                        <span class="box">
                            <a href="index.html">Home </a>
                            <span class="lnr lnr-arrow-right"></span>
                            <a href="blog-home.html">Blog </a>
                            <span class="lnr lnr-arrow-right"></span>
                            <a href="about.html">Blog details</a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- End Banner Area -->

<!--================Blog Area =================-->
{{-- <section class="blog_area single-post-area section-gap"> --}}


        <section class="blog_area single-post-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 posts-list">
                            <div class="single-post row">
                                
                                    <div class="col-lg-12">
                                            <div class="feature-img">
                                                    @if (!empty($news->image))
                                      <img src="{{ productImage( $news->image) }}" alt="" style="  width:40% " />
                                      @endif
                                                    @if (!empty($news->images))
                                                    @foreach (json_decode($news->images, true) as $image)
                                                    {{-- <img src="{{ productImages($image) }}" alt="" style="width:40%"> --}}
                                                    <a href="{{ productImage( $image) }}" class="fancybox" title="">   <img src="{{ productImage($image) }}" alt="{{$news->title}}" style="  width:40% "></a>
                    
                                                                          {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
                                                                  @endforeach
                                                                            @endif
                    
                                                                            @if (!empty($news->link))
                                                                            <div class="video-container">{!!$news->link!!}</div>
                                                                            @endif
                    
                                                                            <br><br>
                    @if (!empty($news->audio))
                    <audio controls>
                            <?php $file = (json_decode($news->audio))[0]->download_link; ?>
                            <source src=" /storage/{{ $file }}" type="audio/ogg">
                            <source src=" /storage/{{ $file }}" type="audio/mpeg">
                          Your browser does not support the audio element.
                          </audio>
                    @endif
                                                                     
                    
                                                                                  <br><br>  
                                                                            <div class="sharethis-inline-share-buttons"></div>
                                            </div>
                                        </div>



                          <div class="col-lg-3  col-md-3">
                        <div class="blog_info text-right">
                            {{-- <div class="post_tag">
                                <a href="#">Food,</a>
                                <a class="active" href="#">Technology,</a>
                                <a href="#">Politics,</a>
                                <a href="#">Lifestyle</a>
                            </div> --}}
                            <ul class="blog_meta list">
                                <li><a href="#">DigitalRoah<i class="lnr lnr-user"></i></a></li>
                                <li><a href="#"> {{$news->created_at->diffForHumans()}} <i class="fa fa-clock-o" aria-hidden="true"></i> </a></li>


                                {{-- <li><a href="#">1.2M Views<i class="lnr lnr-eye"></i></a></li> --}}
                                <li><a href="#"> {{$news->comments->count()}} Comments <i class="lnr lnr-bubble"></i></a></li>
                            </ul>

                            {{-- <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-github"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul> --}}
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 blog_details">
                        <h2>{{$news->title}}</h2>
                        <p class="excert">
                                {{$news->body}}
                        </p>
                        @if (!empty($news->eventdate))
                        <i class="lnr lnr-calendar-full"></i> Event Starts:  {{$news->eventdate}} {{$news->eventtime}}
                        @endif

                    </div>
                               
                    <div class="col-lg-12">

                            <div class="quotes">
    
                                <h3 class="text-center text-danger">IMPORTANT ANNOUNCEMENT</h3>
                                MCSE boot camps have its supporters and its detractors. Some people do not understand
                                why you should have to spend money on boot camp when you can get the MCSE study
                                materials yourself at a fraction of the camp price. However, who has the willpower to
                                actually sit through a self-imposed MCSE training.
                            </div>
                       
                        </div>
                            </div>
                            <div class="navigation-area">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                                            {{-- <div class="thumb">
                                                <a href="#"><img class="img-fluid" src="img/blog/prev.jpg" alt=""></a>
                                            </div> --}}
                                            {{-- <div class="arrow">
                                                <a href="#"><span class="lnr text-white lnr-arrow-left"></span> </a>
                                            </div> --}}
                                            <div class="detials">
                
                                            @if ($previous)
                                            <a href=" {{ route('news.show', $previous->randomid) }} ">
                                            <p>Prev Post</p>
                                            <h4> <small>{{$previous->title}} </small>  </h4>
                                            </a>
                                            @endif
                                            </div>
                
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                
                                            <div class="detials">
                                            @if ($next)
                                            <a href=" {{ route('news.show', $next->randomid) }} ">
                                            <p>Next Post</p>
                                            <h4> <small>{{$next->title}}</small>  </h4>
                                            </a>
                                            @endif
                
                                            </div>
                                     
                                        </div>
                                    </div>
                                </div>
                            <div class="comments-ara">
                                    <main id="main" class="col-md-10">
                                    
                                            <!-- article -->
                                            <div class="article event-details">
                                            
                          
                        
                            @if (!empty($news->link))
                            <div class="video-container">{!!$news->link!!}</div>
                            @endif
                        
                            <br><br>
                         
                                                 
                                            
                        
                        
                        
                                                            <div class="card">
                                                                    <h4 class="text-center">{{$news->comments->count()}} Comments  </h4>
                                                                <div class="card-body">
                                                                        <div class="sharethis-inline-reaction-buttons"></div>
                                                @include('news-folder._comment_replies', ['comments' => $news->comments, 'post_id' => $news->id])
                        
                                                <hr />
                        
                        
                        
                        
                        
                                                <h4>Add comment</h4> <small> {{$news->comments->count()}} Comments   </small>
                                                <form method="post" action="{{ route('comment.add') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <input type="text" name="comment_body" class="form-control" style="height:5em" />
                                                        <input type="hidden" name="post_id" value="{{ $news->id }}" />
                        
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="submit" class="btn btn-warning" value="Add Comment" />
                                                    </div>
                                                </form>
                                                                </div></div>
                                                <!-- /article reply form -->
                                            </div>
                                            <!-- /article -->
                                        </main>
                            </div>
                         
                        </div>
                        <div class="col-lg-4">
                            <div class="blog_right_sidebar">
                             
                                    <aside class="single_sidebar_widget popular_post_widget">
                                            <h3 class="widget_title">Recent Posts</h3>
                                            @foreach ($recentposts as $recentpost)
                    
                    
                    
                                            <div class="media post_item">
                                                    @if (!empty($recentpost->image))
                    
                                                    <img src="{{ productImages($recentpost->image) }}" alt="" style="width:20%">
                    
                                                                          {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
                    
                                                                            @endif
                    
                                                <div class="media-body">
                                                    <a href="{{ route('news.show', $recentpost->randomid) }}">
                                                        <h3>{{$recentpost->title}}</h3>
                                                    </a>
                                                    <small>{{$recentpost->created_at->diffForHumans()}}</small>
                                                </div>
                                            </div>
                                            @endforeach
                                      
                                            <div class="br"></div>
                                        </aside>
                         
                                <aside class="single_sidebar_widget popular_post_widget">
                                        <h3 class="widget_title">Might Also Like</h3>
                                        @foreach ($mightAlsoLike as $product)
            
            
            
                                        <div class="media post_item">
                                                @if (!empty($product->image))
                                                <img src="{{ Voyager::image($product->image) }}" alt="" style="width:20%">
                                                {{-- <img src="{{ productImages($recentpost->image) }}" alt="" style="width:20%"> --}}
            
                                                                      {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
            
                                                                        @endif
            
                                            <div class="media-body">
                                                <a href="{{ route('news.show', $product->randomid) }}">
                                                    <h3>{{$product->title}}</h3>
                                                </a>
                                                <small>{{$product->created_at->toDayDateTimeString()}}</small>
                                            </div>
                                        </div>
                                        @endforeach
             
                                        <div class="br"></div>
                                    </aside>

                                    <aside class="single_sidebar_widget ads_widget">
                                            <h4 class="widget_title">ADVERTISEMENT</h4>
                                            <div class="advert">
                                                    @if (!empty($adverts->image))
                                                    <img src="{{ Voyager::image($adverts->image) }}" class="image" alt="" style="width:100%"  >
                                                    {{-- <img src="{{ productImages($recentpost->image) }}" alt="" style="width:20%"> --}}
                    
                                                                          {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
                    
                                                                            @endif
                                                    <div class="middle">
                                                      <div class="text">VIEW</div>
                                                    </div>
                                                  </div>
                                        <div class="br"></div>
                                    </aside>
                                {{-- <aside class="single-sidebar-widget newsletter_widget">
                                    <h4 class="widget_title">Newsletter</h4>
                                    <p>
                                        Here, I focus on a range of items and features that we use in life without
                                        giving them a second thought.
                                    </p>
                                    <div class="form-group d-flex flex-row">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email"
                                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'">
                                        </div>
                                        <a href="#" class="bbtns">Subcribe</a>
                                    </div>
                                    <p class="text-bottom">You can unsubscribe at any time</p>
                                    <div class="br"></div>
                                </aside> --}}
                                {{-- <aside class="single-sidebar-widget tag_cloud_widget">
                                    <h4 class="widget_title">Tag Clouds</h4>
                                    <ul class="list">
                                        <li><a href="#">Technology</a></li>
                                        <li><a href="#">Fashion</a></li>
                                        <li><a href="#">Architecture</a></li>
                                        <li><a href="#">Fashion</a></li>
                                        <li><a href="#">Food</a></li>
                                        <li><a href="#">Technology</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">Art</a></li>
                                        <li><a href="#">Adventure</a></li>
                                        <li><a href="#">Food</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">Adventure</a></li>
                                    </ul>
                                </aside> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--================Blog Area =================-->


 








<link rel="stylesheet"   href="{{asset("fancybox/fancybox-2.1.7/source/jquery.fancybox.css")}}" type="text/css" media="screen" />


<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ccfcb1d84760c0019807a78&product=inline-share-buttons"></script>


@endsection



@section('fancyboxscript')
<script type="text/javascript" src="{{asset("fancybox/fancybox-2.1.7/source/jquery.fancybox.pack.js")}}"></script>


<script>
        $(document).ready(function() {
            $('.m-carousel-inner').find('img:first');
            $(".fancybox").fancybox({
            wrapCSS    : 'fancybox-custom',
            closeClick : true,

            openEffect : 'none',

            helpers : {
                title : {
                    type : 'inside'
                },
                overlay : {
                    css : {
                        'background' : 'rgba(238,238,238,0.85)'
                    }
                }
            }
        });
        });
    </script>
@stop
