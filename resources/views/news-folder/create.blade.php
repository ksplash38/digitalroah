@extends('layouts.app')


@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/css/main.css')}}">
    @if (session()->has('success_message'))
    <div class="spacer"></div>
    <div class="alert alert-success">
        {{ session()->get('success_message') }}
    </div>
@endif

@if(count($errors) > 0)
    <div class="spacer"></div>
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container-contact100">
            <div class="wrap-contact100">
                    <form method="post" action="{{ route('news.store') }}"  class="contact100-form validate-form">
                            @csrf

                    <span class="contact100-form-title">
                       CREATE NEWS & EVENTS
                    </span>

<a href="{{ URL::previous() }}" class="btn btn-success" style="margin-bottom:1em">Back</a>



                    <div class="wrap-input100  bg1" >
                        <span class="label-input100">TITLE *</span>




                    <input class="input100" type="text" name="title" placeholder="Enter title" required>

                    </div>


                    <div class="wrap-input100  bg1">
                            <span class="label-input100">LINK *</span>




                        <input class="input100" type="text" name="link" placeholder="Enter link">

                        </div>

                    {{-- <div class="wrap-input100 bg1 rs1-wrap-input100">
                        <span class="label-input100">Category *</span>
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                        <div>
                            <select class="js-select2" name="category">
                                <option>Please choose</option>
                                <option class="select">PAYMENT ENVIDENCE</option>
                                <option>COMPLAINT</option>
                                <option>OTHERS</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div> --}}







                    <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                            <span class="label-input100">Start Date </span>
                            <input class="input100" type="date" name="start_date" placeholder="Enter Your subject ">
                        </div>

                        <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                                <span class="label-input100">Start Time </span>
                                <input class="input100" type="time" name="start_time" placeholder="Enter Your subject ">
                            </div>


                            <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                                    <span class="label-input100">End Date </span>
                                    <input class="input100" type="date" name="end_date" placeholder="Enter Your subject ">
                                </div>

                                <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                                        <span class="label-input100">End Time </span>
                                        <input class="input100" type="time" name="end_time" placeholder="Enter Your subject ">
                                    </div>

{{-- {{dd($unique_key)}} --}}



<div class="wrap-input100 bg1 rs1-wrap-input100">
        <span class="label-input100">Re-occuring *</span>
        <i class="fa fa-arrows-v" aria-hidden="true"></i>
        <div>


                <input type="checkbox" name="is_recurring">

        </div>

        <span class="label-input100">Re-occuring  Frequency*</span>
        <i class="fa fa-arrows-v" aria-hidden="true"></i>
        <div>


                <input type="number" name="frequence_number_of_recurring">

        </div>


    </div>


    <div class="wrap-input100 bg1 rs1-wrap-input100">
            <span class="label-input100">Reoccurance Period *</span>
            <i class="fa fa-arrows-v" aria-hidden="true"></i>
            <div>

    <select id="langOption" class="filterOptions js-select2" name="frequence_type_of_recurring"   >
            <option data-lang-option="all" value="all" selected> Choose Options </option>
            <option data-lang-option="payment" value="RecurringFrequenceType::DAY" >Daily</option>
            <option data-lang-option="payment" value="RecurringFrequenceType::WEEK" >Weekly</option>
            <option data-lang-option="payment" value="RecurringFrequenceType::MONTH" >Monthly</option>

        </select>
        <div class="dropDownSelect2"></div>
            </div></div>



            <div class="wrap-input100 bg1 rs1-wrap-input100">
                    <span class="label-input100">Visibility *</span>
                    <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    <div>


                            <input type="checkbox" name="is_public">

                    </div>




                </div>


                <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                        <span class="label-input100">End of Reoccuring </span>
                        <input class="input100" type="date" name="end_of_recurring" placeholder="Enter Your subject ">
                    </div>







                    <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "">
                        <span class="label-input100">Cover Image *</span>
                        {{-- <input class="input100" type="text" name="email" placeholder="Enter Your Email "> --}}


                    <input class="input100" type="file" name="image" placeholder="Enter Your Email" required>

                    </div>

                    <div class="wrap-input100 bg1 rs1-wrap-input100">
                        <span class="label-input100">Images</span>
                        <input class="input100" type="file"  name="images[]"  multiple>
                    </div>




                    <div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Please Type Your Message">
                        <span class="label-input100">Description</span>
                        <textarea class="input100" name="description" placeholder="Your message here..."></textarea>
                    </div>

                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn">
                            <span>
                                Submit
                                <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                            <div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Sent your message successfully!</h3> </div>
                                <div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Error</h3> Sorry there was an error sending your form. </div>
                </form>
            </div>
        </div>




    <!--===============================================================================================-->

    <!--===============================================================================================-->
    <script>

            window.addEventListener
            (
                "load",
                function()
                {
                    document.getElementById("something").style.display = "none";
                }, false
            );
            $('.filterOptions').change(function(){

            var theChosenLang = $('#langOption').find(':selected').attr('data-lang-option');

            // var theChosenCat = $('#categoryOption').find(':selected').attr('data-category-option');


            $('.displayBox').css('display','none');
            var selector = '.displayBox';

            if(theChosenLang != "all"){
                selector += '[data-lang-option="'+ theChosenLang +'"]';
            }
            // if(theChosenCat != "all"){
            //     selector += '[data-category-option="'+ theChosenCat +'"]';
            // }

            $(selector).fadeIn();

            });
                    </script>




        <script src="{{asset('contactustemp/vendor/select2/select2.min.js')}}"></script>
        <script>
            $(".js-select2").each(function(){
                $(this).select2({
                    minimumResultsForSearch: 20,
                    dropdownParent: $(this).next('.dropDownSelect2')
                });


                $(".js-select2").each(function(){
                	$(this).on('select2:close', function (e){
                		if($(this).val() == "Please chooses") {
                			$('.js-show-service').slideUp();
                		}
                		else {
                			$('.js-show-service').slideUp();
                			$('.js-show-service').slideDown();
                		}
                	});
                });
            })
        </script>

@endsection
