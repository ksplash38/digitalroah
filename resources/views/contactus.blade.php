@extends('layouts.app')


@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="{{asset('contactustemp/css/main.css')}}">
    @if (session()->has('success_message'))
    <div class="spacer"></div>
    <div class="alert alert-success">
        {{ session()->get('success_message') }}
    </div>
@endif

@if(count($errors) > 0)
    <div class="spacer"></div>
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

    <div class="container-contact100">
            <div class="wrap-contact100">
                    <form method="post" action="{{ route('contactus.store') }}"  class="contact100-form validate-form">
                            @csrf

                    <span class="contact100-form-title">
                        Contact Us
                    </span>



                    <div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
                        <span class="label-input100">FULL NAME *</span>

                        @if (auth()->user())
                        <input type="text" class="form-control"  name="name" value="{{ auth()->user()->name }}" readonly>
                    @else
                    <input class="input100" type="text" name="name" placeholder="Enter Your Name" required>
                    @endif
                    </div>

                    {{-- <div class="wrap-input100 bg1 rs1-wrap-input100">
                        <span class="label-input100">Category *</span>
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                        <div>
                            <select class="js-select2" name="category">
                                <option>Please choose</option>
                                <option class="select">PAYMENT ENVIDENCE</option>
                                <option>COMPLAINT</option>
                                <option>OTHERS</option>
                            </select>
                            <div class="dropDownSelect2"></div>
                        </div>
                    </div> --}}







                    <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
                            <span class="label-input100">Subject *</span>
                            <input class="input100" type="text" name="subject" placeholder="Enter Your subject ">
                        </div>

{{-- {{dd($unique_key)}} --}}



<div class="wrap-input100 bg1 rs1-wrap-input100">
        <span class="label-input100">Category *</span>
        <i class="fa fa-arrows-v" aria-hidden="true"></i>
        <div>

<select id="langOption" class="filterOptions js-select2" name="category"   >
        <option data-lang-option="all" value="all" selected> Choose Options </option>
        <option data-lang-option="payment" value="paymentenvidence" >PAYMENT ENVIDENCE</option>
 <option data-lang-option="english" value="complaint">COMPLAINT</option>
 <option data-lang-option="english" value="complaint">BECOME MEMBER</option>
 <option data-lang-option="english" value="others">OTHERS</option>
    </select>
    <div class="dropDownSelect2"></div>
        </div></div>

        @if (!empty($unique_key))
        <div class="col-md-12">
                <div class="displayBox" id="something" data-lang-option="payment">

                        <span class="label-input100">Payment ID</span>
                        <input class="input100"  type="text" name="payment_id" value="{{($unique_key)}} ">

        </div>



</div>

@endif



                    <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
                        <span class="label-input100">Email *</span>
                        {{-- <input class="input100" type="text" name="email" placeholder="Enter Your Email "> --}}
                        @if (auth()->user())
                        <input type="email" class="form-control"  name="email" value="{{ auth()->user()->email }}" readonly>
                    @else
                    <input class="input100" type="text" name="email" placeholder="Enter Your Email" required>
                    @endif
                    </div>

                    <div class="wrap-input100 bg1 rs1-wrap-input100">
                        <span class="label-input100">Phone</span>
                        <input class="input100" type="text" name="mobile" placeholder="Enter Phone Number">
                    </div>




                    <div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Please Type Your Message">
                        <span class="label-input100">Message</span>
                        <textarea class="input100" name="message" placeholder="Your message here..."></textarea>
                    </div>

                    <div class="container-contact100-form-btn">
                        <button class="contact100-form-btn">
                            <span>
                                Submit
                                <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                            </span>
                        </button>
                    </div>
                            <div id="success_message" style="width:100%; height:100%; display:none; "> <h3>Sent your message successfully!</h3> </div>
                                <div id="error_message" style="width:100%; height:100%; display:none; "> <h3>Error</h3> Sorry there was an error sending your form. </div>
                </form>
            </div>
        </div>




    <!--===============================================================================================-->

    <!--===============================================================================================-->
    <script>

            window.addEventListener
            (
                "load",
                function()
                {
                    document.getElementById("something").style.display = "none";
                }, false
            );
            $('.filterOptions').change(function(){

            var theChosenLang = $('#langOption').find(':selected').attr('data-lang-option');

            // var theChosenCat = $('#categoryOption').find(':selected').attr('data-category-option');


            $('.displayBox').css('display','none');
            var selector = '.displayBox';

            if(theChosenLang != "all"){
                selector += '[data-lang-option="'+ theChosenLang +'"]';
            }
            // if(theChosenCat != "all"){
            //     selector += '[data-category-option="'+ theChosenCat +'"]';
            // }

            $(selector).fadeIn();

            });
                    </script>




        <script src="{{asset('contactustemp/vendor/select2/select2.min.js')}}"></script>
        <script>
            $(".js-select2").each(function(){
                $(this).select2({
                    minimumResultsForSearch: 20,
                    dropdownParent: $(this).next('.dropDownSelect2')
                });


                $(".js-select2").each(function(){
                	$(this).on('select2:close', function (e){
                		if($(this).val() == "Please chooses") {
                			$('.js-show-service').slideUp();
                		}
                		else {
                			$('.js-show-service').slideUp();
                			$('.js-show-service').slideDown();
                		}
                	});
                });
            })
        </script>

@endsection
