@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{ asset('owl/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('owl/dist/assets/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('fadeinfolder/aos.css') }}">

<style>

    .panel
    {

        text-align: center;
    }
    .panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
    .panel-body
    {
        padding: 0px;
        text-align: center;
    }

    .the-price
    {
        background-color: rgba(220,220,220,.17);
        box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
        padding: 20px;
        margin: 0;
    }

    .the-price h1
    {
        line-height: 1em;
        padding: 0;
        margin: 0;
    }

    .subscript
    {
        font-size: 25px;
    }

    /* CSS-only ribbon styles    */
    .cnrflash
    {
        /*Position correctly within container*/
        position: absolute;
        top: -9px;
        right: 4px;
        z-index: 1; /*Set overflow to hidden, to mask inner square*/
        overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
        width: 100px;
        height: 100px;
        border-radius: 3px 5px 3px 0;
    }
    .cnrflash-inner
    {
        /*Set position, make larger then 			container and rotate 45 degrees*/
        position: absolute;
        bottom: 0;
        right: 0;
        width: 145px;
        height: 145px;
        -ms-transform: rotate(45deg); /* IE 9 */
        -o-transform: rotate(45deg); /* Opera */
        -moz-transform: rotate(45deg); /* Firefox */
        -webkit-transform: rotate(45deg); /* Safari and Chrome */
        -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
        -ms-transform-origin: 100% 100%;  /* IE 9 */
        -o-transform-origin: 100% 100%; /* Opera */
        -moz-transform-origin: 100% 100%; /* Firefox */
        background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
        background-size: 4px,auto, auto,auto;
        background-color: #aa0101;
        box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
    }

    .cnrflash-ultimate
    {
        /*Set position, make larger then 			container and rotate 45 degrees*/
        position: absolute;
        bottom: 0;
        right: 0;
        width: 145px;
        height: 145px;
        -ms-transform: rotate(45deg); /* IE 9 */
        -o-transform: rotate(45deg); /* Opera */
        -moz-transform: rotate(45deg); /* Firefox */
        -webkit-transform: rotate(45deg); /* Safari and Chrome */
        -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
        -ms-transform-origin: 100% 100%;  /* IE 9 */
        -o-transform-origin: 100% 100%; /* Opera */
        -moz-transform-origin: 100% 100%; /* Firefox */
        background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
        background-size: 4px,auto, auto,auto;
        background-color: rgb(240,230,140);
        box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
    }
    .cnrflash-inner:before, .cnrflash-inner:after
    {
        /*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
        content: " ";
        display: block;
        position: absolute;
        bottom: -16px;
        width: 0;
        height: 0;
        border: 8px solid #800000;
    }
    .cnrflash-inner:before
    {
        left: 1px;
        border-bottom-color: transparent;
        border-right-color: transparent;
    }
    .cnrflash-inner:after
    {
        right: 0;
        border-bottom-color: transparent;
        border-left-color: transparent;
    }
    .cnrflash-label
    {
        /*Make the label look nice*/
        position: absolute;
        bottom: 0;
        left: 0;
        display: block;
        width: 100%;
        padding-bottom: 5px;
        color: #fff;
        text-shadow: 0 1px 1px rgba(1,1,1,.8);
        font-size: 0.95em;
        font-weight: bold;
        text-align: center;
    }

section{
	padding: 60px 0;
}
section .section-title{
	text-align:center;
	color:#007b5e;
	margin-bottom:50px;
	text-transform:uppercase;
}
#what-we-do{
	background:#ffffff;
}
#what-we-do .card{
	padding: 1rem!important;
	border: none;
	margin-bottom:1rem;
	-webkit-transition: .5s all ease;
	-moz-transition: .5s all ease;
	transition: .5s all ease;
}
#what-we-do .card:hover{
	-webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
	-moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
	box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
}
#what-we-do .card .card-block{
	padding-left: 50px;
    position: relative;
}
#what-we-do .card .card-block a{
	color: #5cb85c !important;
	font-weight:700;
	text-decoration:none;
}
#what-we-do .card .card-block a i{
	display:none;

}
#what-we-do .card:hover .card-block a i{
	display:inline-block;
	font-weight:700;

}
#what-we-do .card .card-block:before{
	font-family: FontAwesome;
    position: absolute;
    font-size: 39px;
    color: #5cb85c;
    left: 0;
	-webkit-transition: -webkit-transform .2s ease-in-out;
    transition:transform .2s ease-in-out;
}
#what-we-do .card .block-1:before{
    content: "\f2b5";
}
#what-we-do .card .block-2:before{
    content: "\f0d1";
}
#what-we-do .card .block-3:before{
    content: "\f274";
}
#what-we-do .card .block-4:before{
    content: "\f0a1";
}
#what-we-do .card .block-5:before{
    content: "\f0ac";
}
#what-we-do .card .block-6:before{
    content: "\f1ba";
}
#what-we-do .card:hover .card-block:before{
	-webkit-transform: rotate(360deg);
	transform: rotate(360deg);
	-webkit-transition: .5s all ease;
	-moz-transition: .5s all ease;
	transition: .5s all ease;
}


.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  /* max-width: 300px; */
  border-start-end-radius: 2em;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.price {
  color: grey;
  font-size: 15px;
}

.card button {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #5cb85c;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

.card button:hover {
  opacity: 0.7;
}
.owl-theme .owl-dots .owl-dot{
    display:none;
}


    </style>
    <script src="{{ asset('fadeinfolder/aos.js') }}"></script>

<script>
        AOS.init();
      </script>

{{-- <a class="btn btn-default" href="https://api.whatsapp.com/send?phone=2347064933041" target="_blank">Click to Send Message</a> --}}
    <!-- start Banner Area -->

	<section class="home-banner-area relative">
		<div class="container-fluid">
			<div class="row  d-flex align-items-center justify-content-center">
				<div class="header-left col-lg-5 col-md-6 ">
					<h1>
						TRACE YOUR WAY <br>
						WITH A NEW FRIEND
					</h1>
					<p class="pt-20 pb-20">
						Get to meet new friends posted to thesame place as you.
					</p>
					<div class="vdo-section">
						<a href="https://www.youtube.com/watch?v=p29dKpHnPjQ" class="single-charity">
							<img src="img/video-icon.png" alt="">
						</a>
						<span>
							<a href="https://www.youtube.com/watch?v=p29dKpHnPjQ" class="single-charity" style="color:red">
                                <i class="fa fa-play-circle" aria-hidden="true"></i>	Watch How to Use
							</a>
						</span>
					</div>
				</div>
				<div class="col-lg-7 col-md-6 col-sm-8 header-right">
                        <div id="home-owl" class="owl-carousel owl-theme">

                        @if(!empty ($sliders))
                                  @foreach($sliders as $slider)
                                  <a href="{{$slider->link}}" >
                                  <div class="home-item">
                                        <!-- section background -->
                                        <img src="{{ Voyager::image($slider->image) }}" alt=""  style="width:100% ; height:28em">
                   {{-- <div class="section-bg" style="background-image: url({{ Voyager::image($slider->image) }});"></div> --}}
                   <!-- /section background -->

                   <!-- home content -->
                   {{-- <div class="home">
                       <div class="container">
                           <div class="row">
                               <div class="col-md-8">
                                   <div class="home-content">
                                        <h1 style="color:black">{{$slider->title}}</h1>
                                       <p class="lead">{{$slider->body}}</p>
                                       <a href="#" class="btn btn-success">View Causes</a>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div> --}}
                   <!-- /home content -->
               </div></a>
                    @endforeach
                    @endif
                </div>
					<div class="form-wrap">
                            @if (Session::has('message'))
                            <div class="alert alert-danger">{{ Session::get('message') }}</div>
                         @endif

                         @if ($nysccamp == 1)
                         @if (Auth::user())
                         <p class="mb-20 text-white"> You Have  {{ $attempt }} Attempt{{ ($attempt > 1) ? 's' : '' }} Left </p>
                         @endif


                         <p class="mb-20 text-white">Search Me</p>


                         <form action="/search-side" class="form" role="form" autocomplete="off"  method="GET">
                             <div class="row">

                                 <div class="col-md-5 wrap-left donation-input">
                                     <div class="form-group">

                                             <div class="text-center" style="color:white  ; background-color:black; padding:0.5em 0.5em 0.5em 0.5em" >FROM</div>


                                             <select class="form-control" name="stateFrom"  value="{{ request()->input('stateFrom') }}"  >

                                                     @foreach($states as $state)
                                                     @if($user = Auth::user())
                                                     <option value="{{$state->name}}"  @if($user->stateFrom=== $state->name) selected='selected' @endif>{{$state->name}}     </option>

 @else

 <option value="{{$state->name}}" >{{$state->name}}     </option>

                                                     @endif
                                                   @endforeach
                                                   </select>

                                     </div>
                                 </div>

                                 <div class="col-md-5 wrap-center donation-input">
                                         <div class="form-group">

                                                 <div class="text-center" style="color:white  ; background-color:black; padding:0.5em 0.5em 0.5em 0.5em"">DESTINATION</div>
                                                 <select class="form-control" name="stateTo" value="{{ request()->input('stateTo') }}">

                                                         @foreach($states as $state)
                                                         <option value="{{$state->name}}">{{$state->name}}     </option>
                                                       @endforeach
                                                       </select>

                                         </div>
                                     </div>

                                     <div class="col-md-5 wrap-center donation-input">
                                             <div class="form-group">
                                                     <div class="text-center" style="color:white  ; background-color:black; padding:0.5em 0.5em 0.5em 0.5em"" >BATCH</div>

                                                     <select class="form-control" name="batch" value="{{ request()->input('batch') }}">
                                                             @foreach($batches as $batch)
                                                             <option value="{{$batch->name}}">{{$batch->name}}     </option>
                                                           @endforeach
                                                           </select>

                                             </div>
                                         </div>


                                         <div class="col-md-5 wrap-center donation-input">
                                                 <div class="form-group">

                                                  <div class="text-center" style="color:white  ; background-color:black; padding:0.5em 0.5em 0.5em 0.5em"" >YEAR</div>
                                                         <select class="form-control" name="year" value="{{ request()->input('year') }}">
                                                                 @foreach($years as $year)
                                                                 <option value="{{$year->year}}">{{$year->year}}     </option>
                                                               @endforeach
                                                               </select>

                                                 </div>
                                             </div>

                                 <div class="col-md-2 wrap-right">
                                     <div class="input-group dates-wrap">
                                         <button type="submit" class="primary-btn white">SEARCH</button>
                                     </div>
                                 </div>
                             </div>
                         </form>
                         @endif

					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- End Banner Area -->
    <style>

/* .igg{
    background-image: url("{{ asset('deliveryman.png') }}");
    background-repeat: no-repeat, repeat;
    vertical-align: middle;

    background-size: 100% 300px;

} */

.container .content {
  position: absolute;
  bottom: 0;
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1;
  width: 100%;
  padding: 20px;
}

.full-forms {
    padding-top:9em
    /* background-color:rgb(192,192,192);opacity:0.6;
    height: 100%;
    width:100%; */
}
.col-centered{
    float: none;
    margin: 0 auto;
}
.myDiv {
    position: relative;
    z-index: 1;
    background-color:#dadada;
}

section h4{
font-size: 3em;
color:#007b5e;
font-family: "Playfair Display", sans-serif;
font-weight: 600;

}


.myDiv .bg {

background: url("{{ asset('deliverymansec.png') }}") ;



    padding: 3em 3em 3em 3em;
background-position: center;
    z-index: -1;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
background-size: cover;
    background-repeat: no-repeat, repeat;

background-color:#dadada;
    background-size: 100% 200px;
    /* opacity: .1; */
    width: 100%;
    height: 300px;
    /* background-attachment: fixed; */
}

.paddingsystem{
    padding: 4em 3em 3em 3em;

}

.smallboxtrack{
    padding: 4em 3em 3em 3em;
    background-color: rgba(255, 255, 255, 0.3);
}
    </style>


    <section>
            <h4 class="text-center mb-2">TRACK YOUR WAYBILL</h4>
    <div class="myDiv">

        <div class="row">


<div class="col-md-6" >
    <div class="paddingsystem">
        <div class="smallboxtrack">


                <form action="/trackme" method="GET">

                    {{ csrf_field() }}
                        <div class="form-group">
                                {{-- <label for="inputOrderTrackingID" class="col-sm-12 control-label mb-20 text-white text-center"  >Track Waybill</label> --}}

                                <div class="row">
                                        <div class="col-lg-12 col-centered">     <input type="text" name="tracking_id" class="form-control" id="inputOrderTrackingID" value="" placeholder="Insert Tracking ID ..."></div>
                                    </div>
                            </div>
                            <div class="form-group text-center">
                                <div class=" col-sm-12">
                                    <button    class="btn btn-danger">Track Me</button>
                                </div>
                            </div>


                </form>
            </div>
        </div>



</div>
<div class="col-md-6">
        <div data-aos="fade-left"
        data-aos-anchor="#example-anchor"
        data-aos-offset="500"
        data-aos-duration="1500">
        <div class="bg">       </div>
    </div>
    </div>
</div>




        </div>

    </section>

    {{-- <div class="form-horizontal form-wrap igg" style="
    padding: 40px;
    /* position: absolute; */

    left: 100px;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    transform: translateY(-50%);
     ">
     <br>
    <div class="full-form" >
            <div class="form-group">
                    <label for="inputOrderTrackingID" class="col-sm-12 control-label mb-20 text-white text-center"  >Track Waybill</label>

                    <div class="row">
                            <div class="col-lg-5 col-centered">     <input type="text" class="form-control" id="inputOrderTrackingID" value="" placeholder="Insert Tracking ID"></div>
                        </div>
                </div>
                <div class="form-group text-center">
                    <div class=" col-sm-12">
                        <button type="button" id="shopGetOrderStatusID" class="btn btn-danger">Track Me</button>
                    </div>
                </div>

    </div>

        </div> --}}
	<section id="what-we-do">
            <div class="container">
                <h2 class="section-title mb-2 h1">What we do</h2>
                <p class="text-center text-muted h5">Explicit services brought together all at your door step...</p>
                <div class="row mt-5">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="card-block block-1">
                                <h3 class="card-title">Make New Friend</h3>
                                <p class="card-text">(DR)DigitalRoah has made it a major priority for people who have been posted by NYSC for camp to another state to make a new friend posted to thesame state as him / her and could possibly travel to where the camp is located together.</p>
                                {{-- <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="card-block block-2">
                                <h3 class="card-title">Open Market & Delivery Service</h3>
                                <p class="card-text">(DR)DigitalRoah has provided an online platform for buyers and sellers to trade and have their products delivered at their doorstep.</p>
                                {{-- <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">

                        <div class="card">
                            <div class="card-block block-3">
                                <h3 class="card-title">News & Events</h3>
                                <p class="card-text">(DR)DigitalRoahWith has taken it upon itself to bring you latest news, events, music & videos around the globe. Stay Connected!!!.</p>
                                <a href="/news" title="view" class="read-more" >View >><i class="fa fa-angle-double-right ml-2"></i></a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="card-block block-4">

                                <h3 class="card-title">Advertisement & Promotion</h3>
                                <p class="card-text">(DR)DigitalRoah tends to support everyone as we believe everyone's voice should be heard especially with the Digital Trend. Therefore we promote Ads and products online.</p>
                                {{-- <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="card-block block-5">
                                <h3 class="card-title">Web Design & Photography</h3>
                                <p class="card-text">Nature has been our best sight thats why we have gone through the thicks to have them beautifully made in view form through photography. Our web services, UI UX interfaces has been outstanding together with its functions. Think Website, think Digitalroah</p>
                                {{-- <a   onclick="javascript:alert('Coming Soon')" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                        <div class="card">
                            <div class="card-block block-6">
                                <h3 class="card-title">Ride Hailing Service</h3>
                                <p class="card-text">(DR)DigitalRoah provides varieties of vehicles and drivers to pick you and drop you off at any location of your choice.</p>
                                {{-- <a href="javascript:void();" title="Read more" class="read-more" >Read more<i class="fa fa-angle-double-right ml-2"></i></a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section  >

             <div class="container">
                 <div class="row">
<div class="col-lg-12">

        <div  style="background-color:#5cb85c">
                <h3 class="text-center" style="color:white; margin-top:-2em">PRODUCTS</h3>
            </div>
            <br><br>

                <div class="owl-carousel-products  ">
                        @foreach ($products as $product)
                        <div class="item">
            <div class="card" >
              <div class="product-image">

                    <a href="{{ route('product.show', [$product->randomid]) }}"><img src="{{ productImage($product->image) }}" alt="product" class="image"   style="width: 100%; height: 168px"></a>
                    <hr>
                </div>

              <div class="product-info">
              <h3 class="limitme2">{{$product->name}}</h3>
              <div class="mt-3">
                    <span class="mr-4">{{Presentprice($product->price)}}</span>
                    @if (!empty($product->oldprice))
                    <del>₦ {{number_format($product->oldprice)}}</del>
                    @endif

                  </div>

                    <p class="limit3">{{$product->description}}</p>
                    <a href="{{ route('product.show', [$product->randomid]) }}">  <button>ORDER</button>  </a>
              </div>

            </div></div>
            @endforeach
        </div>
</div>

                 </div>

            </div>

            <!-- It's likely you'll need to link the card somewhere. You could add a button in the info, link the titles, or even wrap the entire card in an <a href="..."> -->


                <!-- more products -->

              </section>
<hr>

{{-- <section>
        <div class="owl-carousel-products owl-theme">
                @foreach ($products as $product)
          <div class="item"  style="width: 178px; height: 178px;  margin-right: 20px;">
                <a href="{{ route('product.show', [$product->randomid]) }}"><img src="{{ productImage($product->image) }}" alt="product" class="image" style="width:100%"></a>

          </div>



          @endforeach



          </div>



</section> --}}


	<!-- Start Causes Area -->
	<section class="causes-area sectin-gap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 section-title">
					<h2>Get Connected now with people in thesame camp as you!</h2>
					<p>
                        Digitalroah is Focused on helping Youth Corp members to find people posted to thesame place as them and also move together as group instead of individually.

                    </p>
                    <p>However, we have lots of interesting news and events to keep you enguaged!</p>

                    <br>
                    <h3 style="color:red">VIEW PRICING BELOW FOR THOSE GOING TO CAMP</h3>
                <img src="{{asset('animated-arrow-image-0368.gif')}}" alt="">
				</div>
            </div>


            <div class="row  ">
                <div class=" col-md-offset-2 col-xs-12 col-md-3">
                    <div class="panel panel-primary">
                            <div class="single-cause">
                                    <div class="top">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="color:#cd7f32">
                                Bronze</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1 style="color:#cd7f32">
                                        ₦500<span class="subscript">/1mo</span></h1>
                                {{-- <small>1 month FREE trial</small> --}}
                            </div>
                            <table class="table">
                                <tr>
                                    <td>
                                        3 Attempts
                                    </td>
                                </tr>
                                {{-- <tr class="active">
                                    <td>
                                        1 Project
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        100K API Access
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        100MB Storage
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Custom Cloud Services
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        Weekly Reports
                                    </td>
                                </tr> --}}
                            </table>
                        </div>
                        <div class="panel-footer"  >
                            <a href="/payment" class="btn btn-success" role="button">GET ME</a>
                            </div>
                    </div>
                </div>
                </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="panel panel-success" style="padding-bottom:2em">
                        <div class="cnrflash">
                            <div class="cnrflash-inner">
                                <span class="cnrflash-label">MOST
                                    <br>
                                    POPULR</span>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <h3 class="panel-title" style="color:silver">
                                Silver</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1 style="color:silver">
                                        ₦1,000<span class="subscript">/1mo</span></h1>
                                {{-- <small>1 month FREE trial</small> --}}
                            </div>
                            <table class="table">
                                <tr>
                                    <td>
                                            5 Attempts
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        Live Customer Care Support / Services
                                    </td>
                                </tr>
                                {{-- <tr>
                                    <td>
                                        100K API Access
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        200MB Storage
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Custom Cloud Services
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        Weekly Reports
                                    </td>
                                </tr> --}}
                            </table>
                        </div>
                        <div class="panel-footer">
                                <a href="/payment" class="btn btn-success" role="button">GET ME</a>
                                </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="panel panel-info" style="padding-bottom:1em">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="color:rgb(185,242,255)">
                                Diamond</h3>
                        </div>
                        <div class="panel-body">
                            <div class="the-price">
                                <h1 style="color:rgb(185,242,255)">
                                        ₦2,000<span class="subscript">/1mo</span></h1>
                                {{-- <small>1 month FREE trial</small> --}}
                            </div>
                            <table class="table">
                                <tr>
                                    <td>
                                            10 Attempts
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                            Live Customer Care Support / Services
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                            Live Chat with Medical Doctors
                                    </td>
                                </tr>
                                {{-- <tr class="active">
                                    <td>
                                        500MB Storage
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Custom Cloud Services
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td>
                                        Weekly Reports
                                    </td>
                                </tr> --}}
                            </table>
                        </div>
                        <div class="panel-footer">
                                <a href="/payment" class="btn btn-success" role="button">GET ME</a>
                                </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                        <div class="panel panel-info" style="padding-bottom:1em">
                                <div class="cnrflash">
                                        <div class="cnrflash-ultimate" >
                                            <span class="cnrflash-label">ULTIMATE
                                             </span>
                                        </div>
                                    </div>
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    Gold</h4>
                            </div>
                            <div class="panel-body">
                                <div class="the-price">
                                    <h1>
                                            ₦5,000<span class="subscript">/2mo</span></h1>
                                    {{-- <small>1 month FREE trial</small> --}}
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                                Infinity Attempts
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                                Live Customer Care Support / Services
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                Transportation to Camp
                                        </td>
                                    </tr>
                                    <tr>
                                            <td>
                                                    Refreshment / Food on Arrival
                                            </td>
                                        </tr>
                                    {{-- <tr class="active">
                                        <td>
                                            500MB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr> --}}
                                </table>
                            </div>
                            <div class="panel-footer">
                                    <a href="/payment" class="btn btn-success" role="button">GET ME</a>
                                    </div>
                        </div>
                    </div>
            </div>

			{{-- <div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-cause">
						<div class="top">
							<div class="thumb">
								<img class="c-img img-fluid" src="theme/img/causes/c1.jpg" alt="">
							</div>
							<a href="#">
								<h3>Help Restoring Uganda’s Water
									Pipelines Construction</h3>
							</a>
							<p class="text">
								inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct especially
								workplace.
							</p>
						</div>
						<div class="middle">
							<div class="skill_main">
								<div class="skill_item">
									<div class="progress">
										<div class="progress-bar progress-bar1" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="d-flex">
								<div class="mr-50">
									<h5><span class="counter">76</span>%</h5>
									<p>Funded</p>
								</div>
								<div class="mr-50">
									<h5>$<span class="counter">7,689</span></h5>
									<p>Pledged</p>
								</div>
								<div class="">
									<h5><span class="counter">29</span></h5>
									<p>Days Remaining</p>
								</div>
							</div>
						</div>
						<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							<a href="#" class="primary-btn offwhite">View Details</a>
							<a href="#" class="primary-btn primary-btn1">Donate Here</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6">
					<div class="single-cause">
						<div class="top">
							<div class="thumb">
								<img class="c-img img-fluid" src="img/causes/c2.jpg" alt="">
							</div>
							<a href="#">
								<h3>Help Restoring Uganda’s Water
									Pipelines Construction</h3>
							</a>
							<p class="text">
								inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct especially
								workplace.
							</p>
						</div>
						<div class="middle">
							<div class="skill_main">
								<div class="skill_item">
									<div class="progress">
										<div class="progress-bar progress-bar2" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="d-flex">
								<div class="mr-50">
									<h5><span class="counter">76</span>%</h5>
									<p>Funded</p>
								</div>
								<div class="mr-50">
									<h5>$<span class="counter">7,689</span></h5>
									<p>Pledged</p>
								</div>
								<div class="">
									<h5><span class="counter">29</span></h5>
									<p>Days Remaining</p>
								</div>
							</div>
						</div>
						<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							<a href="#" class="primary-btn offwhite">View Details</a>
							<a href="#" class="primary-btn">Donate Here</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-6">
					<div class="single-cause">
						<div class="top">
							<div class="thumb">
								<img class="c-img img-fluid" src="img/causes/c3.jpg" alt="">
							</div>
							<a href="#">
								<h3>Help Restoring Uganda’s Water
									Pipelines Construction</h3>
							</a>
							<p class="text">
								inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct especially
								workplace.
							</p>
						</div>
						<div class="middle">
							<div class="skill_main">
								<div class="skill_item">
									<div class="progress">
										<div class="progress-bar progress-bar3" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
							<div class="d-flex">
								<div class="mr-50">
									<h5><span class="counter">76</span>%</h5>
									<p>Funded</p>
								</div>
								<div class="mr-50">
									<h5>$<span class="counter">7,689</span></h5>
									<p>Pledged</p>
								</div>
								<div class="">
									<h5><span class="counter">29</span></h5>
									<p>Days Remaining</p>
								</div>
							</div>
						</div>
						<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							<a href="#" class="primary-btn offwhite">View Details</a>
							<a href="#" class="primary-btn primary-btn3">Donate Here</a>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</section>
	<!-- end Causes Area -->

	<!-- Start Collection Area -->
	<section class="collection-area section-gap">
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-4 col-md-8">
					<div class="text">
						<h2>Have a great experience.</h2>
						<p>
                             DigitalRoah gives you an awsome user experience with 48hours live customer care and chat flow experience / support. We render you the best form of services that eases your service comfort.
						</p>
					</div>
                </div>

                <div class="col-lg-8 col-md-10 col-sm-12 col-12">
                        <div class="collection-box">
                            <p><b>USERS</b></p>
                            <h3 class="color4"><span class="counter">{{ $countUser }}</span></h3>
                            <i class="lnr lnr-arrow-up"></i>
                            <p>Get Connected!</p>
                        </div>
                    </div>

				{{-- <div class="col-lg-2 col-md-4 col-sm-6 col-6">
					<div class="collection-box">
						<p><small>USD</small></p>
						<h3 class="color1"><span class="counter">21</span> M</h3>
						<i class="lnr lnr-arrow-up"></i>
						<p>2015</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-6">
					<div class="collection-box">
						<p><small>USD</small></p>
						<h3><span class="counter">15</span> M</h3>
						<i class="lnr lnr-arrow-up"></i>
						<p>2016</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-6">
					<div class="collection-box">
						<p><small>USD</small></p>
						<h3 class="color3"><span class="counter">23</span> M</h3>
						<i class="lnr lnr-arrow-up"></i>
						<p>2017</p>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-6">
					<div class="collection-box">
						<p><small>USD</small></p>
						<h3 class="color4"><span class="counter">25</span> M</h3>
						<i class="lnr lnr-arrow-up"></i>
						<p>2018</p>
					</div>
				</div> --}}
			</div>
		</div>
	</section>
	<!-- End Collection Area -->

	<!-- Start Condition Area -->
	{{-- <section class="condition-area section-gap">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-6 col-md-8 col-sm-10">
					<div class="condition-left owl-carousel owl-condition">
						<img class="img-fluid" src="img/condition/c1.jpg" alt="">
						<img class="img-fluid" src="img/condition/c1.jpg" alt="">
					</div>
				</div>
				<div class="offset-lg-1 col-lg-5">
					<div class="condition-right">
						<h2>
							New way <br>
							to give back
						</h2>
						<p>
							If you are looking at blank cassettes on the web, you may be very confused at the difference in price You may
							see some for as low as each. If you are looking at blank cassettes on the web, you may be very confused at the
							difference in price You may see.
						</p>
						<ul>
							<li>If you are looking at blank cassettes on the web.</li>
							<li>Difference in price You may see some as low as each.</li>
							<li>May be very confused at the difference in price.</li>
							<li>If you are looking at blank cassettes on the web.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!-- End Condition Area -->

	<!-- Start Donation Area -->
	{{-- <section class="donation-area relative section-gap-top">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-5">
					<div class="text-center text-wrap">
						<h1 class="mb-25">Donate to help People Around the World</h1>
						<p>Las Vegas has more than 100,000 hotel rooms to choose from. There is something for every budget, and enough.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Donation Area -->

	<!-- Start Donation Form Area -->
	<section class="donation-form-area section-gap-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-10 col-sm-12">
					<div class="donation-box">
						<form action="">
							<div class="donation-input">
								<div class="form-group">
									<input type="text" placeholder="$20.00" onfocus="this.placeholder = ''" onblur="this.placeholder = '$20.00'"
									 class="form-control">
									<span class="fs-14">USD</span>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6 col-md-6">
									<div class="donation-type">
										<div class="form-check">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="optradio">
												One Time
											</label>
										</div>
										<p>Donate your amount for
											this session only</p>
									</div>
								</div>

								<div class="col-lg-6 col-md-6">
									<div class="donation-type">
										<div class="form-check">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" name="optradio">
												Ongoing
											</label>
										</div>
										<p>Donate your amount for
											this session only</p>
									</div>
								</div>
							</div>

							<button type="button" class="primary-btn w-100">Donate Now</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section> --}}
	<!-- end Donation Form Area -->

	<!-- Start Client Logo Area -->
	{{-- <section class="brand-area section-gap-bottom">
		<div class="container">
			<div class="owl-brand owl-carousel">
				<div class="item">
					<img src="charity/img/brands/b1.png" alt="">
				</div>
				<div class="item">
					<img src="charity/img/brands/b2.png" alt="">
				</div>
				<div class="item">
					<img src="charity/img/brands/b3.png" alt="">
				</div>
				<div class="item">
					<img src="charity/img/brands/b4.png" alt="">
				</div>
				<div class="item">
					<img src="charity/img/brands/b5.png" alt="">
				</div>
			</div>
		</div>
	</section> --}}
	<!-- End Client Logo Area -->

	<!-- Start Event Area -->
	<section class="event-area section-gap-bottom">
		<div class="container">
			<div class="row justify-content-center align-items-center ">
                    <div class="col-lg-5 event-left">
                            <h1>Sponsored</h1>
                            <p>
                             Sponsored News, Events / Media
                            </p>
                            @foreach ($sponsored as $sponsor)
                            <div class="single-event">
                                    <p>{{$sponsor->created_at->format('d F, Y ')}}</p>
                                    <h4>
                                        <a href="{{ route('news.show', [$sponsor->randomid]) }}">{{$sponsor->title}}
                                          </a>
                                    </h4>
                                </div>
                            @endforeach


                        </div>

                        <div class="col-lg-5 event-left">
                                <h1>Latest News</h1>
                                <p>
                                  Get Updated with the Latest New
                                </p>
                                @foreach ($news as $new)
                                <div class="single-event">
                                        <p>{{$new->created_at->format('d F, Y ')}}</p>
                                        <h4>
                                            <a href="{{ route('news.show', [$new->randomid]) }}">{{$new->title}}
                                              </a>
                                        </h4>
                                    </div>
                                @endforeach


                            </div>

			</div>
        </div>




	</section>
    <!-- End Event Area -->

    <script>

$(function() {
  $('.owl-carousel-products').owlCarousel({
    nav:false,
    autoplay:true,
    autoplayHoverPause:true,

    loop: true,

    stagePadding: 50,
    responsive:{
                    0:{
                        items:1,
                        margin: 10,
                    },
                    400:{
                        margin: 10,
                        items:3
                    },
                    1000:{
                        margin: 10,
                        items:4
                    }
                }

  });
});

            $(document).ready(function ()
               { $(".limitme2").each(function(i){
                    var len=$(this).text().trim().length;
                    if(len>2)
                    {
                        // this trims the letters to show only 3alphabets
                        $(this).text($(this).text().substr(0,11)+'');
                    }
                });
             });


            $(document).ready(function ()
               { $(".limit3").each(function(i){
                    var len=$(this).text().trim().length;
                    if(len>2)
                    {
                        // this trims the letters to show only 3alphabets
                        $(this).text($(this).text().substr(0,20)+'');
                    }
                });
             });




                </script>
    {{-- <script src="{{ asset('owl/docs/assets/owlcarousel/owl.carousel.js')}}"></script>

    <script>




                        $(document).ready(function() {
                          var owl = $('.owl-carousel-productss').owlCarousel({
                loop:false,
                margin:10,

                nav:false,
                autoplay:true,
              statgePadding: 300,
             autoplayHoverPause:true,

                responsive:{
                    0:{
                        items:1
                    },
                    400:{
                        items:3.3
                    },
                    1000:{
                        items:4.3
                    }
                }
            })
                        })


                      </script> --}}

@endsection
