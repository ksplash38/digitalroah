@extends('layouts.app')

@section('title', 'Thank You')



@section('body-class', 'sticky-footer')


@section('content')



<br><br>
   <div class="thank-you-section text-center">
     <img src="{{URL::to('digital-cicle.png')}}" style="width:10em; height:100px" />
       <h1>Thank you for <br> Your Order!</h1>
       <p>A confirmation email was sent</p>
       <div class="spacer"></div>
       <div>
           <a href="{{ url('/') }}" class="btn btn-success">Home Page</a>
       </div>
   </div>




@endsection

