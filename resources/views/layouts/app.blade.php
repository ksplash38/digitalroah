<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'DIGITAL ROAH') }}</title>
	<!-- Favicon-->
	<link rel="shortcut icon" href="{{ asset('main-logo.png') }}">
	<!-- Author Meta -->
	<meta name="author" content="digitalroah services">
	<!-- Meta Description -->
	<meta name="description" content="digitalroah services is focused on enhancing networking, delivery and waybill tracking">
	<!-- Meta Keyword -->
	<meta name="keywords" content="digitalroah, digitalroahservices, digitalroar, digitalroarservices, digitalroah delivery, digitalroar delivery">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->


	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:300,400,500,700" rel="stylesheet">
	<!--
			CSS
			============================================= -->
	<link rel="stylesheet" href="{{ asset('theme/css/linearicons.css') }}">
	<link rel="stylesheet" href="{{ asset('theme/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('theme/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('theme/css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ asset('theme/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/animate.min.css') }}">

	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{ asset('theme/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('cartstyle/css/style.css') }}">
    @yield('og')
    @yield('stylesproducts')
    <script type="application/ld+json">

        {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name" : "Digitalroah services",
        "url": "https://www.digitalroahservices.com.ng/",
        "sameAs" : [
        "https://web.facebook.com/digitalroah/posts/",
        "https://www.instagram.com/official_digitalroah/",
        "",
        ""
        ]
        }
        </script>
        <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "priceRange": "< 100",
        "name" : "Digitalroah services",
        "url": "https://www.digitalroahservices.com.ng/",
        "logo": "https://www.digitalroahservices.com.ng/main-logo.png",
        "image": "https://www.digitalroahservices.com.ng/main-logo.png",
        "description": "Digitalroah services offers different type of explicit services such as delivery, ride hailing, webdesigns, photography .e.t.c",
        "telephone": "08184751332",
        "address": {
        "@type": "PostalAddress",
        "addressCountry": "NG",

        "addressRegion": "",
        "streetAddress": "",
        "postalCode": ""
        },
        "openingHours": [
        "Mo -Su 24hrs"
        ]
        }

        </script>

        <script type="application/ld+json">
            {
              "@context": "https://schema.org/",
              "@type": "BreadcrumbList",
              "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "name": "homepage",
                "item": "https://www.digitalroahservices.com.ng/home"
              },{
                "@type": "ListItem",
                "position": 2,
                "name": "contactus",
                "item": "https://www.digitalroahservices.com.ng/contactus"
              },{
                "@type": "ListItem",
                "position": 3,
                "name": "landingpage",
                "item": "https://www.digitalroahservices.com.ng/"
              }]
            }
            </script>


        <script type="application/ld+json">
        {
          "@context": "https://schema.org/",
          "@type": "WebSite",
          "name": "digitalroah",
          "url": "https://www.digitalroahservices.com.ng/",
          "potentialAction": {
            "@type": "SearchAction",
            "target": "{search_term_string}",
            "query-input": "required name=search_term_string"
          }
        }
        </script>

        <style>
@media (max-width: 767px) {
.hidden-xs {
display: none !important;
}
}
@media (min-width: 768px) and (max-width: 991px) {
.hidden-sm {
display: none !important;
}
}

@media (min-width: 1200px) {
.visible-lg {
display: block !important;
}
}

@media (min-width: 992px) and (max-width: 1199px) {
.visible-md {
display: block !important;
}
}

        </style>
</head>

<body>

	<!-- Start Header Area -->

            <header class="header_area" id="header">
                    <div class="top_menu">
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-7">
                            <div class="float-left">
                              <p><a href="tel:+2348184751332" style="color:red"><i class="fa fa-phone"></i> +2348184751332</a> </p>
                              {{-- <p>email: info@eiser.com</p> --}}
                            </div>
                          </div>
                          <div class="col-md-5 hidden-sm hidden-xs hidden-md">
                            <div class="float-right">
                              <ul class="right_side">
                                {{-- <li>
                                  <a href="cart.html">
                                    gift card
                                  </a>
                                </li> --}}
                                {{-- <li>
                                  <a href="tracking.html">
                                    track order
                                  </a>
                                </li> --}}
                                <li>
                                  <a href="/contactus">
                                    Contact Us
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>



		<div class="container">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
                    {{-- <a href="/">	<h3>YUTH<span>CONNECT</span></h3></a> --}}
                    <a href="/">	<img src="{{ asset('main-logo.png') }}" alt="" style="height:4em"></a>

				</div>
				<nav id="nav-menu-container" style="padding-right:1em">
					<ul class="nav-menu">
						<li class="menu-active"><a href="/">Home</a></li>
						<li><a href="about.html">About</a></li>
                        <li><a href="/news">News & Events</a></li>

                        <li class="menu-has-children"><a href="#">Services</a>
							<ul>
								<li><a href="/delivery">Products & Delivery</a></li>
								<li><a href="event-details.html">Event Details</a></li>
								<li><a href="elements.html">Elements</a></li>
							</ul>
						</li>
						{{-- <li><a href="events.html">Events</a></li> --}}
						{{-- <li class="menu-has-children"><a href="#">News & Events</a>
							<ul>
								<li><a href="/news">News</a></li>
								<li><a href="/events">Events</a></li>
							</ul>
                        </li> --}}
                        {{-- <li><a href="/contactus">Contact</a></li> --}}
						<li class="menu-has-children"><a href="">Pages</a>
							<ul>
								<li><a href="donation.html">Donation</a></li>
								<li><a href="event-details.html">Event Details</a></li>
								<li><a href="elements.html">Elements</a></li>
							</ul>
                        </li>

                        <li class="visible-md visible-lg">           <a href="/contactus">
                            Contact Us
                          </a></li>
                        <script         src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                        <li class="menu-has-children">

                            @guest
                            <li>
                                <a  href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li>
                                    <a href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <a href="#" class="limitme sf-wit-ul">   {{ Auth::user()->name }}   </a>
                        <ul class="menu-has-children">
                                <li><a href="/dashboard">Dashboard</a></li>
                                       <li><a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                     </a>

                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                     </form>  </li>

                        @endguest
                    </ul>
            </li>

					</ul>
				</nav><!-- #nav-menu-container -->
			</div>
		</div>
	</header>






        <main class="py-4">



        {{-- <div id="page-header">
            </div> --}}
            {{-- <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br>   <br> --}}
                  <br>
            @yield('content')


        </main>

    </div>
    @include('layouts._footer')
{{--
	<!-- FOOTER -->
	<footer id="footer" class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- footer contact -->
				<div class="col-md-4">
					<div class="footer">
						<div class="footer-logo">
							<a class="logo" href="#"><img src="./img/logo.png" alt=""></a>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
						<ul class="footer-contact">
							<li><i class="fa fa-map-marker"></i> 2736 Hinkle Deegan Lake Road</li>
							<li><i class="fa fa-phone"></i> 607-279-9246</li>
							<li><i class="fa fa-envelope"></i> <a href="#">Charity@email.com</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer contact -->

				<!-- footer galery -->
				<div class="col-md-4">
					<div class="footer">
						<h3 class="footer-title">Galery</h3>
						<ul class="footer-galery">
							<li><a href="#"><img src="./img/galery-1.jpg" alt=""></a></li>
							<li><a href="#"><img src="./img/galery-2.jpg" alt=""></a></li>
							<li><a href="#"><img src="./img/galery-3.jpg" alt=""></a></li>
							<li><a href="#"><img src="./img/galery-4.jpg" alt=""></a></li>
							<li><a href="#"><img src="./img/galery-5.jpg" alt=""></a></li>
							<li><a href="#"><img src="./img/galery-6.jpg" alt=""></a></li>
						</ul>
					</div>
				</div>
				<!-- /footer galery -->

				<!-- footer newsletter -->
				<div class="col-md-4">
					<div class="footer">
						<h3 class="footer-title">Newsletter</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
						<form class="footer-newsletter">
							<input class="input" type="email" placeholder="Enter your email">
							<button class="primary-button">Subscribe</button>
						</form>
						<ul class="footer-social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
						</ul>
					</div>
				</div>
				<!-- /footer newsletter -->
			</div>
			<!-- /row -->

			<!-- footer copyright & nav -->
			<div id="footer-bottom" class="row">
				<div class="col-md-6 col-md-push-6">
					<ul class="footer-nav">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Causes</a></li>
						<li><a href="#">Events</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>

				<div class="col-md-6 col-md-pull-6">
					<div class="footer-copyright">
						<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
					</div>
				</div>
			</div>
			<!-- /footer copyright & nav -->
		</div>
		<!-- /container -->
	</footer>
	<!-- /FOOTER --> --}}

	<!-- jQuery Plugins -->

    <script src="{{ asset('theme/js/vendor/jquery-2.2.4.min.js') }}"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
	<script src="{{ asset('theme/js/vendor/bootstrap.min.js') }}"></script>

    <script src="{{ asset('theme/js/jquery.countdownTimer.js') }}"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{ asset('theme/js/easing.min.js') }}"></script>
	<script src="{{ asset('theme/js/hoverIntent.js') }}"></script>
	<script src="{{ asset('theme/js/superfish.min.js') }}"></script>
	<script src="{{ asset('theme/js/jquery.ajaxchimp.min.js') }}"></script>
	<script src="{{ asset('theme/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('theme/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('theme/js/jquery.sticky.js') }}"></script>
	<script src="{{ asset('theme/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('theme/js/waypoints.min.js') }}"></script>

	<script src="{{ asset('theme/js/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('theme/js/parallax.min.js') }}"></script>
	<script src="{{ asset('theme/js/mail-script.js') }}"></script>
    <script src="{{ asset('theme/js/main.js') }}"></script>



    @yield('fancyboxscript')
    @yield('extra-js')

    <script>

$(document).ready(function ()
   { $(".limitme").each(function(i){
        var len=$(this).text().trim().length;
        if(len>2)
        {
            // this trims the letters to show only 3alphabets
            $(this).text($(this).text().substr(0,6)+'');
        }
    });
 });




    </script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5d448a727d27204601c9037d/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}


    @yield('extra-imageselect-product')
<script>

$(document).ready(function () {
    $('ul#nav > li').click(function () {
        selfActive = $(this).hasClass("show")?true:false;
        $(".show").removeClass("show").find("ul").slideUp();
        if (!selfActive){
            $(this).addClass("show").find("ul").slideDown();
        }

    });
});
</script>
</body>
</html>
