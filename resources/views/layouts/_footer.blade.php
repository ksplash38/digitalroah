
	{{-- <link rel="stylesheet" href="{{ asset('footer/demo.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('footer/footer-distributed-with-address-and-phones.css') }}">


    <!-- The content of your page would go here. -->

    <footer class="footer-distributed">

			<div class="footer-left">

				<h3>DIGITAL<span style="color:#5cb85c">ROAH</span></h3>

				<p class="footer-links">
					<a href="/">Home</a>
					·
					<a href="/news">News</a>
					{{-- ·
					<a href="#">Vote</a>
					·
					<a href="#">About</a>
					·
					<a href="#">Faq</a>
					·
					<a href="#">Contact</a> --}}
				</p>

				<p class="footer-company-name">Digitalroah &copy; 2015</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Eutan, Jos</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>08184751332</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p ><a href="mailto:support@digitalroahservices.com.ng" style="color:#5cb85c">support@digitalroahservices.com.ng</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>About DR</span>
					We are bent on giving our clients explicit services & maximum satisfaction.
				</p>

				<div class="footer-icons">

					<a target="_blank" href="https://web.facebook.com/pg/digitalroah/posts/"><i class="fa fa-facebook"></i></a>
					<a  target="_blank" href="https://www.instagram.com/official_digitalroah/"><i class="fa fa-instagram"></i></a>
					{{-- <a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a> --}}

				</div>

			</div>

		</footer>
