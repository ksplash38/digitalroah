@extends('layouts.app')

@section('content')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<style>

.panel
{
    text-align: center;
}
.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
.panel-body
{
    padding: 0px;
    text-align: center;
}

.the-price
{
    background-color: rgba(220,220,220,.17);
    box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
    padding: 20px;
    margin: 0;
}

.the-price h1
{
    line-height: 1em;
    padding: 0;
    margin: 0;
}

.subscript
{
    font-size: 25px;
}

/* CSS-only ribbon styles    */
.cnrflash
{
    /*Position correctly within container*/
    position: absolute;
    top: -9px;
    right: 4px;
    z-index: 1; /*Set overflow to hidden, to mask inner square*/
    overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
    width: 100px;
    height: 100px;
    border-radius: 3px 5px 3px 0;
}


.cnrflash-ultimate
    {
        /*Set position, make larger then 			container and rotate 45 degrees*/
        position: absolute;
        bottom: 0;
        right: 0;
        width: 145px;
        height: 145px;
        -ms-transform: rotate(45deg); /* IE 9 */
        -o-transform: rotate(45deg); /* Opera */
        -moz-transform: rotate(45deg); /* Firefox */
        -webkit-transform: rotate(45deg); /* Safari and Chrome */
        -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
        -ms-transform-origin: 100% 100%;  /* IE 9 */
        -o-transform-origin: 100% 100%; /* Opera */
        -moz-transform-origin: 100% 100%; /* Firefox */
        background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
        background-size: 4px,auto, auto,auto;
        background-color: rgb(240,230,140);
        box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
    }

.cnrflash-inner
{
    /*Set position, make larger then 			container and rotate 45 degrees*/
    position: absolute;
    bottom: 0;
    right: 0;
    width: 145px;
    height: 145px;
    -ms-transform: rotate(45deg); /* IE 9 */
    -o-transform: rotate(45deg); /* Opera */
    -moz-transform: rotate(45deg); /* Firefox */
    -webkit-transform: rotate(45deg); /* Safari and Chrome */
    -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
    -ms-transform-origin: 100% 100%;  /* IE 9 */
    -o-transform-origin: 100% 100%; /* Opera */
    -moz-transform-origin: 100% 100%; /* Firefox */
    background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
    background-size: 4px,auto, auto,auto;
    background-color: #aa0101;
    box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
}
.cnrflash-inner:before, .cnrflash-inner:after
{
    /*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
    content: " ";
    display: block;
    position: absolute;
    bottom: -16px;
    width: 0;
    height: 0;
    border: 8px solid #800000;
}
.cnrflash-inner:before
{
    left: 1px;
    border-bottom-color: transparent;
    border-right-color: transparent;
}
.cnrflash-inner:after
{
    right: 0;
    border-bottom-color: transparent;
    border-left-color: transparent;
}
.cnrflash-label
{
    /*Make the label look nice*/
    position: absolute;
    bottom: 0;
    left: 0;
    display: block;
    width: 100%;
    padding-bottom: 5px;
    color: #fff;
    text-shadow: 0 1px 1px rgba(1,1,1,.8);
    font-size: 0.95em;
    font-weight: bold;
    text-align: center;
}

</style>

	<!-- Start Causes Area -->
	<section class="causes-area section-gap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 section-title">
                        <p class="search-results-count">{{ $states->count() }} people / person going from '{{ request()->input('stateFrom') }}' to '{{ request()->input('stateTo') }}' </p>
					<h2>WE ARE SORRY</h2>
					<p>
					We are sorry you have used up all your chances of connecting to a partner. Please choose a  plan to connect to a partner.
					</p>
				</div>
			</div>



            <div class="row  ">
                    <div class=" col-md-offset-2 col-xs-12 col-md-3">
                        <div class="panel panel-primary">
                                <div class="single-cause">
                                        <div class="top">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color:#cd7f32">
                                    Bronze</h3>
                            </div>
                            <div class="panel-body">
                                <div class="the-price">
                                    <h1 style="color:#cd7f32">
                                            ₦500<span class="subscript">/1mo</span></h1>
                                    {{-- <small>1 month FREE trial</small> --}}
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                            3 Attempts
                                        </td>
                                    </tr>
                                    {{-- <tr class="active">
                                        <td>
                                            1 Project
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            100K API Access
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            100MB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr> --}}
                                </table>
                            </div>
                            <div class="panel-footer"  >
                                <a href="#" class="btn btn-success" role="button">GET ME</a>
                                </div>
                        </div>
                    </div>
                    </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="panel panel-success" style="padding-bottom:2em">
                            <div class="cnrflash">
                                <div class="cnrflash-inner">
                                    <span class="cnrflash-label">MOST
                                        <br>
                                        POPULR</span>
                                </div>
                            </div>
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color:silver">
                                    Silver</h3>
                            </div>
                            <div class="panel-body">
                                <div class="the-price">
                                    <h1 style="color:silver">
                                            ₦1,000<span class="subscript">/1mo</span></h1>
                                    {{-- <small>1 month FREE trial</small> --}}
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                                5 Attempts
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Live Customer Care Support / Services
                                        </td>
                                    </tr>
                                    {{-- <tr>
                                        <td>
                                            100K API Access
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            200MB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr> --}}
                                </table>
                            </div>
                            <div class="panel-footer">
                                    <a href="#" class="btn btn-success" role="button">GET ME</a>
                                    </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <div class="panel panel-info" style="padding-bottom:1em">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="color:rgb(185,242,255)">
                                    Diamond</h3>
                            </div>
                            <div class="panel-body">
                                <div class="the-price">
                                    <h1 style="color:rgb(185,242,255)">
                                            ₦2,000<span class="subscript">/1mo</span></h1>
                                    {{-- <small>1 month FREE trial</small> --}}
                                </div>
                                <table class="table">
                                    <tr>
                                        <td>
                                                10 Attempts
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                                Live Customer Care Support / Services
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                Live Chat with Medical Doctors
                                        </td>
                                    </tr>
                                    {{-- <tr class="active">
                                        <td>
                                            500MB Storage
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Custom Cloud Services
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <td>
                                            Weekly Reports
                                        </td>
                                    </tr> --}}
                                </table>
                            </div>
                            <div class="panel-footer">
                                    <a href="#" class="btn btn-success" role="button">GET ME</a>
                                    </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-3">
                            <div class="panel panel-info" style="padding-bottom:1em">
                                    <div class="cnrflash">
                                            <div class="cnrflash-ultimate" >
                                                <span class="cnrflash-label">ULTIMATE
                                                 </span>
                                            </div>
                                        </div>
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        Gold</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="the-price">
                                        <h1>
                                                ₦5,000<span class="subscript">/2mo</span></h1>
                                        {{-- <small>1 month FREE trial</small> --}}
                                    </div>
                                    <table class="table">
                                        <tr>
                                            <td>
                                                    Infinity Attempts
                                            </td>
                                        </tr>
                                        <tr class="active">
                                            <td>
                                                    Live Customer Care Support / Services
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    Transportation to Camp
                                            </td>
                                        </tr>
                                        <tr>
                                                <td>
                                                        Food on Arrival
                                                </td>
                                            </tr>
                                        {{-- <tr class="active">
                                            <td>
                                                500MB Storage
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Custom Cloud Services
                                            </td>
                                        </tr>
                                        <tr class="active">
                                            <td>
                                                Weekly Reports
                                            </td>
                                        </tr> --}}
                                    </table>
                                </div>
                                <div class="panel-footer">
                                        <a href="#" class="btn btn-success" role="button">GET ME</a>
                                        </div>
                            </div>
                        </div>
                </div>

		</div>
	</section>
	<!-- end Causes Area -->


@endsection
