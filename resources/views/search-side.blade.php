@extends('layouts.app')
@section('content')
<div class="container">
        <br>  <br>  <br>
  <h1>Search Results</h1>
  <p class="search-results-count">{{ $states->count() }} Person(s) / People going from '{{ request()->input('stateFrom') }}' to '{{ request()->input('stateTo') }}' </p>
  @if(!empty ($states))

   <div class="featured-section">

       <div class="container ">


           <div class="table-responsive products  text-center" >
                @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
             @endif
                <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Mobile Number</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                                @foreach ($states as $state)
                                <td>
                                        {{$state->name}}

                               </td>

                                <td>
                                        {{$state->email}}

                              </td>
                              <td>
                                    {{$state->mobilenumber}}

                          </td>

                          <td>

                                <a class="btn btn-info" href="/searcheduserprofile/{{$state->unique_id}}">View Profile</a>


                      </td>

                          {{-- <td>
                                @if (Session::has('message'))


                                <a class="btn btn-warning pushme-with-color" style="color:white" href="/sendrequest/{{$state->id}}"  disabled>Pending Request...</a>

                                @elseif(Session::has('sentfriend'))
                                <a class="btn btn-warning pushme-with-color" style="color:white" href="/sendrequest/{{$state->id}}"  disabled>Pending Request...</a>

                                @else

                                <a class="btn btn-info pushme-with-color" style="color:white" href="/sendrequest/{{$state->id}}"  >Send Request</a>
                             @endif

                      </td> --}}
                          <td>

                                <a class="btn btn-success" href="https://api.whatsapp.com/send?phone={{$state->mobilenumber}}&text=Hi!!%20my%20name%20is%20{{$state->name}}.%20I%20am%20in%20from%20DigitalRoah%20and%20would%20like%20to%20connect%20with%20you" target="_blank">Message</a>


                      </td>



                                               @endforeach

                          </tr>

                        </tbody>
                      </table>





           </div> <!-- end products -->



       </div> <!-- end container -->



   </div>

 @else

 <h1>SORRY THERE IS CURRENTLY NO ONE GOING TO YOUR ROUTE YET. PLEASE CHECK BACK</h1>

    @endif



  </div>


<script>

$(document).ready(function(){

    // $(".pushme").click(function () {
    //    $(this).text("DON'T PUSH ME");
    // });

    $(".pushme-with-color").click(function () {
       $(this).text("Pending...");
       $(this).addClass("btn-warning");
       $(this).removeClass("btn-default");
    });

    // $(".with-color").click(function () {
    //    if($(this).hasClass("btn-warning"))
    //    {
    //    		$(this).addClass("btn-danger");
    //    		$(this).removeClass("btn-warning");
    //    }
    //    else{
    //    		$(this).addClass("btn-warning");
    //    		$(this).removeClass("btn-danger");
    //    }
    // });

    // $(".pushme2").click(function(){
	// 	$(this).text(function(i, v){
	// 	   return v === 'PUSH ME' ? 'PULL ME' : 'PUSH ME'
	// 	});
    // });
});
</script>

@endsection
