@extends('layouts.app')

@section('og')

<meta property="og:url"     itemprop="url"      content="{{Request::url()}}"/>
<meta property="og:price"     itemprop="price"      content="₦ {{$product->price}}"/>
<meta property="og:site_name" content="Digitalroah services">
<meta property="og:type"   itemprop="type"         content="website" />
<meta property="og:title"   itemprop="title"        content="{{$product->name}}" />
<meta property="og:description" itemprop="description"    content=" {{$product->description}}" />
@if (!empty($product->image))

<meta property="og:image"   itemprop="image"      content="{{ productImages($product->image) }}" />
                      {{-- <a  class="fancybox"  data-fancybox="images" href="{{ productImages($image) }}" > --}}
@endif
@endsection

@section('content')
<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ccfcb1d84760c0019807a78&product=inline-share-buttons"></script>
<style>



.fa-stack[data-count]:after{
  position:absolute;
  right:0%;
  top:0%;
  content: attr(data-count);
  font-size:40%;
  padding:.6em;
  border-radius:999px;
  line-height:.75em;
  color: white;
  color:#DF0000;
  text-align:center;
  min-width:2em;
  font-weight:bold;
  background: white;
  border-style:solid;
}
.fa-circle {
  color:#DF0000;
}

.red-cart {
	color: #DF0000; background:white;
}




.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  /* max-width: 300px; */
  border-start-end-radius: 2em;
  margin: auto;
  text-align: center;
  font-family: arial;
}

.price {
  color: grey;
  font-size: 15px;
}

.card button {
  border: none;
  outline: 0;
  padding: 12px;
  color: white;
  background-color: #5cb85c;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 18px;
}

.card button:hover {
  opacity: 0.7;
}
.owl-theme .owl-dots .owl-dot{
    display:none;
}


 

.product-section {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 100px;
  padding: 100px 0 120px;
}



.product-section-images {
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  grid-gap: 5px;
  margin-top: 15px;
}

.product-section-thumbnail {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border: 1px solid lightgray;
  /* min-height: 66px; */
 
  cursor: pointer;
  object-fit: contain;
}

.product-section-thumbnail img{
  
height: 100%;
  width:100%;
}


.product-section-thumbnail:hover {
  border: 1px solid #979797;
}
.selectedthumbnail {
  border: 1px solid green;
}

.product-section-image {
     
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  border: 1px solid green;
  padding: 10px 10px 10px 10px;
  text-align: center;
  height: 300px;
}

.product-section-image img {
  opacity: 0;
  -webkit-transition: opacity .10s ease-in-out;
  transition: opacity .10s ease-in-out;
  max-height: 100%;
  width:100%;
  
}

.product-section-image img.activeselect {
  opacity: 1;
}

                    
                    </style>

 



    <!--================Home Banner Area =================-->
    <section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="container">
          <div class="banner_content d-md-flex justify-content-between align-items-center">
         
            <div class="page_link mb-3 mb-md-0">
                    <a href="/">Home</a>
                    <a href="{{ URL::previous() }}">Previous</a>
            </div>



            <a href="{{ route('cart.index') }}">
            @if (Cart::instance('default')->count() > 0)
            <span class="fa-stack fa-2x has-badge" data-count="{{ Cart::content('default')->count() }}">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                  </span>
            {{-- <span class="cart-count">  <i class="fa fa-shopping-cart"></i><span class="badge">{{ Cart::content('default')->count() }}</span></span> --}}
            @endif
            </a>
          </div>
        </div>
      </div>
    </section>
    <!--================End Home Banner Area =================-->
    @if(session()->has('success_message'))
    <div class="alert alert-success">
        {{ session()->get('success_message') }}
    </div>
@endif






    <!--================Single Product Area =================-->
    <div class="container product_image_area">
      <div class="container">
        <div class="row s_product_inner">
          <div class="col-lg-6">
 
                <div class="product-section-image">
                        <img src="{{ productImage($product->image) }}" alt="{{$product->name}}" class="activeselect" id="currentImage">
                    </div>

                    <br>

                    <div class="product-section-images">
                      <div class="product-section-thumbnail  selectedthumbnail"     >
                            <img src="{{ productImage($product->image) }}" alt="{{$product->name}}" />
                      </div>
        
                        @if ($product->images)
                            @foreach (json_decode($product->images, true) as $image)
                            <div class="product-section-thumbnail"  >

                                <img src="{{ productImage($image) }}" alt="{{$product->name}}"  /> 
                         
                             </div>
                             @endforeach
                       @endif
                    </div>
                
{{-- 
            <div class="s_product_img">
              <div
                id="carouselExampleIndicators"
                class="carousel slide"
                data-ride="carousel"
              >
                <ol class="carousel-indicators">
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="0"
                    class="active"
                  >
      
                    <img
                      src="img/product/single-product/s-product-s-2.jpg"
                      alt=""
                    />
                  </li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="1"
                  >
                    <img
                      src="img/product/single-product/s-product-s-3.jpg"
                      alt=""
                    />
                  </li>
                  <li
                    data-target="#carouselExampleIndicators"
                    data-slide-to="2"
                  >
                    <img
                      src="img/product/single-product/s-product-s-4.jpg"
                      alt=""
                    />
                  </li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img
                      class="d-block w-100"
                      src="img/product/single-product/s-product-1.jpg"
                      alt="First slide"
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      class="d-block w-100"
                      src="img/product/single-product/s-product-1.jpg"
                      alt="Second slide"
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      class="d-block w-100"
                      src="img/product/single-product/s-product-1.jpg"
                      alt="Third slide"
                    />
                  </div>
                </div>
              </div>
            </div> --}}
          </div>
          <div class="col-lg-5 offset-lg-1">
            <div class="s_product_text">
              <h3>{{$product->name}}</h3>
            <h2>₦ {{$product->price}}</h2>
              <ul class="list">
                <li>
                  <a class="active" >

                    <span>Company</span>: {{$productname->title}} </a>
                </li>
                <li>
                   <span>Availibility</span> : {!! $stockLevel !!}
                </li>
              </ul>
              <p>
                {{$product->description}}
              </p>
            
 

              <form action="{{ route('cart.store') }}" method="POST">
                    {{ csrf_field() }}
                    <label for="qty">Quantity:</label>

                    <select   style="width:15em" class="form-control sorting" name="quantity" id="category">

                            @for ($i = 1; $i < $product->quantity + 1 ; $i++)
                            {{-- <option >{{ $i }}</option>  --}}
                      
                            <option> {{ $i }}</option>  
                          
                            @endfor

                      </select>


               
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    <input type="hidden" name="name" value="{{ $product->name }}">
                    <input type="hidden" name="price" value="{{ $product->price }}">
                
     
                    <br><br>
                    <div class="card_area">
                  <button type="submit" class=" main_btn btn btn-success">  <i class="fa fa-shopping-cart"></i>Add to Cart</button>
                </div>
                </form>

                <div class="sharethis-inline-share-buttons"></div>
 
            </div>
          </div>
        </div>
      </div>
      
    </div>
 
<br><br><br>
<section class="product_description_area">
        <div class="container"> 
        
    
                <div  style="background-color:#5cb85c">
                        <h3 class="text-center" style="color:white; margin-top:-2em">Sponsored</h3>
                    </div>
                    <br><br>
                
                   <div class="owl-carousel-products  ">
                                 @foreach ($sponsored as $product)
                                 <div class="item">        
                     <div class="card" >
                       <div class="product-image">
                           
                             <a href="{{ route('product.show', [$product->randomid]) }}"><img src="{{ productImage($product->image) }}" alt="product" class="image"   style="width: 100%; height: 168px"></a>
                             <hr>
                         </div>
                     
                       <div class="product-info">
                       <h3 class="limitme2">{{$product->name}}</h3>
                             <p class="price">{{Presentprice($product->price)}}</p>
                
                             <p class="limit3">{{$product->description}}</p>
                             <a href="{{ route('product.show', [$product->randomid]) }}">  <button>ORDER</button>  </a>
                       </div>
                
                     </div></div>
                     @endforeach  
                 </div> 
        </div>
        </section>

    <section class="product_description_area">
    <div class="container"> 
    

            <div  style="background-color:#5cb85c">
                    <h3 class="text-center" style="color:white; margin-top:-2em">Might Also Like</h3>
                </div>
                <br><br>
            
               <div class="owl-carousel-products  ">
                             @foreach ($mightAlsoLike as $product)
                             <div class="item">        
                 <div class="card" >
                   <div class="product-image">
                       
                         <a href="{{ route('product.show', [$product->randomid]) }}"><img src="{{ productImage($product->image) }}" alt="product" class="image"   style="width: 100%; height: 168px"></a>
                         <hr>
                     </div>
                 
                   <div class="product-info">
                   <h3 class="limitme2">{{$product->name}}</h3>
                         <p class="price">{{Presentprice($product->price)}}</p>
            
                         <p class="limit3">{{$product->description}}</p>
                         <a href="{{ route('product.show', [$product->randomid]) }}">  <button>ORDER</button>  </a>
                   </div>
            
                 </div></div>
                 @endforeach  
             </div> 
    </div>
    </section>
    <!--================End Single Product Area =================-->

    <!--================Product Description Area =================-->
    {{-- <section class="product_description_area">
      <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a
              class="nav-link"
              id="home-tab"
              data-toggle="tab"
              href="#home"
              role="tab"
              aria-controls="home"
              aria-selected="true"
              >Description</a
            >
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              id="profile-tab"
              data-toggle="tab"
              href="#profile"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
              >Specification</a
            >
          </li>
          <li class="nav-item">
            <a
              class="nav-link"
              id="contact-tab"
              data-toggle="tab"
              href="#contact"
              role="tab"
              aria-controls="contact"
              aria-selected="false"
              >Comments</a
            >
          </li>
          <li class="nav-item">
            <a
              class="nav-link active"
              id="review-tab"
              data-toggle="tab"
              href="#review"
              role="tab"
              aria-controls="review"
              aria-selected="false"
              >Reviews</a
            >
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div
            class="tab-pane fade"
            id="home"
            role="tabpanel"
            aria-labelledby="home-tab"
          >
            <p>
              Beryl Cook is one of Britain’s most talented and amusing artists
              .Beryl’s pictures feature women of all shapes and sizes enjoying
              themselves .Born between the two world wars, Beryl Cook eventually
              left Kendrick School in Reading at the age of 15, where she went
              to secretarial school and then into an insurance office. After
              moving to London and then Hampton, she eventually married her next
              door neighbour from Reading, John Cook. He was an officer in the
              Merchant Navy and after he left the sea in 1956, they bought a pub
              for a year before John took a job in Southern Rhodesia with a
              motor company. Beryl bought their young son a box of watercolours,
              and when showing him how to use it, she decided that she herself
              quite enjoyed painting. John subsequently bought her a child’s
              painting set for her birthday and it was with this that she
              produced her first significant work, a half-length portrait of a
              dark-skinned lady with a vacant expression and large drooping
              breasts. It was aptly named ‘Hangover’ by Beryl’s husband and
            </p>
            <p>
              It is often frustrating to attempt to plan meals that are designed
              for one. Despite this fact, we are seeing more and more recipe
              books and Internet websites that are dedicated to the act of
              cooking for one. Divorce and the death of spouses or grown
              children leaving for college are all reasons that someone
              accustomed to cooking for more than one would suddenly need to
              learn how to adjust all the cooking practices utilized before into
              a streamlined plan of cooking that is more efficient for one
              person creating less
            </p>
          </div>
          <div
            class="tab-pane fade"
            id="profile"
            role="tabpanel"
            aria-labelledby="profile-tab"
          >
            <div class="table-responsive">
              <table class="table">
                <tbody>
                  <tr>
                    <td>
                      <h5>Width</h5>
                    </td>
                    <td>
                      <h5>128mm</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Height</h5>
                    </td>
                    <td>
                      <h5>508mm</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Depth</h5>
                    </td>
                    <td>
                      <h5>85mm</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Weight</h5>
                    </td>
                    <td>
                      <h5>52gm</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Quality checking</h5>
                    </td>
                    <td>
                      <h5>yes</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Freshness Duration</h5>
                    </td>
                    <td>
                      <h5>03days</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>When packeting</h5>
                    </td>
                    <td>
                      <h5>Without touch of hand</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h5>Each Box contains</h5>
                    </td>
                    <td>
                      <h5>60pcs</h5>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div
            class="tab-pane fade"
            id="contact"
            role="tabpanel"
            aria-labelledby="contact-tab"
          >
            <div class="row">
              <div class="col-lg-6">
                <div class="comment_list">
                  <div class="review_item">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-1.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <h5>12th Feb, 2017 at 05:56 pm</h5>
                        <a class="reply_btn" href="#">Reply</a>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                  <div class="review_item reply">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-2.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <h5>12th Feb, 2017 at 05:56 pm</h5>
                        <a class="reply_btn" href="#">Reply</a>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                  <div class="review_item">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-3.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <h5>12th Feb, 2017 at 05:56 pm</h5>
                        <a class="reply_btn" href="#">Reply</a>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="review_box">
                  <h4>Post a comment</h4>
                  <form
                    class="row contact_form"
                    action="contact_process.php"
                    method="post"
                    id="contactForm"
                    novalidate="novalidate"
                  >
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="text"
                          class="form-control"
                          id="name"
                          name="name"
                          placeholder="Your Full name"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="email"
                          class="form-control"
                          id="email"
                          name="email"
                          placeholder="Email Address"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="text"
                          class="form-control"
                          id="number"
                          name="number"
                          placeholder="Phone Number"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea
                          class="form-control"
                          name="message"
                          id="message"
                          rows="1"
                          placeholder="Message"
                        ></textarea>
                      </div>
                    </div>
                    <div class="col-md-12 text-right">
                      <button
                        type="submit"
                        value="submit"
                        class="btn submit_btn"
                      >
                        Submit Now
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div
            class="tab-pane fade show active"
            id="review"
            role="tabpanel"
            aria-labelledby="review-tab"
          >
            <div class="row">
              <div class="col-lg-6">
                <div class="row total_rate">
                  <div class="col-6">
                    <div class="box_total">
                      <h5>Overall</h5>
                      <h4>4.0</h4>
                      <h6>(03 Reviews)</h6>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="rating_list">
                      <h3>Based on 3 Reviews</h3>
                      <ul class="list">
                        <li>
                          <a href="#"
                            >5 Star
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> 01</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            >4 Star
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> 01</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            >3 Star
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> 01</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            >2 Star
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> 01</a
                          >
                        </li>
                        <li>
                          <a href="#"
                            >1 Star
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> 01</a
                          >
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="review_list">
                  <div class="review_item">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-1.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                  <div class="review_item">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-2.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                  <div class="review_item">
                    <div class="media">
                      <div class="d-flex">
                        <img
                          src="img/product/single-product/review-3.png"
                          alt=""
                        />
                      </div>
                      <div class="media-body">
                        <h4>Blake Ruiz</h4>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="review_box">
                  <h4>Add a Review</h4>
                  <p>Your Rating:</p>
                  <ul class="list">
                    <li>
                      <a href="#">
                        <i class="fa fa-star"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-star"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-star"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-star"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-star"></i>
                      </a>
                    </li>
                  </ul>
                  <p>Outstanding</p>
                  <form
                    class="row contact_form"
                    action="contact_process.php"
                    method="post"
                    id="contactForm"
                    novalidate="novalidate"
                  >
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="text"
                          class="form-control"
                          id="name"
                          name="name"
                          placeholder="Your Full name"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="email"
                          class="form-control"
                          id="email"
                          name="email"
                          placeholder="Email Address"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input
                          type="text"
                          class="form-control"
                          id="number"
                          name="number"
                          placeholder="Phone Number"
                        />
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea
                          class="form-control"
                          name="message"
                          id="message"
                          rows="1"
                          placeholder="Review"
                        ></textarea>
                      </div>
                    </div>
                    <div class="col-md-12 text-right">
                      <button
                        type="submit"
                        value="submit"
                        class="btn submit_btn"
                      >
                        Submit Now
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> --}}

 
 

      

     
 
           <!-- It's likely you'll need to link the card somewhere. You could add a button in the info, link the titles, or even wrap the entire card in an <a href="..."> -->
             

               <!-- more products -->
            
 

@endsection
@section('extra-imageselect-product')
    <!--================End Product Description Area =================-->
    {{-- <script src="{{ asset('owl/docs/assets/owlcarousel/owl.carousel.js')}}"></script> --}}
 
   
    <script>

            $(function() {
              $('.owl-carousel-products').owlCarousel({         
                nav:false,
                autoplay:true,
                autoplayHoverPause:true,
               
                loop: true,
             
                stagePadding: 50,
                responsive:{
                                0:{
                                    items:1,
                                    margin: 10,
                                },
                                400:{
                                    margin: 10,
                                    items:3
                                },
                                1000:{
                                    margin: 10,
                                    items:4
                                }
                            }
                
              });
            });
            
                        $(document).ready(function ()
                           { $(".limitme2").each(function(i){
                                var len=$(this).text().trim().length;
                                if(len>2)
                                {
                                    // this trims the letters to show only 3alphabets
                                    $(this).text($(this).text().substr(0,11)+'');
                                }
                            });
                         });
            
                         
                        $(document).ready(function ()
                           { $(".limit3").each(function(i){
                                var len=$(this).text().trim().length;
                                if(len>2)
                                {
                                    // this trims the letters to show only 3alphabets
                                    $(this).text($(this).text().substr(0,20)+'');
                                }
                            });
                         });
                        
                        
                      
                         
                            </script>
<script>

        (function(){
            const currentImage = document.querySelector('#currentImage');
            const images = document.querySelectorAll('.product-section-thumbnail');

            images.forEach((element) => element.addEventListener('click', thumbnailClick));

            function thumbnailClick(e) {
    

                // currentImage.src = this.querySelector('img').src;
            //    console.log(e);
                currentImage.classList.remove('activeselect');

                currentImage.addEventListener('transitionend', () => {
                    currentImage.src = this.querySelector('img').src;
                    currentImage.classList.add('activeselect');
                })

                images.forEach((element) => element.classList.remove('selectedthumbnail'));
                this.classList.add('selectedthumbnail');
            }

        })();

</script>


<script>
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
});
</script>


<!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
<!-- <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>



@endsection
