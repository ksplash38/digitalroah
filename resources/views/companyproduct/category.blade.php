@extends('layouts.app')
@section('content')
<style>
        .fa-stack[data-count]:after{
          position:absolute;
          right:0%;
          top:0%;
          content: attr(data-count);
          font-size:40%;
          padding:.6em;
          border-radius:999px;
          line-height:.75em;
          color: white;
          color:#DF0000;
          text-align:center;
          min-width:2em;
          font-weight:bold;
          background: white;
          border-style:solid;
        }
        .fa-circle {
          color:#DF0000;
        }
        
        .red-cart {
            color: #DF0000; background:white;
        }
        
        </style>
    <!--================Home Banner Area =================-->
    <section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="container">
          <div class="banner_content d-md-flex justify-content-between align-items-center">
            <div class="mb-3 mb-md-0">

              <h2>{{$productname->title}}</h2>
              <p>Always ready to serve you</p>
            </div>
            <div class="page_link">
              <a href="/">Home</a>
            <a href="{{URL::previous()}}">prev</a>
           
            </div>
            <a href="{{ route('cart.index') }}">
                    @if (Cart::instance('default')->count() > 0)
                    <span class="fa-stack fa-2x has-badge" data-count="{{ Cart::content('default')->count() }}">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                          </span>
                    {{-- <span class="cart-count">  <i class="fa fa-shopping-cart"></i><span class="badge">{{ Cart::content('default')->count() }}</span></span> --}}
                    @endif
                    </a>
          </div>
        </div>
      </div>
    </section>
    <!--================End Home Banner Area =================-->
    {{-- @if (!empty($products))
    @foreach ($products as $product)
    {{$product->name}}
    @endforeach
    @endif --}}

    <!--================Category Product Area =================-->



    <section class="cat_product_area sectio n_gap">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-9">
            <div class="product_top_bar">
                    <form action="#" method="POST" name="sub">
                            <div class="left_dorp">
                            <select   style="width:15em" class="form-control sorting" name="category" id="category">
                                  <option>-- SELECT LOCATION --</option>
                                  @foreach ($presentlocations as $presentlocation)

                                  <option value='{{ $presentlocation->id }}'>{{ $presentlocation->presentlocation_name }}</option>
                                  @endforeach



                            </select>




                                              <select style="width:15em" class="form-control show product-name ex2" id="subcategory"  onchange="location = this.value;">

                                              </select>
                            </div>

                                                  </form>




              {{-- <div class="left_dorp">
                <select class="sorting">
                  <option value="1">Default sorting</option>
                  <option value="2">Default sorting 01</option>
                  <option value="4">Default sorting 02</option>
                </select>
                <select class="show">
                  <option value="1">Show 12</option>
                  <option value="2">Show 14</option>
                  <option value="4">Show 16</option>
                </select>
              </div> --}}
            </div>

            <div class="latest_product_inner">
              <div class="row">
                    @foreach ($products as $item)
 
<a href="{{ route('product.show', [$item->randomid]) }} ">
                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img style="height:10em"
                        class="card-img"
                        src="{{ Voyager::image($item->image) }}"
                        alt=""
                      />
                      <div class="p_icon">
                        <div style="color:white">
                                {{$item->description}} 
                        </div>  
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>{{$item->name}}</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">₦ {{number_format($item->price)}}</span>
                        @if (!empty($item->oldprice))
                        <del>₦ {{number_format($item->oldprice)}}</del>
                        @endif
                  
                      </div>
                    </div>
                  </div>

                </div>
            </a>
 @endforeach
                {{-- <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i2.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i3.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i4.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i5.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i6.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i7.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i8.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-lg-4 col-md-6">
                  <div class="single-product">
                    <div class="product-img">
                      <img
                        class="card-img"
                        src="img/product/inspired-product/i2.jpg"
                        alt=""
                      />
                      <div class="p_icon">
                        <a href="#">
                          <i class="ti-eye"></i>
                        </a>
                        <a href="#">
                          <i class="ti-heart"></i>
                        </a>
                        <a href="#">
                          <i class="ti-shopping-cart"></i>
                        </a>
                      </div>
                    </div>
                    <div class="product-btm">
                      <a href="#" class="d-block">
                        <h4>Latest men’s sneaker</h4>
                      </a>
                      <div class="mt-3">
                        <span class="mr-4">$25.00</span>
                        <del>$35.00</del>
                      </div>
                    </div>
                  </div>
                </div> --}}
              </div>
            </div>
          </div>




     


          <div class="col-lg-3">

             
                        <aside class="left_widgets p_filter_widgets">
      
                              <div class="l_w_title">
                                      <h3>Filter</h3>
                                    </div>
                  
                                    <div class="widgets_inner">
                  
                        <!-- <form action="{{URL::current()}}" method="GET"> -->
                  
                            <form action="/category-search" method="GET">
                  
                  
                          <div class="input-group input-group-sm mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm">Min ₦</span>
                          </div>
                          <input  type="number" class="form-control" name="min_price"   aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                        </div>
                  
                        <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm ">Max ₦</span>
                        </div>
                        <input  type="number" class="form-control" name="max_price"   aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                        </div>
                   
                  
                  
                  
                  
                  
                        <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroup-sizing-sm  ">Company</span>
                        </div>
                        <select name="company" id="" class="form-control">
                              <option value=''>None</option>
                        @foreach($companies as $company)
      
              <option value='{{ $company->title }}'>{{ $company->title }}</option>
      
                        @endforeach
                      </select>
                        {{-- <input  type="text" class="form-control" name="brand_name" value="{{ request()->input('brand_name') }}"  aria-label="Small" aria-describedby="inputGroup-sizing-sm"> --}}
                        </div>
                  
                        
                        <div class="input-group input-group-sm mb-3 ol-lg-12">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-sm  ">Location</span>
                          </div>
      
                          <select name="location" id="" class="form-control">
                                  <option value=''>None</option>
                                  @foreach($presentlocations as $location)
                
                        <option value='{{ $location->presentlocation_name }}'>{{ $location->presentlocation_name }}</option>
                
                                  @endforeach
                                </select>
                          {{-- <input  type="text" class="form-control" name="state" value="{{ request()->input('state') }}"  aria-label="Small" aria-describedby="inputGroup-sizing-sm"> --}}
                          </div>
                  
                          {{-- <div class="input-group input-group-sm mb-3 ol-lg-12">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm  ">Country</span>
                              </div>
                              <input  type="text" class="form-control" name="country" value="{{ request()->input('country') }}"  aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                              </div> --}}
                  
                              <div class="input-group input-group-sm mb-3 ol-lg-12">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-sm  ">Keyword</span>
                                  </div>
                                  <input  type="text" class="form-control" name="keyword" value="{{ request()->input('keyword') }}"  aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                  </div>
                   
                  <br />
                            <input type="submit" class="btn btn-success" value="Search">
                        </form>
                  
                      </div>
                  
                  
                        </aside>  
            {{-- <div class="left_sidebar_area">
              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Browse Categories</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                        @foreach ($presentlocations as $presentlocation)
                    <li>
                       

                            <a href="#">{{ $presentlocation->presentlocation_name }}</a>   
                       
                    </li>
                    @endforeach
                  
                  </ul>
                </div>
              </aside>

              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Product Brand</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    <li>
                      <a href="#">Apple</a>
                    </li>
                    <li>
                      <a href="#">Asus</a>
                    </li>
                    <li class="active">
                      <a href="#">Gionee</a>
                    </li>
                    <li>
                      <a href="#">Micromax</a>
                    </li>
                    <li>
                      <a href="#">Samsung</a>
                    </li>
                  </ul>
                </div>
              </aside>
 
              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Color Filter</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    <li>
                      <a href="#">Black</a>
                    </li>
                    <li>
                      <a href="#">Black Leather</a>
                    </li>
                    <li class="active">
                      <a href="#">Black with red</a>
                    </li>
                    <li>
                      <a href="#">Gold</a>
                    </li>
                    <li>
                      <a href="#">Spacegrey</a>
                    </li>
                  </ul>
                </div>
              </aside> 

              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3>Price Filter</h3>
                </div>
                <div class="widgets_inner">
                  <div class="range_item">
                    <div id="slider-range"></div>
                    <div class="">
                      <label for="amount">Price : </label>
                      <input type="text" id="amount" readonly />
                    </div>
                  </div>
                </div>
              </aside>
            </div> --}}
          </div>
        </div>
      </div>
    </section>
    <!--================End Category Product Area =================-->



    <script>



            $(document).ready(function () {
            $('#category').on('change', function(e){

                // console.log(e);
                var cat_id = e.target.value;
                console.log(cat_id);
                $.get('/ajax-subcat?cat_id=' + cat_id, function(data){

                         console.log(data);
                         $('#subcategory').empty();
                         $('#subcategory').append( '<option value="">-- SELECT ITEM --</option>');
            $.each(data,function(index, subcatObj){


                var url = '{{ route("delivery.product", ["subcat" => "id" ]) }}';

    url = url.replace('id', subcatObj.id);
                $('#subcategory').append( '<option value="'+url+'">'+subcatObj.title+'</option>');
            });

                });
            })

        });

            </script>
    @endsection
