@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
 
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container">
        <div class="row">
            <div class="col-md-12">
 
 

	     


        <h1>Create New Route </h1>
        @if ($errors->any())
        <div style="color:red">
                {{ implode('', $errors->all(':message')) }}
        </div>
       
@endif
	    {!! Form::open(['action' => 'RoutestransportpartnersController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
 


 
	        <div class="form-group">
	            {{Form::label('routename', 'Route Name')}}
	            {{Form::text('routename', '', ['class' => 'form-control', 'placeholder' => 'route name'])}}
	        </div>

      
 
		{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
	 
 

            </div></div></div>
      
@endsection
     
@section('javascript')

<script>
 
       $(".limitme").each(function(i){
                 
                            var len=$(this).text().trim().length;
                            if(len>2)
                            {
                               
                                // this trims the letters to show only 3alphabets
                                $(this).text($(this).text().substr(0,5)+'');
                            }
                        });
                      
</script>
@endsection
