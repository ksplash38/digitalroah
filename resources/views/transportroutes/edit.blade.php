@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')

    @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container">
        <div class="row">
            <div class="col-md-12">






        <h1>Edit Route </h1>
        @if ($errors->any())
        <div style="color:red">
                {{ implode('', $errors->all(':message')) }}
        </div>

@endif



{!! Form::open(['action' => ['RoutestransportpartnersController@update', $routestransport->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}


{{ method_field('PUT') }}
{{ csrf_field() }}
 <br>
 <h3>Route: {{$routestransport->routesname}}</h3>

<br>

            <div class="form-group">
                    {{Form::label('presentlocation', 'Present Location')}}
                    {{Form::text('presentlocation', $routestransport->Presentlocation, ['class' => 'form-control', 'placeholder' => 'Present Location'])}}
            </div>

{{-- 

                <input type="hidden" name="presentlocation" value="ARRIVED">
            <li class="list-group-item text-danger">Please Click BOX if Arrived <input type="checkbox" onChange="this.form.submit()" >  </li>
 --}}




    <!-- ?php

        echo uniqid('id' , false), "\t",  uniqidReal(), PHP_EOL;

    ? -->



{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}



            </div></div></div>

            <script>


function forceKeyPressUppercase(e)
{
  var charInput = e.keyCode;
  if((charInput >= 97) && (charInput <= 122)) { // lowercase
    if(!e.ctrlKey && !e.metaKey && !e.altKey) { // no modifier key
      var newChar = charInput - 32;
      var start = e.target.selectionStart;
      var end = e.target.selectionEnd;
      e.target.value = e.target.value.substring(0, start) + String.fromCharCode(newChar) + e.target.value.substring(end);
      e.target.setSelectionRange(start+1, start+1);
      e.preventDefault();
    }
  }
}

document.getElementsByName('presentlocation')[0].addEventListener("keypress", forceKeyPressUppercase, false);


            </script>

@endsection



@section('scripts')


	{!! Html::script('select2/js/select2.min.js') !!}

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
