@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')

    @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container">
        <div class="row">
            <div class="col-md-12">






        <h1>Create New Waybill </h1>
        @if ($errors->any())
        <div style="color:red">
                {{ implode('', $errors->all(':message')) }}
        </div>

@endif
	    {!! Form::open(['action' => 'PartnertransportrecieptController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}



	    <div class="form-group">





	    </div>

        {{-- {{ Form::label('company_id', 'Company:') }}
        <select class="form-control" name="company_id">
          @foreach($companies as $company)
            <option value='{{ $company->id }}'>{{ $company->title }}</option>
          @endforeach

        </select> --}}
	        <div class="form-group">
	            {{Form::label('name', 'Name')}}
	            {{Form::text('passengername', '', ['class' => 'form-control', 'placeholder' => 'Passenger name'])}}
	        </div>


					<div class="form-group">
							{{Form::label('passengerroute', 'Passenger Route')}}

        <select class="form-control" name="passengerroute">
          @foreach($routes as $company)

            <option value='{{ $company->routesname }}'>{{ $company->routesname }}</option>
          @endforeach

        </select>
					</div>





	        <div class="form-group">
	            {{Form::label('price', 'Price ₦')}}
	            {{Form::text('price', '', ['class' => 'form-control', 'placeholder' => 'price'])}}
            </div>

            <div class="form-group">
                    {{Form::label('passengernumber', 'Passenger Number')}}
                    {{Form::text('passengernumber', '', ['class' => 'form-control', 'placeholder' => 'passenger number'])}}
                </div>

                <div class="form-group">
                        {{Form::label('drivername', 'Driver Name')}}

        <select class="form-control" name="drivername">
          @foreach ($drivers as $item)
          <option value='{{ $item->driver_name }}'>{{ $item->driver_name }}</option>
          @endforeach




              </select>
                    </div>


                        <div class="form-group">
                            <div>
                                Home Delivery <small>(do we deliver to passengers house)</small>
                            </div>
                                <select name="homedelivery" class="form-control">

                                        <option value="No">
                                                No
                                        </option>
                                        <option value="Yes" >
                                                Yes
                                        </option>



                                    </select>                            </div>


                                    <div class="form-group">
                                            {{Form::label('pickupdelivery', 'Pickup Delivery')}}
                                            <select name="pickupdelivery" class="form-control">

                                                    <option value="No">
                                                            No
                                                    </option>
                                                    <option value="Yes" >
                                                            Yes
                                                    </option>



                                                </select>                            </div>










	        <!-- ?php

	            echo uniqid('id' , false), "\t",  uniqidReal(), PHP_EOL;

	        ? -->



		{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}



            </div></div></div>

@endsection

@section('javascript')

<script>

       $(".limitme").each(function(i){

                            var len=$(this).text().trim().length;
                            if(len>2)
                            {

                                // this trims the letters to show only 3alphabets
                                $(this).text($(this).text().substr(0,5)+'');
                            }
                        });

</script>
@endsection
