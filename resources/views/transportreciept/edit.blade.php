@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
 
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container">
        <div class="row">
            <div class="col-md-12">
 
 

	     


        <h1>Edit Product </h1>
        @if ($errors->any())
        <div style="color:red">
                {{ implode('', $errors->all(':message')) }}
        </div>
       
@endif

 
 
{!! Form::open(['action' => ['PartnerproductController@update', $product->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
 

{{ method_field('PUT') }}
{{ csrf_field() }}
<div class="form-group">


                         


</div>

{{ Form::label('company_id', 'Company:') }}
<select class="form-control" name="company_id">
  @foreach($companies as $company)
    <option value='{{ $company->id }}'   @if($user ===  $company->owner_id ) selected='selected' @endif>{{ $company->title }}</option>
  @endforeach

</select>
    <div class="form-group">
        {{Form::label('name', 'Name')}}
        {{Form::text('name', $product->name, ['class' => 'form-control', 'placeholder' => 'name'])}}
    </div>

    <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description',  $product->description, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'description'])}}
        </div>
            <div class="form-group">
                    {{Form::label('quantity', 'Quantity')}}
                    {{Form::text('quantity', $product->quantity, ['class' => 'form-control', 'placeholder' => 'Quantity'])}}
            </div>





    <div class="form-group">
        {{Form::label('price', 'Price ₦')}}
        {{Form::text('price',  $product->price, ['class' => 'form-control', 'placeholder' => 'price'])}}
    </div>
    
    <div class="form-group">
            {{Form::label('oldprice', 'Old Price (optional) ₦')}}
            {{Form::text('oldprice',  $product->oldprice, ['class' => 'form-control', 'placeholder' => 'optional previous slashed'])}}
        </div>



    <div class="form-group">
        <h5>Cover Image</h5>
        {{Form::file('image')}}
    </div>
<h5>Images</h5>
    <input  type="file" class="form-control" name="images[]" multiple>




    <!-- ?php

        echo uniqid('id' , false), "\t",  uniqidReal(), PHP_EOL;

    ? -->



{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
 


            </div></div></div>

@endsection



@section('scripts')


	{!! Html::script('select2/js/select2.min.js') !!}

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
