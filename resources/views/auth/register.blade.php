@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('register') }}"  enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-7">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-7">
                                <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username">

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-7">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                    <label for="mobilenumber" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                                    <div class="col-md-3">
                                        <select class="form-control" name="countrycode">
                                            @foreach($countries as $country)
                                            <option value="{{$country->phonecode}}">({{$country->iso}}) {{$country->phonecode}}     </option>
                                          @endforeach
                                          </select>

                                    </div>
                                    <div class="col-md-4">
                                        <input id="mobilenumber" type="mobilenumber" class="form-control @error('mobilenumber') is-invalid @enderror" name="mobilenumber" value="{{ old('mobilenumber') }}" required autocomplete="mobilenumber">

                                        @error('mobilenumber')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                        <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                                        <div class="col-md-7">
                                            <input id="address" type="address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address">

                                            @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


                            <div class="form-group row">
                                    <label for="stateOforigin" class="col-md-4 col-form-label text-md-right">{{ __('State Of Origin') }}</label>

                                    <div class="col-md-7">
                                        <input id="stateOforigin" type="stateOforigin" class="form-control @error('stateOforigin') is-invalid @enderror" name="stateOforigin" value="{{ old('stateOforigin') }}" required autocomplete="stateOforigin">

                                        @error('stateOforigin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group row">
                                        <label for="stateFrom" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                                        <div class="col-md-7">

                                                <select class="form-control" name="gender"   >
                                                        <option value="m"> MALE </option>

                                                        <option value="f"> FEMALE </option>






                                                               </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">



                                            <label for="stateFrom" class="col-md-4 col-form-label text-md-right">{{ __('Going For NYSC Camp') }}</label>

                                            <div class="col-md-7">

                                                <label class="text-danger"> Click Me if your Going For NYSC CAMP   
                                                  


                                                     <input type='hidden' value='No' name='nysc'  >
                                                     <input type='checkbox' value='Yes' name='nysc'  onclick="myFunction()" id="myCheck">
                                                    </label>
                                            </div>

                                            <script>
   
                                                    function myFunction() {
                                                      var checkBox = document.getElementById("myCheck");
                                                      var text = document.getElementById("text");
                                                      if (checkBox.checked == true){
                                                        text.style.display = "block";
                                                      } else {
                                                         text.style.display = "none";
                                                      }
                                                    }
                                                    </script>

                                        </div>


                                    {{-- <p id="text" style="display:none">Checkbox is CHECKED!</p> --}}
<div  id="text" style="display:none">
                                    <div class="form-group row">
                                            <label for="stateFrom" class="col-md-4 col-form-label text-md-right">{{ __('Posted From') }}</label>

                                            <div class="col-md-7">

                                                    <select class="form-control" name="stateFrom"   >
                                                            <option value="chooseoption">   -- Choose Option -- </option>
                                                            <option value="Abia State">  Abia State </option>

                                                            <option value="Adamawa State">  Adamawa State </option>
                                                            <option value="Akwa State">   Akwa Ibom State</option>
                                                            <option value="Anambra State">    Anambra State</option>
                                                            <option value="Bauchi State">  Bauchi State </option>
                                                            <option value="Bayelsa State">  Bayelsa State </option>
                                                            <option value="Benue State">  Benue State </option>
                                                            <option value="Borno State">  Borno State </option>
                                                            <option value="Cross State">  Cross State </option>
                                                            <option value="River State">  River State </option>
                                                            <option value="Delta State">  Delta State </option>
                                                            <option value="Ebonyi State">  Ebonyi State </option>
                                                            <option value="Edo State">  Edo State </option>
                                                            <option value="Ekiti State">  Ekiti State </option>
                                                            <option value="Enugu State">  Enugu State </option>
                                                            <option value="FCT">  FCT </option>
                                                            <option value="Gombe State">  Gombe State </option>
                                                            <option value="Imo State">  Imo State </option>
                                                            <option value="Jigawa State">  Jigawa State </option>
                                                            <option value="Kaduna State">  Kaduna State </option>
                                                            <option value="Kano State">  Kano State </option>
                                                            <option value="Katsina State">  Katsina State </option>
                                                            <option value="Kebbi State">  Kebbi State </option>
                                                            <option value="Kogi State">  Kogi State </option>
                                                            <option value="Kwara State">  Kwara State </option>
                                                            <option value="Lagos State">  Lagos State </option>
                                                            <option value="Nasarawa State">  Nasarawa State </option>
                                                            <option value="Niger State">  Niger State </option>
                                                            <option value="Ogun State">  Ogun State </option>
                                                            <option value="Ondo State">  Ondo State </option>
                                                            <option value="Osun State">  Osun State </option>
                                                            <option value="Oyo State">  Oyo State </option>
                                                            <option value="Plateau State">  Plateau State </option>
                                                            <option value="Rivers State">  Rivers State </option>
                                                            <option value="Sokoto State">  Sokoto State </option>
                                                            <option value="Taraba State">  Taraba State </option>
                                                            <option value="Yobe State">  Yobe State </option>
                                                            <option value="Zamfara State">  Zamfara State </option>
                                                                   </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('State Posted To') }}</label>

                                                <div class="col-md-7">

                                                        <select class="form-control" name="stateTo"   >
                                                                <option value="chooseoption">   -- Choose Option -- </option>
                                                       <option value="Abia State">  Abia State </option>
                                                       <option value="Adamawa State">  Adamawa State </option>
                                                       <option value="Akwa State">   Akwa Ibom State</option>
                                                        <option value="Anambra State">    Anambra State</option>
                                                       <option value="Bauchi State">  Bauchi State </option>
                                                       <option value="Bayelsa State">  Bayelsa State </option>
                                                       <option value="Benue State">  Benue State </option>
                                                       <option value="Borno State">  Borno State </option>
                                                       <option value="Cross State">  Cross State </option>
                                                       <option value="River State">  River State </option>
                                                       <option value="Delta State">  Delta State </option>
                                                       <option value="Ebonyi State">  Ebonyi State </option>
                                                       <option value="Edo State">  Edo State </option>
                                                       <option value="Ekiti State">  Ekiti State </option>
                                                       <option value="Enugu State">  Enugu State </option>
                                                       <option value="FCT">  FCT </option>
                                                       <option value="Gombe State">  Gombe State </option>
                                                       <option value="Imo State">  Imo State </option>
                                                       <option value="Jigawa State">  Jigawa State </option>
                                                       <option value="Kaduna State">  Kaduna State </option>
                                                       <option value="Kano State">  Kano State </option>
                                                       <option value="Katsina State">  Katsina State </option>
                                                       <option value="Kebbi State">  Kebbi State </option>
                                                       <option value="Kogi State">  Kogi State </option>
                                                       <option value="Kwara State">  Kwara State </option>
                                                       <option value="Lagos State">  Lagos State </option>
                                                       <option value="Nasarawa State">  Nasarawa State </option>
                                                       <option value="Niger State">  Niger State </option>
                                                       <option value="Ogun State">  Ogun State </option>
                                                       <option value="Ondo State">  Ondo State </option>
                                                       <option value="Osun State">  Osun State </option>
                                                       <option value="Oyo State">  Oyo State </option>
                                                       <option value="Plateau State">  Plateau State </option>
                                                       <option value="Rivers State">  Rivers State </option>
                                                       <option value="Sokoto State">  Sokoto State </option>
                                                       <option value="Taraba State">  Taraba State </option>
                                                       <option value="Yobe State">  Yobe State </option>
                                                       <option value="Zamfara State">  Zamfara State </option>
                                                              </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Batch') }}</label>

                                                    <div class="col-md-7">
                                                            <select class="form-control" name="batch">
                                                                    @foreach($batches as $batch)
                                                                    <option value="{{$batch->name}}">{{$batch->name}}     </option>
                                                                  @endforeach
                                                                  </select>



                                                    </div>
                                                </div>

                                            <div class="form-group row">
                                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Year') }}</label>

                                                    <div class="col-md-7">
                                                            <select class="form-control" name="year" >
                                                                    @foreach($years as $year)
                                                                    <option value="{{$year->year}}">{{$year->year}}     </option>
                                                                  @endforeach
                                                                  </select>

                                                    </div>
                                                </div>
                                            </div>

                               








                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-7">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="bio" class="col-md-4 col-form-label text-md-right">{{ __('Brief Self Bio') }}</label>

                                <div class="col-md-7">
                                    <textarea id="bio" type="text" class="form-control @error('bio') is-invalid @enderror" name="bio" value="{{ old('bio') }}" required> </textarea>
                                    @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 col-form-label text-md-right">Choose Photo</div>
                                <br>
                                <input type="file" name="image" id="fileUpload" class="col-md-4 col-form-label text-md-right">
                                    {{-- {{Form::file('image' , ['id'=> 'fileUpload' ,  class="col-md-4 col-form-label text-md-right">])}} --}}
                                    <br />

                                </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <script>
   var num1 = parseInt("025", 10);
         document.write(num1);
</script> --}}
@endsection
