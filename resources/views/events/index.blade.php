@extends('layouts.app')


@section('content')
	<!-- start Banner Area -->
	<section class="home-banner-area relative banner-area">
		<div class="container-fluid">
			<div class="row  d-flex align-items-center justify-content-center">
				<div class="header-left col-lg-5 col-md-6 ">
					<h6 class="text-white ">the Royal Essence of Journey</h6>
					<h2>Event Details</h2>
					<p class="pt-10 pb-20">
						If you are looking at blank cassettes on the web, you may be very confused at the.
					</p>
				</div>
				<div class="col-lg-7 col-md-6 col-sm-8 header-right">
					<div class="">
						<img class="img-fluid w-100" src="img/banner-img.jpg" alt="">
					</div>
					<div class="form-wrap about-content">
						<p class="text-white m-0">
							<span class="box">
								<a href="index.html">Home </a>
								<span class="lnr lnr-arrow-right"></span>
								<a href="event-details.html">Event Details</a>
							</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	<!-- Start Condition Area -->
	<section class="condition-area event-details-area section-gap">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-lg-6 col-md-8 col-sm-10">
					<div class="condition-left">
						<img class="img-fluid" src="img/events/ed1.jpg" alt="">
					</div>
				</div>
				<div class="offset-lg-1 col-lg-5">
					<div class="condition-right">
						<h2>
							Spreading Peace <br>
							Around the Worldwide
						</h2>
						<p>
							If you are looking at blank cassettes on the web, you may be very confused at the difference in price You may
							see some for as low as each. If you are looking at blank cassettes on the web, you may be very confused at the
							difference in price You may see.
						</p>
						<p>
							If you are looking at blank cassettes on the web, you may be very confused at the difference in price You may
							see some for as low as each. If you are looking at blank cassettes on the web, you may be very confused at the
							difference in price You may see.
						</p>
						<ul>
							<li>Saturday, 15th September, 2018</li>
							<li>Rocky beach Church</li>
							<li>Los Angeles, USA.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Condition Area -->

    @endsection
