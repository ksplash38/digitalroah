@extends('layouts.app')

@section('content')



<table class="table table-bordered" id="laravel_crud">
        <thead>
           <tr>
              <th>Id</th>
              <th>Title</th>
              <th>Description</th>
              <th>Created at</th>

           </tr>
        </thead>
        <tbody>
           @foreach($users as $user)
           <tr>
              <td>{{ $user->id }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ date('d m Y', strtotime($user->created_at)) }}</td>

           </tr>
           @endforeach
        </tbody>
       </table>
       @endsection
