
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'yuthconnect') }}</title>
	<!-- Favicon-->
	<link rel="shortcut icon" href="{{ asset('theme/img/fav.png') }}">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
    <!-- Site Title -->


</head>

<body>

<div style="margin-left:50%">
<h3>Digital Roah</h3>
</div>

<table class="table table-bordered" id="laravel_crud">
        <thead>
           <tr>
              <th></th>
              <th></th>


           </tr>
        </thead>
        <tbody>
                <tr>

                    <td><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('main-logo.png', .3, true)->size(100)->generate($users->planName .$users->planExpire)) !!} "></td>

                    {{-- <td><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('theme/img/fav.png', .3, true)->size(100)->generate($users->planExpire)) !!} "  style="margin-left:15em"></td> --}}
                    {{-- <td>{{ $users->id }}</td> --}}

                </tr>

             </tbody>
       </table>



       <div>Name: {{ $users->name }}</div>

<br>

       <div>Email: {{ $users->email }}</div>

<br>
<div>State of Origin: {{ $users->stateOforigin }}</div>

<br>

       <div>Leaving From: {{ $users->stateFrom }}</div>

       <br>

              <div>Going To: {{ $users->stateTo }}</div>

              <br>



       <div>Plan Name: {{ $users->planName }}</div>

       <br>

              <div>Expiry: {{ $users->planExpire }}</div>
              <br>

              <div>Payment Status: {{ $users->paymentStatus }}</div>
              <br>

              <div>Phone No: {{ $users->mobilenumber }}</div>
              <br>

              <div>Address: {{ $users->address }}</div>

</body>
</html>
