@extends('layouts.app')



@section('title')
Account Settings
@endsection


@section('content')
<body onload="myFunction()">
    

<style>



        /* Profile container */
        .profile {
          margin: 20px 0;
        }

        /* Profile sidebar */
        .profile-sidebar {
          padding: 20px 0 10px 0;
          background: #fff;
        }

        .profile-userpic img {
          float: none;
          margin: 0 auto;
          width: 50%;
          height: 50%;
          -webkit-border-radius: 50% !important;
          -moz-border-radius: 50% !important;
          border-radius: 50% !important;
        }

        .profile-usertitle {
          text-align: center;
          margin-top: 20px;
        }

        .profile-usertitle-name {
          color: #5a7391;
          font-size: 16px;
          font-weight: 600;
          margin-bottom: 7px;
        }

        .profile-usertitle-job {
          text-transform: uppercase;
          color: #5b9bd1;
          font-size: 12px;
          font-weight: 600;
          margin-bottom: 15px;
        }

        .profile-userbuttons {
          text-align: center;
          margin-top: 10px;
        }

        .profile-userbuttons .btn {
          text-transform: uppercase;
          font-size: 11px;
          font-weight: 600;
          padding: 6px 15px;
          margin-right: 5px;
        }

        .profile-userbuttons .btn:last-child {
          margin-right: 0px;
        }

        .profile-usermenu {
          margin-top: 30px;
        }

        .profile-usermenu ul li {
          border-bottom: 1px solid #f0f4f7;
        }

        .profile-usermenu ul li:last-child {
          border-bottom: none;
        }

        .profile-usermenu ul li a {
          color: #93a3b5;
          font-size: 14px;
          font-weight: 400;
        }

        .profile-usermenu ul li a i {
          margin-right: 8px;
          font-size: 14px;
        }

        .profile-usermenu ul li a:hover {
          background-color: #fafcfd;
          color: #5b9bd1;
        }

        .profile-usermenu ul li.active {
          border-bottom: none;
        }

        .profile-usermenu ul li.active a {
          color: #5b9bd1;
          background-color: #f6f9fb;
          border-left: 2px solid #5b9bd1;
          margin-left: -2px;
        }

        /* Profile Content */
        .profile-content {
          padding: 20px;
          background: #fff;
          min-height: 460px;
        }

        </style>



@if (session('info'))
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('info') }}
		</div>
	</div>
</div>
@elseif (session('error'))
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('error') }}
		</div>
	</div>
</div>
@endif
<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">

				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                               {{-- {!!QrCode:: backgroundColor(255,255,0)->size(100)->generate('Make me into a QrCode!')!!} --}}
                               @if (!empty($user->planName))
                               <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('main-logo.png', .3, true)->size(100)->generate($user->planName.$user->planExpire)) !!} ">
                               @endif
{{--
                               @if (!empty($user->planExpire))
                               <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('theme/img/fav.png', .3, true)->size(100)->generate($user->planExpire)) !!} ">
                               @endif --}}

                               {{-- {!! QrCode::format('png')->mergeString(Storage::get({{ asset('theme/img/fav.png') }}), .3)->generate() !!} --}}
                            <br>
                           <img src="{{ productImage($user->image) }}" alt="" style="border-radius: 50%; width:10em; height:10em">
                           <br>
                                {{$user->name}}
                        </div>
                        <div class="profile-usertitle-name">
                                {{$user->planName}} - Plan
                        </div>
                        <div class="profile-usertitle-job">
                                {{$user->email}}
                        </div>
                    </div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                        <button type="button" class="btn btn-success btn-sm">Follow</button>


                        <a href="{{ url('pdf') }}" class="btn btn-danger btn-sm"  target="_blank">Export PDF</a>




                    </div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nv">
						<li>
							<a href="/dashboard">
							<i class="fa fa-home"></i>
							Overview </a>
						</li>
						<li  class="active">
						<a href="/accountsettings/{{$user->unique_id}}/edit">
							<i class="fa fa-user"></i>
							Account Settings </a>
						</li>

                        <li>
                                <a href="/payment">
                                <i class="fa fa-upload"></i>
                                Upload Payment </a>
                            </li>
                                    <li>
                                        <a href="#">
                                                <i class="fa fa-help"></i>
                                        Help </a>
                                    </li>
                                </ul>

{{--
						<li>
							<a href="#">
							<i class="fa fa-flag"></i>
							Help </a>
						</li> --}}
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <br>   <br>
            <div class="profile-content">
				<div class="row">
					{!! Form::open(['action' => ['DashboardController@update', 'user' => $user], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

					{{ csrf_field() }}
					{{ method_field('PUT') }}
		<div class="col-md-12">

				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						{{Form::label('name', 'Names')}}
						{{Form::text('name', $user->name, ['class' => 'form-control' ,'autofocus' => 'true', 'placeholder' => 'Full Names'])}}

						@if ($errors->has('name'))
						<span class="help-block">
							<strong style="color:red">{{ $errors->first('name') }}</strong>
						</span>
					@endif

			  </div>


				<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
						{{Form::label('username', 'Username')}}
						{{Form::text('username', $user->username, ['class' => 'form-control' ,'autofocus' => 'true', 'placeholder' => 'username'])}}

						@if ($errors->has('username'))
						<span class="help-block">
							<strong style="color:red">{{ $errors->first('username') }}</strong>
						</span>
					@endif

			  </div>

			  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					{{Form::label('email', 'Email')}}
                    {{Form::email('email', $user->email, ['class' => 'form-control' ,'autofocus' => 'true', 'placeholder' => 'Email'])}}

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong style="color:red">{{ $errors->first('email') }}</strong>
                    </span>
                @endif
		  </div>

			<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			  {{ Form::label('password', 'New Password') }}
			  {{ Form::password('password', ['class' => 'form-control' ,  'placeholder' => 'New Password'])}}


              @if ($errors->has('password'))
              <span class="help-block">
                  <strong style="color:red">{{ $errors->first('password') }}</strong>
              </span>
          @endif

			</div>
			  <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

			  {{ Form::label('password_confirmation', 'Confrim New Password') }}
              {{ Form::password('password_confirmation', ['class' => 'form-control' ,  'placeholder' => 'new Password Confirmation'])}}

              @if ($errors->has('password_confirmation'))
              <span class="help-block">
                  <strong style="color:red">{{ $errors->first('password_confirmation') }}</strong>
              </span>
          @endif
			</div>

				<div class="form-group">
								{{Form::label('address', 'Address')}}
								{{Form::text('address', $user->address, ['class' => 'form-control', 'placeholder' => 'Address'])}}
				</div>



				<div class="form-group">
						{{Form::label('mobile', 'Mobile Number')}}
						{{Form::text('mobilenumber', $user->mobilenumber, ['class' => 'form-control', 'placeholder' => 'Mobile Number'])}}
				  </div>
<hr>

<div class="form-group row">



        <label for="stateFrom" class="col-md-4 col-form-label text-md-right">{{ __('Going For NYSC Camp') }}</label>

        <div class="col-md-7">

            <label class="text-danger"> Click Me if your Going For NYSC CAMP    
                
                    <input type='hidden' value='off' name='nysc'>
   
                <input type="checkbox" id="myCheck" value="on" @if($user->nysc === 'on') checked='checked' @endif name="nysc" onclick="myFunction()"></label>
        </div>
    </div>


    <div  id="text" style="display:none">
            <div class="form-group row">
                    <label for="stateFrom" class="col-md-4 col-form-label text-md-right">{{ __('Posted From') }}</label>

                    <div class="col-md-7">

                            <div class="form-group">

                                    {{Form::label('stateFrom', 'Leaving From')}}


                                    <select class="form-control" name="stateFrom" id="stateFrom"  value="{{ request()->input('stateFrom') }}"  >
                                        @foreach($states as $state)
                                            <option value="{{$state->name}}"   @if($user->stateFrom=== $state->name) selected='selected' @endif>{{$state->name}}     </option>
                                          @endforeach
                                          </select>

                            </div>
                    </div>
                </div>

                <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('State Posted To') }}</label>

                        <div class="col-md-6 wrap-center donation-input">
                                <div class="form-group">

                                        {{Form::label('stateTo', 'Posted To')}}
                                        <select class="form-control" name="stateTo" value="{{ request()->input('stateTo') }}">
                                                @foreach($states as $state)
                                                <option value="{{$state->name}}"   @if($user->stateTo=== $state->name) selected='selected' @endif>{{$state->name}}     </option>
                                              @endforeach
                                              </select>

                                </div>
                            </div>
                    </div>

                    <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Batch') }}</label>
                            <div class="col-md-7">
                            <div class="form-group">
{{--
                                    {{Form::label('batch', 'Batch & Stream')}} --}}


                                    <select class="form-control" name="batch" id="batch"  value="{{ request()->input('batch') }}"  >
                                        @foreach($batches as $batch)
                                            <option value="{{$batch->name}}"   @if($user->batch=== $batch->name) selected='selected' @endif>{{$batch->name}}     </option>
                                          @endforeach
                                          </select>

                            </div>   </div>
                        </div>

                    <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Year') }}</label>

                            <div class="col-md-7">
                                    <div class="form-group">

                                            {{-- {{Form::label('year', 'Year')}} --}}
                                            <select class="form-control" name="year" value="{{ request()->input('year') }}">
                                                    @foreach($years as $year)
                                                    <option value="{{$year->year}}"   @if($user->year=== $year->year) selected='selected' @endif>{{$year->year}}     </option>
                                                  @endforeach
                                                  </select>

                                    </div>
                            </div>
                        </div>
                    </div>

            <script>
            function myFunction() {
              var checkBox = document.getElementById("myCheck");
              var text = document.getElementById("text");
              if (checkBox.checked == true){
                text.style.display = "block";
              } else {
                 text.style.display = "none";
              }
            }
            </script>
                  {{-- <div class="row">

                        <div class="col-md-6 wrap-left donation-input">
                            <div class="form-group">

                                    {{Form::label('stateFrom', 'Leaving From')}}


                                    <select class="form-control" name="stateFrom" id="stateFrom"  value="{{ request()->input('stateFrom') }}"  >
                                        @foreach($states as $state)
                                            <option value="{{$state->name}}"   @if($user->stateFrom=== $state->name) selected='selected' @endif>{{$state->name}}     </option>
                                          @endforeach
                                          </select>

                            </div>
                        </div>

                        <div class="col-md-6 wrap-center donation-input">
                                <div class="form-group">

                                        {{Form::label('stateTo', 'Posted To')}}
                                        <select class="form-control" name="stateTo" value="{{ request()->input('stateTo') }}">
                                                @foreach($states as $state)
                                                <option value="{{$state->name}}"   @if($user->stateTo=== $state->name) selected='selected' @endif>{{$state->name}}     </option>
                                              @endforeach
                                              </select>

                                </div>
                            </div>

                    </div>


                    <div class="row">

                            <div class="col-md-6 wrap-left donation-input">
                                <div class="form-group">

                                        {{Form::label('batch', 'Batch & Stream')}}


                                        <select class="form-control" name="batch" id="batch"  value="{{ request()->input('batch') }}"  >
                                            @foreach($batches as $batch)
                                                <option value="{{$batch->name}}"   @if($user->batch=== $batch->name) selected='selected' @endif>{{$batch->name}}     </option>
                                              @endforeach
                                              </select>

                                </div>
                            </div>

                            <div class="col-md-6 wrap-center donation-input">
                                    <div class="form-group">

                                            {{Form::label('year', 'Year')}}
                                            <select class="form-control" name="year" value="{{ request()->input('year') }}">
                                                    @foreach($years as $year)
                                                    <option value="{{$year->year}}"   @if($user->year=== $year->year) selected='selected' @endif>{{$year->year}}     </option>
                                                  @endforeach
                                                  </select>

                                    </div>
                                </div>

                        </div> --}}
                    <hr>
			  <div class="form-group">
								  {{Form::label('stateOforigin', 'State Of Origin')}}
										{{Form::text('stateOforigin', $user->stateOforigin, ['class' => 'form-control', 'placeholder' => 'State Of Origin'])}}
                </div>

                <div class="form-group">
                        {{Form::label('bio', 'Self Description')}}
                              {{Form::textarea('bio', $user->bio, ['class' => 'form-control', 'placeholder' => 'Brief Self Bio'])}}
      </div>
                <div class="form-group">
       {{Form::file('image' , ['id'=> 'fileUpload'])}}

    </div>

						<div class="form-group">

				{{-- <div class="form-group">
						{{Form::label('newssubscription', 'News Subscription')}}
				{{Form::select('newssubscription', array(
    'Subscribe Me' => array('1' => 'Yes'),
    'Unsubscribe Me' => array('0' => 'No'),
))}}
		</div>	 --}}

		<hr>
<h4>
	Enter Current Password To Apply Changes
</h4>
		<div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">

				{{ Form::password('current_password' , ['class' => 'form-control' ,  'placeholder' => 'Current Password'])}}
				@if ($errors->has('current_password'))
				<span class="help-block">
					<strong style="color:red">{{ $errors->first('current_password') }}</strong>
				</span>
			@endif
			  </div>


					</div>

	<div class="col-md-12 col-lg-12">
			<div class="well">


				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($user->updated_at)) }}</dd>
				</dl>

				<hr>
				<div class="row">
					<div class="col-sm-6">

						<a href="{{url()->previous()}}" class="btn btn-danger btn-block">Cancel</a>
					</div>
					<div class="col-sm-6">



						{{-- {{Form::hidden('_method','PUT')}} --}}
						{{Form::submit('Submit', ['class'=>'btn btn-success btn-block'])}}

					</div>
				</div>
	</div>
			</div>
				{!! Form::close() !!}
		  </div>

            </div>
		</div>
	</div>
</div>

<br>
<br>

</body>
@endsection
