@extends('layouts.app')

@section('content')
<style>
/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  height: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}

.profile-usermenu {
  margin-top: 30px;
}

.profile-usermenu ul li {
  border-bottom: 1px solid #f0f4f7;
}

.profile-usermenu ul li:last-child {
  border-bottom: none;
}

.profile-usermenu ul li a {
  color: #93a3b5;
  font-size: 14px;
  font-weight: 400;
}

.profile-usermenu ul li a i {
  margin-right: 8px;
  font-size: 14px;
}

.profile-usermenu ul li a:hover {
  background-color: #fafcfd;
  color: #5b9bd1;
}

.profile-usermenu ul li.active {
  border-bottom: none;
}

.profile-usermenu ul li.active a {
  color: #5b9bd1;
  background-color: #f6f9fb;
  border-left: 2px solid #5b9bd1;
  margin-left: -2px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}

</style>
 <link rel="stylesheet"   href="{{asset("fancybox/fancybox-2.1.7/source/jquery.fancybox.css")}}" type="text/css" media="screen" />


<div class="container">
        <div class="row profile">
            <div class="col-md-3">
                <div class="profile-sidebar">



                    <!-- SIDEBAR USERPIC -->
                    <div class="profile-userpic">
                        <img src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                               {{-- {!!QrCode:: backgroundColor(255,255,0)->size(100)->generate('Make me into a QrCode!')!!} --}}


                               {{-- {!! QrCode::format('png')->mergeString(Storage::get({{ asset('theme/img/fav.png') }}), .3)->generate() !!} --}}
                            <br>
                            <a href="{{ productImage($user->image) }}" class="fancybox" title="  {{$user->bio}}">   <img src="{{ productImage($user->image) }}" alt="" style="border-radius: 50%; width:10em; height:10em"></a>

                           <br>
                                {{$user->name}}
                        </div>
                        <div class="profile-usertitle-name">
                                {{$user->planName}} - Plan
                        </div>
                        <div class="profile-usertitle-job">
                                {{$user->email}}
                        </div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <div class="profile-userbuttons">
                        <button type="button" class="btn btn-success btn-sm">Follow</button>


                        <a href="{{ url('pdf') }}" class="btn btn-danger btn-sm"  target="_blank">Export PDF</a>




                    </div>
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul>
                            <li class="active">
                                <a href="#">
                                        <i class="fa fa-home"></i>
                                Overview </a>
                            </li>
                            <li>
                                <a href="/accountsettings/{{$user->unique_id}}">
                              <i class="fa fa-user"></i>
                                Account Settings </a>
                            </li>
                            <li>
                                <a href="/payment">
                                <i class="fa fa-upload"></i>
                                Upload Payment </a>
                            </li>
                            <li>
                                <a href="#">
                                        <i class="fa fa-help"></i>
                                Help </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
            </div>



            <div class="col-md-9">  <br>  <br>  <br>  <br>  <br>

                <div class="profile-content">

                        <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">



                                        {{-- {!!QrCode::phoneNumber('776-004-1698') !!} --}}

                        @if (!empty($user->planName))
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('main-logo.png', .3, true)->size(100)->generate($user->planName .$user->planExpire)) !!} ">
                        @endif

                                </div>
                                {{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        @if (!empty($user->planExpire))
                                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->merge('theme/img/fav.png', .3, true)->size(100)->generate($user->planExpire)) !!} ">
                                        @endif

                              </div> --}}
                          </div>

                

                        <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <h4>
                      NAME
                  </h4>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <h4 style="color:#5cb85c">
                                              {{$user->name}}
                                          </h4>

                              </div>
                          </div>

                          <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4>
                        USERNAME
                    </h4>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h4 style="color:#5cb85c">
                                                {{$user->username}}
                                            </h4>

                                </div>
                            </div>





                          <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4>
                       EMAIL
                    </h4>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h4 style="color:#5cb85c">
                                                {{$user->email}}
                                            </h4>

                                </div>
                            </div>

                          <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4>
                        ADDRESS
                    </h4>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h4 style="color:#5cb85c">
                                                {{$user->address}}
                                            </h4>

                                </div>
                            </div>


                            <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4>
                        MOBILE NUMBER
                    </h4>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h4 style="color:#5cb85c">
                                                {{$user->mobilenumber}}
                                            </h4>

                                </div>
                            </div>

                            <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                      <h4>
                   STATE OF ORIGIN
                      </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <h4 style="color:#5cb85c">
                                        {{$user->stateOforigin}}
                                            </h4>
                                  </div>
                              </div>

                              @if ($user->nysc == 'on')
                              <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                      <h4>
                          EXPIRE-IN
                      </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                          <h4 style="color:#5cb85c">
                                            @if (!empty($user->planExpire))
                                            {{$user->planExpire}}
                                            @endif
    
                                              </h4>
    
                                  </div>
                              </div>
    
                            <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    
                      <h4>
                        COUNTDOWN
                      </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                          <h4 style="color:#5cb85c">
                                                @if (!empty($user->unique_id))
                                                <span id="current_timer{{ $user->unique_id }}"></span>
                                            @endif
                                                <span id="current_timer{{ $user->unique_id }}"></span>
                                              </h4>
    
                                              <script>
                                                    $(function(){
                                                        $('#current_timer{{ $user->unique_id }}').countdowntimer({
                                                            // startDate : "2014/10/01 00:00:00",
    
                                                            dateAndTime : "{{ $user->planExpire }}",
                                                            size : "xs",
                                                            borderColor : "#4fbfa8",
                                                backgroundColor : "#4fbfa8",
                                                fontColor : "#fff",
                                                timeUp : timeisUp,
    
    
    
    
                                                        })
    
                                        function timeisUp() {
                                if ('{{ $user->planExpire }}' < 0){
    
                                    $('#current_timer{{ $user->unique_id }}').html('NO FIXED TIME')
    
                                    }
                                    else{
                                        $('#current_timer{{ $user->unique_id }}').html('EXPIRED / NO PLAN')
                                    }
                                        }
                                                    });
                                                </script>
    
    
                                  </div>
                              </div>
                              <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                      <h4>
                   LEAVING FROM
                      </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <h4 style="color:#5cb85c">
                                        {{$user->stateFrom}}
                                            </h4>
                                  </div>
                              </div>

                            <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <h4>
                 POSTED TO
                    </h4>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <h4 style="color:#5cb85c">
                                      {{$user->stateTo}}
                                        </h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                  <h4>
               BATCH
                  </h4>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <h4 style="color:#5cb85c">
                                    {{$user->batch}}
                                      </h4>
                              </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <h4>
          YEAR
              </h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                  <h4 style="color:#5cb85c">
                                {{$user->year}}
                                  </h4>
                          </div>
                      </div>     
                              @endif
                           

                            <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                      <h4>
                         BIO
                      </h4>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                          <h4 style="color:#5cb85c">
                                                  {{$user->bio}}
                                              </h4>

                                  </div>
                              </div>




                </div>
            </div>
        </div>
    </div>

    <br>
    <br>

    @endsection

    @section('fancyboxscript')
    <script type="text/javascript" src="{{asset("fancybox/fancybox-2.1.7/source/jquery.fancybox.pack.js")}}"></script>


    <script>
            $(document).ready(function() {
                $(".fancybox").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});
            });
        </script>
@stop
