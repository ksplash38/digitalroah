@extends('layouts.app')


{{-- @section('title', 'Shopping Cart')
 --}}


@section('content')


    <!--================Home Banner Area =================-->
    <section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="container">
          <div
            class="banner_content d-md-flex justify-content-between align-items-center"
          >
            <div class="mb-3 mb-md-0">
              <h2>Product Checkout</h2>
              <p>Very us move be blessed multiply night</p>
            </div>
            <div class="page_link">
              <a href="/">Home</a>
              <a href="{{ URL::previous() }}">Product Checkout</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Checkout Area =================-->
    <section class="checkout_area ">
        <br>


        <form action="{{ route('pay') }}" method="POST">
                {{ csrf_field() }}

      <div class="container">
            @if ($errors->any())
         <div class="text-danger" style="font-size:1em">
               {{ implode('', $errors->all(':message')) }}
        </div>
    @endif
        <div class="billing_details">
          <div class="row">
            <div class="col-lg-8">
              <h3>Billing Details</h3>
          


                <div class="col-md-6 form-group p_star">
                        Full Name
                  <input
                    type="text"
                    class="form-control"
                    id="first"
                    name="name" value="{{ auth()->user()->name }}"
                  />

                </div>


                <div class="col-md-6 form-group p_star">
                        Email
                  <input
                    type="text"
                    class="form-control"
                    id="last"
                    name="email" value="{{ auth()->user()->email }}"
                  />

                </div>
                {{-- <div class="col-md-12 form-group">
                  <input
                    type="text"
                    class="form-control"
                    id="company"
                    name="company"
                    placeholder="Company name"
                  />
                </div> --}}
                <div class="col-md-6 form-group p_star">
                Billing Mobilenumber
                  <input
                    type="text"
                    class="form-control"
                    id="number"
                    name="mobilenumber" value="{{ auth()->user()->mobilenumber }}"
                  />

                </div>
                <div class="col-md-6 form-group p_star">
Billing State / city
                  <input
                    type="text"
                    class="form-control"
                    id="state"
                    name="state" value="{{ old('state') }}"
                  />

                </div>

                <div class="col-md-12 form-group p_star">
                   Billing     Address
                  <input
                    type="text"
                    class="form-control"
                    id="add1"
                    name="address"  value="{{ auth()->user()->address }}"
                  />

                </div>
                {{-- <div class="col-md-12 form-group p_star">
                  <input
                    type="text"
                    class="form-control"
                    id="add2"
                    name="add2"
                  />
                  <span
                    class="placeholder"
                    data-placeholder="Address line 02"
                  ></span>
                </div>
                <div class="col-md-12 form-group p_star">
                  <input
                    type="text"
                    class="form-control"
                    id="city"
                    name="city"
                  />
                  <span class="placeholder" data-placeholder="Town/City"></span>
                </div>
                <div class="col-md-12 form-group p_star">
                  <select class="country_select">
                    <option value="1">District</option>
                    <option value="2">District</option>
                    <option value="4">District</option>
                  </select>
                </div> --}}
                {{-- <div class="col-md-12 form-group">
                  <input
                    type="text"
                    class="form-control"
                    id="zip"
                    name="zip"
                    placeholder="Postcode/ZIP"
                  />
                </div> --}}
                {{-- <div class="col-md-12 form-group">
                  <div class="creat_account">
                    <input type="checkbox" id="f-option2" name="selector" />
                    <label for="f-option2">Create an account?</label>
                  </div>
                </div> --}}
                <div class="col-md-12 form-group">
                  {{-- <div class="creat_account">
                    <h3>Shipping Details</h3>
                    <input type="checkbox" id="f-option3" name="shiptodifferent" />
                    <label for="f-option3">Ship to a different address?</label>
                  </div> --}}
                  <hr>
                  <textarea
                    class="form-control"
                    name="message"
                    id="message"
                    rows="1"
                    placeholder="Order Notes"
                 style="height:6em" ></textarea>
                </div>
    
            </div>
            <div class="col-lg-4">
              <div class="order_box">
                <h2>Your Order</h2>
                <ul class="list">
                  <li>
                    <a href="#"
                      class="text-center" >Product

                    </a>
                  </li>
                  @foreach (Cart::content() as $item)
                  <li>
                    <a href="#"
                      >{{ $item->model->name }}
                      <span class="middle">x {{ $item->qty }}</span>
                      <span class="last">{{ presentPrice($item->subtotal) }} </span>
                    </a>
                  </li>
                  @endforeach
                  {{-- <li>
                    <a href="#"
                      >Fresh Tomatoes
                      <span class="middle">x 02</span>
                      <span class="last">$720.00</span>
                    </a>
                  </li>
                  <li>
                    <a href="#"
                      >Fresh Brocoli
                      <span class="middle">x 02</span>
                      <span class="last">$720.00</span>
                    </a>
                  </li> --}}
                </ul>
                <ul class="list list_2">
                  <li>
                    <a href="#"
                      >Subtotal
                      <span> ₦ {{   Cart::subtotal() }}</span>
                    </a>
                  </li>
                  <li>
                        <a href="#"
                          >Tax
                          <span> ₦ {{   Cart::tax() * 100}}</span>

                        </a>
                      </li>



                  <li>
                    <a href="#"
                      >Total
                      <span>{{ presentPrice($newTotal) }}</span>
                    </a>
                  </li>

 
<li>

 


    
        <div style="display:block">
                <small class="text-danger">NOTE: DELIVERY FEE IS NOT INCLUSIVE</small>
                <br>
           
               </div>
               <input type="radio"  name="deliverme"   value="1"  > Deliver  
       
 
        <input type="radio"   name="deliverme"   value="0"  checked > Dont Deliver <br>
    



            </li>
 
<hr>
                </ul>


                <div class="payment_item">
                  <div class="radion_btn">
                    <input type="radio" id="f-option5" name="selector" value="banktransfer" />
                    <label for="f-option5">Bank Transfer</label>
                    <div class="check"></div>
                  </div>
                  <p>
                   Pay by Just transfering your total amount directly into our account through mobile banking or physical payment in the bank.
                  </p>
                </div>


                <div class="payment_item active">
                  <div class="radion_btn">
                    <input type="radio" id="f-option6" name="selector" value="atm" />
                    <label for="f-option6">ATM Transfer </label>
                    {{-- <img src="img/product/single-product/card.jpg" alt="" /> --}}
                    <div class="check"></div>
                  </div>
                  <p>
                 Pay Using your Credit / Atm card also bank transfer can be made through this method
                  </p>
                </div>
                <br>
                <div class="creat_account">
                  <input type="checkbox" id="f-option4" name="terms" />
                  <label for="f-option4">I’ve read and accept the </label>
                  <a href="#">terms & conditions*</a>
                </div>

                <input type="hidden" name="email" value="{{ auth()->user()->email }}">
                <input type="hidden" name="first_name" value="{{ auth()->user()->name}}">

                @if ($deliver == 1)

                <input type="hidden" name="amount" value="{{($shiping) * 100  }} "> {{-- required in kobo --}}

                @else

                <input type="hidden" name="amount" value="{{($newTotal) * 100  }} ">

                @endif

            <input type="hidden" name="quantity" value="{{json_encode($array = ['quantity' => Cart::instance('default')->count()


            ])

        }}">


                <input type="hidden"  name="metadata" value="{{ json_encode($array = ['quantity' => Cart::instance('default')->count(),
                'discount' => collect(session()->get('coupon'))->toJson(),

                'number' => '<label for="shipping-address-1" id="shiadd1"></label>',
             



                ]) }}" >

                <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                {{-- <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">  --}}
                {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                 <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

{{--
                <p>
                  <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
                  <i class="fa fa-plus-circle fa-lg"></i> Pay Now! <div class="text-center">    {{ presentPrice($newTotal) }}</div>
                  </button>
                </p> --}}
<br>
                <button class="main_btn" >Proceed to Payment</button>
              </div>
            </div>
          </div>
        </div>
      </div>
        </form>
    </section>


    <script>
    $("#number").change(function () {
    var addressArray = [$("#address").val()];
    $("#shiadd1").text(addressArray.join(' '));
});
    
    </script>
    <!--================End Checkout Area =================-->
    @endsection
