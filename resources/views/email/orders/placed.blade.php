@component('mail::message')
# Order Received

Thank you for your order.

**Order ID:** {{ $order->billing_unique_id }}

**Order Email:** {{ $order->billing_email }}

**Order Name:** {{ $order->billing_name }}
<!--
**Order Total:** ${{ round($order->billing_total / 100, 2) }} -->
@if ( $order->delivery != 1)
**Tax:** {{round($order->billing_tax)}}
@endif

 
@if ( $order->delivery == 1)

**Order Total:** ₦{{ (round($order->billing_total) - round($order->billing_tax)) + 500}}

@else

**Order Total:** ₦{{ round($order->billing_total) }}
@endif


**Items Ordered**
<hr />
@foreach ($orderd as $product)



Name: {{ $product->name }} <br>
<!-- Price: ${{ round($product->price / 100, 2)}} <br> -->

Price: ₦{{ round($product->price)}} <br>
Quantity: {{ $product->order_quantity }} <br>

<hr />
@endforeach

You can get further details about your order by logging into our website.

@component('mail::button', ['url' => config('app.url'), 'color' => 'green'])
Go to Website
@endcomponent

Thank you again for choosing us.

Regards,<br>
{{ config('app.name') }}
@endcomponent
