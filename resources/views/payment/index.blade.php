@extends('layouts.app')

@section('content')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<style>

.panel
{
    text-align: center;
}
.panel:hover { box-shadow: 0 1px 5px rgba(0, 0, 0, 0.4), 0 1px 5px rgba(130, 130, 130, 0.35); }
.panel-body
{
    padding: 0px;
    text-align: center;
}

.the-price
{
    background-color: rgba(220,220,220,.17);
    box-shadow: 0 1px 0 #dcdcdc, inset 0 1px 0 #fff;
    padding: 20px;
    margin: 0;
}

.the-price h1
{
    line-height: 1em;
    padding: 0;
    margin: 0;
}

.subscript
{
    font-size: 25px;
}

/* CSS-only ribbon styles    */
.cnrflash
{
    /*Position correctly within container*/
    position: absolute;
    top: -9px;
    right: 4px;
    z-index: 1; /*Set overflow to hidden, to mask inner square*/
    overflow: hidden; /*Set size and add subtle rounding  		to soften edges*/
    width: 100px;
    height: 100px;
    border-radius: 3px 5px 3px 0;
}


.cnrflash-ultimate
    {
        /*Set position, make larger then 			container and rotate 45 degrees*/
        position: absolute;
        bottom: 0;
        right: 0;
        width: 145px;
        height: 145px;
        -ms-transform: rotate(45deg); /* IE 9 */
        -o-transform: rotate(45deg); /* Opera */
        -moz-transform: rotate(45deg); /* Firefox */
        -webkit-transform: rotate(45deg); /* Safari and Chrome */
        -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
        -ms-transform-origin: 100% 100%;  /* IE 9 */
        -o-transform-origin: 100% 100%; /* Opera */
        -moz-transform-origin: 100% 100%; /* Firefox */
        background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
        background-size: 4px,auto, auto,auto;
        background-color: rgb(240,230,140);
        box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
    }

.cnrflash-inner
{
    /*Set position, make larger then 			container and rotate 45 degrees*/
    position: absolute;
    bottom: 0;
    right: 0;
    width: 145px;
    height: 145px;
    -ms-transform: rotate(45deg); /* IE 9 */
    -o-transform: rotate(45deg); /* Opera */
    -moz-transform: rotate(45deg); /* Firefox */
    -webkit-transform: rotate(45deg); /* Safari and Chrome */
    -webkit-transform-origin: 100% 100%; /*Purely decorative effects to add texture and stuff*/ /* Safari and Chrome */
    -ms-transform-origin: 100% 100%;  /* IE 9 */
    -o-transform-origin: 100% 100%; /* Opera */
    -moz-transform-origin: 100% 100%; /* Firefox */
    background-image: linear-gradient(90deg, transparent 50%, rgba(255,255,255,.1) 50%), linear-gradient(0deg, transparent 0%, rgba(1,1,1,.2) 50%);
    background-size: 4px,auto, auto,auto;
    background-color: #aa0101;
    box-shadow: 0 3px 3px 0 rgba(1,1,1,.5), 0 1px 0 0 rgba(1,1,1,.5), inset 0 -1px 8px 0 rgba(255,255,255,.3), inset 0 -1px 0 0 rgba(255,255,255,.2);
}
.cnrflash-inner:before, .cnrflash-inner:after
{
    /*Use the border triangle trick to make  				it look like the ribbon wraps round it's 				container*/
    content: " ";
    display: block;
    position: absolute;
    bottom: -16px;
    width: 0;
    height: 0;
    border: 8px solid #800000;
}
.cnrflash-inner:before
{
    left: 1px;
    border-bottom-color: transparent;
    border-right-color: transparent;
}
.cnrflash-inner:after
{
    right: 0;
    border-bottom-color: transparent;
    border-left-color: transparent;
}
.cnrflash-label
{
    /*Make the label look nice*/
    position: absolute;
    bottom: 0;
    left: 0;
    display: block;
    width: 100%;
    padding-bottom: 5px;
    color: #fff;
    text-shadow: 0 1px 1px rgba(1,1,1,.8);
    font-size: 0.95em;
    font-weight: bold;
    text-align: center;
}

</style>
<div class="container">
	<!-- Start Causes Area -->
	<section class="causes-area section-gap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-7 section-title">
                        {{-- <p class="search-results-count">{{ $states->count() }} people / person going from '{{ request()->input('stateFrom') }}' to '{{ request()->input('stateTo') }}' </p> --}}
					<h2>PAYMENT STEPS</h2>
					<p>
					Please kindly follow the simple steps below to make your payment.
					</p>
				</div>
			</div>



            <div class="row">
                <div class="col-md-6">
<ul class="text-center">
<li><h3>
        Make Payment of your Plan into the account details below
</h3>

</li>

<li style="border: 1px solid #c3c3c3;">
DIAMOND BANK <br>
0106920081 <br>
Kelvin Oni Ibulu
    </li>


    <li style="border: 1px solid #c3c3c3 ; padding:1em 1em 1em 1em">
    <div style="color:green">Upload Payment Confirmation</div>
    {!! Form::open(['action' => 'PaymentplansController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <input type="file"  name="images[]" multiple required />

        @if ($errors->any())
        <div > <b style="color:red">{{ implode('', $errors->all(':message')) }}  </b> </div>
    @endif


        {{Form::submit('Submit', ['class'=>'btn btn-danger'])}}
        {!! Form::close() !!}
        </li>


        @if (!empty($paymentplans))
        <li style="border: 1px solid #c3c3c3 ; padding:1em 1em 1em 1em">
               {{-- {{dd($paymentplans)}} --}}
               {{-- @foreach ($paymentplans as $paymentplan) --}}
               <div><b>PAYMENT ID</b></div>
               <p id="p1">{{$paymentplans->unique_id}} </p>   <button onclick="copyToClipboard('#p1')">Copy Your ID</button>
               {{-- @endforeach --}}
            </li>



        <li style="border: 1px solid #c3c3c3 ; padding:1em 1em 1em 1em">
           Copy your payment ID. Send us a mail via <a href="mailto:payment@yuthconnect.com">payment@yuthconnect.com</a>,  Use our live chat panel to send us a message and confirm your payment or <a href="/contactus" class="btn btn-success">click here</a> to send us a message with your Payment ID above.
                    </li>

                    <li style="border: 1px solid #c3c3c3 ; padding:1em 1em 1em 1em">
                           You shall recieve an email confirming your payment with yuthconnect or you should see a count down and your plan name on your <a href="/dashboard" class="btn btn-success">Dashboard</a>
                                     </li>

                                     <li style="border: 1px solid #c3c3c3 ; padding:1em 1em 1em 1em">
                                         SUCCESS!!! You may now enjoy our service Based on your plan.
                                                      </li>

                                                      @endif

</ul>

                </div>
                <div class="col-md-6">
<a href="/contactus">
<img src="advertisehere.jpg" alt="" style="width:100%; height:35em">
</a>
                    </div>


                </div>

		</div>
    </section>
</div>
	<!-- end Causes Area -->

    <script>
            function copyToClipboard(element) {
              var $temp = $("<input>");
              $("body").append($temp);
              $temp.val($(element).text()).select();
              document.execCommand("copy");
              $temp.remove();
            }


            </script>
@endsection
