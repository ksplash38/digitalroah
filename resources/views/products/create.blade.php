@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(isset($dataTypeContent->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
 
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container">
        <div class="row">
            <div class="col-md-12">
 
 

	     


        <h1>Create Product </h1>
        @if ($errors->any())
        <div style="color:red">
                {{ implode('', $errors->all(':message')) }}
        </div>
       
@endif
	    {!! Form::open(['action' => 'PartnerproductController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
 


	    <div class="form-group">


	                             


	    </div>

        {{ Form::label('company_id', 'Company:') }}
        <input type="text" value="{{$companies->title}}" name="company_id" class="form-control" disabled>
     
	        <div class="form-group">
	            {{Form::label('name', 'Name')}}
	            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'name'])}}
	        </div>

            <div class="form-group">
                    {{Form::label('description', 'Description')}}
                    {{Form::textarea('description', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'description'])}}
                </div>
					<div class="form-group">
							{{Form::label('quantity', 'Quantity')}}
							{{Form::text('quantity', '', ['class' => 'form-control', 'placeholder' => 'Quantity'])}}
					</div>



	   

	        <div class="form-group">
	            {{Form::label('price', 'Price ₦')}}
	            {{Form::text('price', '', ['class' => 'form-control', 'placeholder' => 'price'])}}
            </div>
            
            <div class="form-group">
                    {{Form::label('oldprice', 'Old Price (optional) ₦')}}
                    {{Form::text('oldprice', '', ['class' => 'form-control', 'placeholder' => 'optional previous slashed'])}}
                </div>

	    

	        <div class="form-group">
                <h5>Cover Image</h5>
	            {{Form::file('image')}}
	        </div>
<h5>Images</h5>
	        <input required type="file" class="form-control" name="images[]" placeholder="address" multiple>




	        <!-- ?php

	            echo uniqid('id' , false), "\t",  uniqidReal(), PHP_EOL;

	        ? -->
 

 
		{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
	 
 

            </div></div></div>

@endsection



@section('scripts')


	{!! Html::script('select2/js/select2.min.js') !!}

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
