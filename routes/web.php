<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('send', 'HomeController@sendNotification');

Route::get('/payment', 'PaymentplansController@index')->name('payment');
Route::post('paymentpending', 'PaymentplansController@store');
// Route::get('contactus', 'ConactusController@index');

Route::get('/display', 'PaymentplansController@display')->name('display');
Route::delete('/delete/{id}', 'PaymentplansController@destroy')->name('destroy.display');

Route::get('/events', 'EventController@index')->name('event.index');

Route::get('/news/create', 'NewsController@create')->name('news.create');
Route::post('/news/store', 'NewsController@store')->name('news.store');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/dashboard', 'DashboardController@overview')->name('overview');
Route::get('/search-side', 'HomeController@searchSide')->name('searchSide');
Route::get('/news/show/{randomid}', 'NewsController@show')->name('news.show');
Route::get('/accountsettings/{unique_id}', 'DashboardController@accountSettings')->name('dashboard.accountsetting');
Route::put('/accountupdate/{user}', 'DashboardController@update')->name('dashboard.update');
Route::put('/expired/{user}', 'DashboardController@expired')->name('dashboard.expired');

Route::post('/comment/store', 'CommentController@store')->name('comment.add');
Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/contactus', 'ContactusController@contactus')->name('contactus');
Route::post('/contactussent', ['as'=>'contactus.store','uses'=>'ContactusController@contactUSPost']);

Route::get('/pdf', 'DashboardController@pdf');

Route::get('/sendrequest/{state}', 'friendrequestController@user_can_send_a_friend_request')->name('request.send');
Route::get('/requestaccepted/{item}', 'friendrequestController@friend_request_is_accepted')->name('request.accepted');

Route::get('/seefriends', 'friendrequestController@index')->name('request.index');
Route::get('/searcheduserprofile/{state}', 'DashboardController@searchuserprofile')->name('request.profile');


Route::get('/delivery', 'DeliveryController@location')->name('delivery.location');
Route::get('/deliverycompany/{presentlocation}', 'DeliveryController@company')->name('delivery.company');
Route::get('/deliveryproduct/{company}', 'DeliveryController@product')->name('delivery.product');

Route::get('/ajax-subcat', 'DeliveryController@getSubCategories')->name('submission');
Route::get('/product/{randomid}', 'DeliveryController@show')->name('product.show');

Route::post('/cart', 'CartController@store')->name('cart.store');
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');

Route::post('/coupon', 'CouponsController@store')->name('coupon.store');
Route::delete('/coupon', 'CouponsController@destroy')->name('coupon.destroy');

// Route::post('/checkoutnow', 'CheckoutController@deliver')->name('checkout.delivery')->middleware('auth');
Route::get('/checkout', 'CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout', 'CheckoutController@store')->name('checkout.store');
Route::post('/checkoutpayment', 'CheckoutController@bankPayment')->name('checkout.bankPayment');
Route::post('/paymentcheck', 'CheckoutController@paymentcheck')->name('checkout.checkpayment');

Route::post('/paybillcheck', 'CheckoutController@storevalue')->name('pay.storebilling');

Route::get('/card', 'CheckoutController@card')->name('pay.card');
Route::post('/pay', 'CheckoutController@redirectToGateway')->name('pay');
Route::get('/payment/callback', 'CheckoutController@handleGatewayCallback')->name('callb');
Route::get('/payment/confirmed', 'CheckoutController@paymentconfirm')->name('paymentconfirm');
Route::get('/payment/mae', 'CheckoutController@checkme')->name('checkme');
Route::get('/trackme' ,'PartnertransportrecieptController@index')->name('checkme.reciept');
Route::post('/delivered' ,'PartnertransportrecieptController@delivered')->name('item.delivered');




Route::get('/thankyou', 'CheckoutController@paymentconfirm')->name('paymentconfirm');








Route::get('/category-search', 'DeliveryController@categorysearch')->name('searchnow');


Route::post('/partner', 'PartnerproductController@store')->name('partner.product');
Route::post('/tranreciept', 'PartnertransportrecieptController@store');

Route::put('/partnerview/{product}', 'PartnerproductController@update')->name('partner.update');


Route::post('/routestransport', 'RoutestransportpartnersController@store')->name('route.store');
Route::put('/routestransport/{routestransport}', 'RoutestransportpartnersController@update')->name('route.update');
// Route::get('/cart', 'CartController@index')->name('delivery.product');

// Route::get('/single', 'CartController@index')->name('delivery.product');
