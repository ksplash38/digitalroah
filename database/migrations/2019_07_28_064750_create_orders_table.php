<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('user_id')->unsigned()->nullable();
            // $table->foreign('user_id')->references('id')->on('users')
            //       ->onUpdate('cascade')->onDelete('set null');
            $table->string('user_name')->nullable();
            $table->string('billing_email');
            $table->string('billing_name');
            $table->string('billing_address');
            $table->string('billing_state');
            $table->string('reference')->nullable();
            $table->string('authorization')->nullable();
            // $table->string('billing_province');

            $table->string('billing_phone');
            $table->string('billing_name_on_card')->nullable();
            $table->integer('billing_discount')->default(0);
            $table->string('billing_discount_code')->nullable();
            $table->integer('billing_subtotal');
            $table->integer('billing_tax');
            $table->boolean('delivery')->nullable();
            $table->integer('billing_total');
            $table->string('expired_time')->nullable();
            $table->string('payment_gateway')->default('paystack');
            $table->string('status')->default('Not Attended');
            $table->boolean('shipped')->default(false);
            $table->string('error')->nullable();
            $table->string('billing_unique_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
