<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransRecieptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_reciepts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('passengername');
            $table->text('currentlocation');
            $table->text('passengerroute');
            $table->text('passengernumber');
            $table->text('drivername');
            $table->text('drivernumber');
            $table->text('price');
            $table->text('booking_id');
            $table->text('homedelivery');
            $table->text('pickupdelivery');
            $table->text('trans_companyname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_reciepts');
    }
}
