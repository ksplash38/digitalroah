$(function () {

    /* Add class active to first li content */
    function tabContent(event) {
        /* getting id */
        var criteriaId = event.target.hash.substr(1);
    }

    /*load content if clicked*/
    $("#ratingTab li:first").addClass("active");
    if($("#ratingTab li:first").hasClass("active")) {
        setTimeout(function(){$("#ratingTab li a:first").trigger("click");},800);
    }

    /* Rating Tabs */
    $(document).on("click", "#ratingTab li a", tabContent);

});
