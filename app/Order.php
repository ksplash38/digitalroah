<?php

namespace App;
use Ramsey\Uuid\Uuid;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{


// The controller for this is checkout controller


    protected $fillable = [
        'user_id','user_name', 'billing_email', 'billing_name', 'billing_address', 'billing_state',
        'billing_province', 'billing_postalcode', 'billing_phone', 'billing_name_on_card', 'billing_discount',
        'billing_discount_code', 'billing_subtotal', 'billing_tax', 'delivery', 'billing_total', 'billing_unique_id','expired_time', 'payment_gateway', 'error',  'reference', 'note'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('order_quantity');
    }






    // public function save(array $options = [])
    // { if(!empty(\Auth::user()->id)){
    //     $this->user_id = \Auth::user()->id;
    //     $this->user_name = \Auth::user()->username;
    //           $this->billing_unique_id = Uuid::uuid1();
    //           $this->expired_time = Carbon::now()->addMinutes(2);
    //           $this->billing_unique_id = Uuid::uuid1();

    //         parent::save();




    //     }
    //         else{
    //           parent::save([]);
    //         }

    // }




}
