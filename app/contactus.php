<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contactus extends Model
{
    public $table = 'contactus';

    public $fillable = ['name','category','subject','mobile','email','payment_id','message'];
}
