<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\User;
use App\States;
use App\Batches;
use App\Years;
use Auth;
use Carbon\Carbon;
use App\Http\Requests\UpdateProfile;
use Facades\jpmurray\LaravelCountdown\Countdown;
use Redirect;
use PDF;
use App;
use Illuminate\Support\Facades\Storage;
use Request;

use Intervention\Image\ImageManagerStatic as Image;
class DashboardController extends Controller
{


 public function __construct()
 {
     $this->middleware('auth');
 }


   public function overview()
   {


//     $user_id = auth()->user()->id;
//     $user = User::find($user_id);
//     $expire = Auth::user()->planExpire;
//     // dd($expire);
//     $now = Carbon::now();
//     // $countdown->minutes(5); // 15
//     // dd($countdown);
//     $countdown = Countdown::from($now)
//              ->to($expire)
//              ->get();

//             //  dd($countdown);

//              if ($countdown === $expire) {

//                 $user->planExpire = '00.00.00';
//                 $user->planName = 'Free';

//                 dd($user);
//                 $user->save();
//              } else {
//                  # code...
//              }


//              $user_id = auth()->user()->id;
//        $user = User::find($user_id);
//        $user->elapsed('planExpire'); // get the time elapsed between the date in attribute trial_ends_at to now
//        dd($user);
// $user->until('planExpire');

       $user_id = auth()->user()->id;
       $user = User::find($user_id);

    //    $wallet = Wallet::find($user_id);
       // $referred_by = DB::table('users')
       // ->select('referred_by')->selectRaw('COUNT(*) AS count')
       // ->groupBy('referred_by')->orderByDesc('count')->limit(5)
       // ->first();

      return view('dashboard.overview')->with('user', $user );

       // $company = Company::all();
       //  $user = User::all();




 // $categories = Category::with('users')->get();



//   return  view('companies.index')->with([
//       'companies' => $company,
//       'users' => $user,

//   ]);


   }



   public function accountSettings(Request $request, User $user)
   {
    $states = States::all();
    $batches = Batches::all();
    $years = Years::all();
       $user = User::find(Auth::id());


       $viewData = [
           'user' => $user,
       ];
       // render view with data
       return view('dashboard.accountsetting', $viewData ,  compact('states','batches', 'years'));


   }

   public function update(UpdateProfile $request, User $user)
   {
// form data

$data = User::find(Auth::id());

if(Request::hasFile('image')){
    // Get filename with the extension
    $image  = Request::file('image');
    $filenameWithExt    = $image->getClientOriginalName();
    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
    $extension =  Request::file('image')->getClientOriginalExtension();
    $image_resize = Image::make($image->getRealPath());
    $image_resize->resize(200, 200);
    $fileNameToStore= $filename.'_'.time().'.'.$extension;
    $image_resize->save(public_path('storage/'.  $fileNameToStore ));
    // if( !empty($data->image)){
    $oldFilename = $data->image;
    Storage::delete($oldFilename);
// }
}


if(Request::file('image')){
    $data->image = $fileNameToStore;

}

$data->save();


$data = $request->except('image');



$user->update($data);


return redirect(route('overview', ['user' => $user]))
           ->with('info', 'Your profile has been updated successfully.');
}

public function expired(Request $request, $user)
{
// form data
$user= User::findOrFail($user);
$user->planExpire = '00.00.00';
$user->planName = 'Free';
// dd($user);
// $data = $request->all();
// $user->update($data);
$user->save();

return redirect(route('overview', ['user' => $user]))
        ->with('info', 'Your profile has been updated successfully.');
}

public function pdf(){

    $user_id = auth()->user()->id;
    $users = User::find($user_id);
    $pdf = App::make('dompdf.wrapper');
    $pdf = PDF::loadView('dashboard.pdfs', array('users' => $users));
    return $pdf->stream();
   }


   public function searchuserprofile($unique_id){


    $users = User::where('unique_id', $unique_id)->firstOrFail();
    return view('dashboard.searcheduserprofile',  compact('users'));
   }

}
