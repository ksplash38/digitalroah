<?php

namespace App\Http\Controllers;
use App\User;
use App\Order;
use App\Product;
use App\OrderProduct;
use App\Mail\OrderPlaced;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CheckoutRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Cartalyst\Stripe\Exception\CardErrorException;
use Carbon\Carbon;
use Stripe\Customer;
use DB;
use Ramsey\Uuid\Uuid;
use Paystack;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use therealsmat\Ebulksms\EbulkSMS;
class CheckoutController extends Controller
{





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//     public function deliver(Request $request)
//     {

//         $deliver = $request->input('yesno');

//     Session::put('deliver', $deliver);
//     return redirect()->route('checkout.index');
// }




// public function paymentcheck(Request $request)
// {
//    $validator = Validator::make($request->all(),[
//         'selector'=> 'required|in:atm,banktransfer',
//         'terms'=> 'required'
//    ] ,[
//     'selector.required' => 'Please Select Payment Method',
//     'terms.required' => ' Also Accept our terms & conditions'

// ] );



//     if($validator->fails()) {
//         return Redirect::back()->withErrors($validator);
//     }


//     if ($request->input('selector') == 'atm'){


//         dd('hi');
//         return redirect()->route('pay');


//     }elseif($request->input('selector') == 'banktransfer'){

//         $this->redirectToGateway();
//     }



// }
    public function index()
    {
        if (Cart::instance('default')->count() == 0) {
            return redirect()->route('shop.index');
        }

        if (auth()->user() && request()->is('guestCheckout')) {
            return redirect()->route('checkout.index');
        }
        //
        // if (auth()->user() && request()->is('bankpayment')) {
        //     return redirect()->route('bankpayment');
        // }

        // $order = Order::where('id', $input['qrcode_id'])->first();
        $order = Order::first();

        $deliver = Session::get('deliver');
// dd($value);
//    Session::forget('deliver');

// dd($forget);
        return view('checkout')->with([
            'discount' => getNumbers()->get('discount'),
            'newSubtotal' => getNumbers()->get('newSubtotal'),
            'newTax' => getNumbers()->get('newTax'),
            'newTotal' => getNumbers()->get('newTotal'),
            'shiping' => getNumbers()->get('shiping'),
            'order' => $order,
            'deliver' => $deliver,
        ]);
    }





    public function card()
    {
        return view('payment') ;


    }

    public function bank()
    {
        if (Cart::instance('default')->count() == 0) {
            return redirect()->route('shop.index');
        }

        if (auth()->user() && request()->is('guestCheckout')) {
            return redirect()->route('checkout.index');
        }
        //
        // if (auth()->user() && request()->is('bankpayment')) {
        //     return redirect()->route('bankpayment');
        // }

        return view('bankpayment')->with([
            'discount' => getNumbers()->get('discount'),
            'newSubtotal' => getNumbers()->get('newSubtotal'),
            'newTax' => getNumbers()->get('newTax'),
            'newTotal' => getNumbers()->get('newTotal'),
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckoutRequest $request)
    {
        // Check race condition when there are less items available to purchase
        if ($this->productsAreNoLongerAvailable()) {
            return back()->withErrors('Sorry! One of the items in your cart is no longer avialble.');
        }

        $contents = Cart::content()->map(function ($item) {
            return $item->model->slug.', '.$item->qty;
        })->values()->toJson();



        try {
            $charge = Stripe::charges()->create([
                // 'amount' => getNumbers()->get('newTotal') / 100,
                      'amount' => getNumbers()->get('newTotal') ,
                'currency' => 'NGN',
                'source' => $request->stripeToken,
                'description' => 'Order',
                'receipt_email' => $request->email,
                'metadata' => [
                'contents' => $contents,
                'quantity' => Cart::instance('default')->count(),
                'discount' => collect(session()->get('coupon'))->toJson(),
                               ],
                                                ]);

            $order = $this->addToOrdersTables($request, null);
            Mail::send(new OrderPlaced($order));

            // decrease the quantities of all the products in the cart
            $this->decreaseQuantities();

            Cart::instance('default')->destroy();
            session()->forget('coupon');

            return redirect()->route('confirmation.index')->with('success_message', 'Thank you! Your payment has been successfully accepted!');
        } catch (CardErrorException $e) {
            $this->addToOrdersTables($request, $e->getMessage());
            return back()->withErrors('Error! ' . $e->getMessage());
        }
    }



    public function redirectToGateway(Request $request)
    {


//         dd( $order = Order::first());
// dd($request->all());

        $validator = Validator::make($request->all(),[
            'selector'=> 'required|in:atm,banktransfer',
            'terms'=> 'required',
            'state'=> 'required',
            'reference' => 'required|unique:orders',
       ] ,[
        'selector.required' => 'Please Select Payment Method',
        'terms.required' => 'Also Accept our terms & conditions',
        'state.required' => 'Insert Billing State',
        'reference.required' => 'Please refresh your browser & Proceed',
    ] );



        if($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }



// dd($request->all());



            if ($request->input('selector') == 'atm'){


        if ($this->productsAreNoLongerAvailable()) {
            return back()->withErrors('Sorry! One of the items in your cart is no longer avialble.');
        }

        $contents = Cart::content()->map(function ($item) {
            return $item->model->id.', '.$item->qty;
        })->values()->toJson();

try{
        $order = $this->addToOrdersTables($request, null);

      $ref=  $request->input('reference') ;

        $orderd = DB::table('orders as t1')



        ->join("order_product AS t2", "t1.id", "=", "t2.order_id")

        ->join("products AS t3", "t3.id", "=", "t2.product_id")

         ->where("t1.reference", "=", $ref)

          ->get();

// dd($order);
Session::put('order', $order);
Session::put('orderd', $orderd);



            // decrease the quantities of all the products in the cart




            // Cart::instance('default')->destroy();


        } catch (\Exception $ex) {
            return $ex->getMessage();
        }




        return Paystack::getAuthorizationUrl()->redirectNow();

    }elseif($request->input('selector') == 'banktransfer'){
        return redirect()->route('payment');

    }


    }





    protected function addToOrdersTables($request, $error)
    {


       $total = \Purify::clean(getNumbers()->get('newTotal'));

        $code = Session::get('checks') ;
        // Insert into orders table
        $order = Order::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'user_name' => \Auth::user()->username,
            'billing_email' => \Purify::clean($request->email),
            'billing_name' =>\Purify::clean( $request->name),
            'billing_address' => \Purify::clean($request->address),
            'billing_state' => \Purify::clean($request->state),
            'reference' => \Purify::clean($request->reference),
            'delivery' => \Purify::clean($request->deliverme),
            'billing_phone' => \Purify::clean($request->mobilenumber),
            'note' => \Purify::clean($request->message),
            // 'billing_name_on_card' => $request->name_on_card,
            'billing_discount' => \Purify::clean(getNumbers()->get('value')),
            'billing_discount_code' => \Purify::clean($code),
            'billing_subtotal' => \Purify::clean(getNumbers()->get('newSubtotal')),
            'billing_tax' => \Purify::clean(getNumbers()->get('newTax')),
            'billing_total' => $total,
            'payment_gateway' =>  \Purify::clean($request->selector),
            'billing_unique_id' => Uuid::uuid1(),
            'expired_time' => Carbon::now()->addMinutes(2),

            // 'expired_time' =>  date('c',strtotime('+ 2 minutes')),


            'error' => $error,
        ]);

        // Insert into order_product table
        foreach (Cart::content() as $item) {
            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $item->model->id,
                'order_quantity' => $item->qty,
                'order_product_uniqueid' => Uuid::uuid1(),
                'expire_time' => '-1',
            ]);
        }

        return $order;

    }

    protected function decreaseQuantities()
    {
        foreach (Cart::content() as $item) {
            $product = Product::find($item->model->id);

            $product->update(['quantity' => $product->quantity - $item->qty]);
        }
    }

    protected function productsAreNoLongerAvailable()
    {
        foreach (Cart::content() as $item) {
            $product = Product::find($item->model->id);
            if ($product->quantity < $item->qty) {
                return true;
            }
        }

        return false;
    }




    // if($paymentDetails['data']['status'] == 'success'){ //registing new funding $funding = new Funding; $funding->user_id = Auth::User()->id; $funding->farm_id = $farmId; $funding->save(); return redirect whatever view('')->with(success message); }else{ return redirect view('')->with(failed message) }
    public function handleGatewayCallback(Request $request, EbulkSMS $sms)
    {




        $paymentDetails = Paystack::getPaymentData();



        if($paymentDetails['data']['status'] == 'success'){

            $amount=  $paymentDetails['data']['amount'] ;

            $ref=  $paymentDetails['data']['reference'] ;
            // dd(  round(getNumbers()->get('newTotal')));
            // dd( round($amount / 100));
        //   dd(  getNumbers()->get('shiping'));

if(round(getNumbers()->get('newTotal')) == round($amount / 100) ){
    $orderd = DB::table('orders as t1')
             ->where("t1.reference", "=", $ref)->update(['status' => 'Paid'  ]);

             $order = Session::get('order');

             $orderd = Session::get('orderd');
             $textname = $order['billing_name'];
             $textnumber = $order['billing_phone'];
            //  dd($textnumber);
             $message = "Thank you {$textname} for placing an order, Your reference number is {$ref}. We are currently working on it and you will get a call from one of our agents. ASAP";
             $recipients = "{$textnumber}";
              $sms->composeMessage($message)
                         ->addRecipients($recipients)
                         ->send();

                         $message = "New order with ref  {$ref} from {$textnumber}";
                         $recipients = "08184751332";
                          $sms->composeMessage($message)
                                     ->addRecipients($recipients)
                                     ->send();

            }

elseif(round(getNumbers()->get('shiping')) == round($amount  / 100)  ){
    $orderd = DB::table('orders as t1')
    ->where("t1.reference", "=", $ref)->update(['status' => 'Paid + Ship'  ]);
}




        $this->decreaseQuantities();
        Cart::instance('default')->destroy();
        session()->forget('coupon');
        session()->forget('deliver');

        session()->forget('checks');




        Mail::send(new OrderPlaced($order, $orderd));

        session()->forget('order');
        session()->forget('orderd');

        return redirect()->route('paymentconfirm');

}else{
    $orderd = DB::table('orders as t1')
    ->where("t1.reference", "=", $ref)->update(['status' => 'Tampered'  ]);
    Flash::error('Sorry, payment failed');
    //redirect here
    return redirect()->back();
}











            // if($paymentDetails['data']['status'] != 'success'){
            //     Flash::error('Sorry, payment failed');
            //     //redirect here
            //     return redirect()->back();

            // }






        // dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }




    public function paymentconfirm()
    {

        return view('thankyou');

    }


    
    public function bankPayment(CheckoutRequest $request)
    {
        // Check race condition when there are less items available to purchase
        if ($this->productsAreNoLongerAvailable()) {
            return back()->withErrors('Sorry! One of the items in your cart is no longer avialble.');
        }

        $contents = Cart::content()->map(function ($item) {
            return $item->model->slug.', '.$item->qty;
        })->values()->toJson();
        try {


            $order = $this->addToOrdersTables($request, null);


            // decrease the quantities of all the products in the cart
            $this->decreaseQuantities();

            Cart::instance('default')->destroy();
            session()->forget('coupon');

            Mail::send(new OrderPlaced($order));

            return redirect()->route('confirmation.bank')->with('success_message', 'Thank you! Your payment has been successfully accepted!');
        } catch (CardErrorException $e) {
            $this->addToOrdersTables($request, $e->getMessage());
            return back()->withErrors('Error! ' . $e->getMessage());
        }
    }

    protected $dates = ['datetime_column'];




    public function subscribe_process(Request $request)
    {
        try {
            \Stripe\Stripe::setApiKey("sk_test_tWYKjH4xVWk8dx0AO7gSlatV");

            $user_id = auth()->user()->id;
            $user = User::find($user_id);
            $user->newSubscription('main', 'bronze')->create($request->stripeToken);

            return 'Subscription successful, you get the course!';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

    }

}
