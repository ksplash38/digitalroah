<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Jobs\UpdateCoupon;
use Illuminate\Http\Request;
use App\User;
use Gabievi\Promocodes\Facades\Promocodes;
use Session;
use DB;
use Illuminate\Support\Facades\Input;
class CouponsController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $code  = \Purify::clean($request->get('coupon_code'));

        $checks = Promocodes::check($code)['code']   ;

        $check = Promocodes::check($code)['reward']  ;


      Promocodes::redeem($code);


    //   session()->put('coupon', [
    //     'name' => $this->coupon->code,
    //     'discount' => $this->coupon->discount(Cart::subtotal()),
    // ]);

       Session::put('coupon', $check);

       Session::put('checks', $checks);

    //    $promo = Promocodes::createDisposable( 1,  null, [], null, null);
    // dd($promo);
        // $coupon = Coupon::where('code', $request->coupon_code)->first();

        // if (!$coupon) {
        //     return back()->withErrors('Invalid coupon code. Please try again.');
        // }

        // dispatch_now(new UpdateCoupon($coupon));

        return back()->with('success_message', 'Coupon has been applied!');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {


        $user_id = auth()->user()->id;
        $user = User::find($user_id);


//         $query = 'DELETE user_id, promocode_id FROM promocode_user
//         INNER JOIN users ON users.id = promocode_user.user_id
//         WHERE promocode_user.user_id = user_id';

// \DB::delete($query, array($id));


        $products = DB::table('promocode_user as t1')

        ->select("t1.user_id" , "promocode_id")

        ->join("users AS t2", "t1.user_id", "=", "t2.id")

        ->where("user_id", "=", $user_id)

        ->delete();




        // $products->delete();
        Session::forget('coupon');
        Session::forget('checks');
        return back()->with('success_message', 'Coupon has been removed.');
    }
}
