<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presentlocation;
use App\Company;
use App\Product;
use Illuminate\Support\Facades\URL;
use DB;
use Session;

use Illuminate\Support\Facades\Input;

use Response;
class DeliveryController extends Controller
{
    public function location()
    {

        // $date = Carbon::now()->format('Y-m-d');
        // // $now = Carbon::now()->format('H:i:s');
        // // $now = Carbon::now()->format('m-d-Y H:i:s');
        // $time = Carbon::now()->format('H:i:s');
        // dd($now);

        $presentlocations = Presentlocation::all();
        // dd($presentlocations);


        return view('delivery.location')->with([

            'presentlocations' => $presentlocations,
        ]);
    }


    public function company($id)
    {



      $companies = DB::table('companies as t1')

->select("t1.title","t1.description","t1.id", "t2.presentlocation_name")

->join("presentlocations AS t2", "t1.presentlocations_id", "=", "t2.id")

 ->where("presentlocations_id", "=",  $id)

  ->get();

 
        // $date = Carbon::now()->format('Y-m-d');
        // // $now = Carbon::now()->format('H:i:s');
        // // $now = Carbon::now()->format('m-d-Y H:i:s');
        // $time = Carbon::now()->format('H:i:s');
        // dd($now);


        // dd($presentlocations);


        return view('delivery.company')->with([

            'companies' => $companies,
        ]);
    }



    public function product($id)
    {

// dd($id);

$companies = Company::all();
$presentlocations = Presentlocation::all();

      $products = DB::table('companies as t1')

->select("t1.id","t1.title", "t2.name", "t2.name","t2.description", "t2.image", "t2.price", "t2.randomid", "t2.oldprice")

->join("products AS t2", "t1.id", "=", "t2.company_id")

 ->where("company_id", "=", $id)

  ->get();

 
  
  Session::put('url', URL::current() );

  
 

$productname =$products->first();
  if (request()->subcat) {

dd('hi');

}


        // $date = Carbon::now()->format('Y-m-d');
        // // $now = Carbon::now()->format('H:i:s');
        // // $now = Carbon::now()->format('m-d-Y H:i:s');
        // $time = Carbon::now()->format('H:i:s');
        // dd($now);


        // dd($presentlocations);


        return view('companyproduct.category')->with([
            'companies' => $companies,
            'products' => $products,
            'presentlocations' => $presentlocations,
            'productname' => $productname,
            
        ]);
    }




    public function show($randomid)
{
    // $promo =   Promocodes::all();
    // $promo =     Promocodes::create(1, 25, ['foo' => 'bar', 'baz' => 'qux']);


    $company = Company::pluck('id');
    $product = Product::where('randomid', $randomid)->firstOrFail();
 
    // $product = Product::where('randomid', $randomid)->firstOrFail();
    $mightAlsoLike = Product::where('randomid', '!=', $randomid)->mightAlsoLike()->get();
    $stockLevel = getStockLevel($product->quantity);
    // $search = Product::where('randomid', $randomid)->firstOrFail()->pluck('');

    $productname = DB::table('companies as t1')

    ->select("t1.title")

    ->join("products AS t2", "t1.id", "=", "t2.company_id")

    ->where("company_id", "=", $company)



    ->first();
    $sponsored  = Product::where('featured', '=', 1 )->inRandomOrder()->get();
    // dd($products);
//     $previous_post = News::where('id','<', $news->id)->max('id');

//     $next_post = News::where('id','>', $news->id)->min('id');



// $previous = News::find($previous_post);
// $next = News::find($next_post);

    // $recentposts = News::where('created_at', '>', Carbon::today()->subDays(1))->take(5)->get();
    // $mightAlsoLike = News::where('randomid', '!=', $randomid)->mightAlsoLike()->get();
    // $adverts = Advert::inRandomOrder()->first();

    // dd($mightAlsoLike);
    return view('companyproduct.single-product', compact('product', 'productname'))
    ->with([
        'mightAlsoLike' => $mightAlsoLike,
        'product' => $product,
     
        'stockLevel' => $stockLevel,
        'sponsored' => $sponsored ,
        // 'recentposts' => $recentposts,
        // 'adverts' => $adverts,
        // 'previous' => $previous,
        // 'next' => $next,
    ])
    ;
}



    public function categorysearch(Request $request)
    {
        $companies = Company::all();
        $presentlocations = Presentlocation::all();

         $company = Input::get ( 'company' );
         $keyword = Input::get ( 'keyword' );
        //  $state = Input::get ( 'state' );
         $location = Input::get ( 'location' );
         


         $query =   DB::table('companies as t1')

        //  ->select("t1.id", "t2.name","t2.description", "t2.image", "t2.price", "t2.randomid")
         
         ->join("products AS t2", "t1.id", "=", "t2.company_id")
         ->join("presentlocations AS t3", "t1.presentlocations_id", "=", "t3.id")
         
        //   ->where("company_id", "=", 2)
         
        ->orderBy('price','asc');

        //    dd($query);

         // $query = Product::orderBy('price','asc');
        //  $query = Product::orderBy('price','asc');
         if ((! is_null($request->min_price)) && (is_null($request->max_price)) ) {
            // fetch all lesser than or equal to max_price
          $query =  $query->where('price', '>=', $request->min_price)  ;
        }
        elseif ((! is_null($request->max_price)) && (is_null($request->min_price))) {
            // fetch all lesser than or equal to max_price
            $query =  $query->where('price', '<=', $request->max_price);
        }

         elseif(! (is_null($request->min_price) && is_null($request->max_price))){
          
            // This will only execute if you received any price
            // Make you you validated the min and max price properly
            $query = $query->where('price','>=',$request->min_price);
            // dd($query);
            $query = $query->where('price','<=',$request->max_price);

            // $query = $query->whereBetween('price', [$request->min_price, $request->max_price]);
        }
   
   
    


         if(! (is_null($request->company))){
             // This will only execute if you received any keyword
         $query = $query->Where('title','LIKE','%'.$company.'%');
         } 

         if(! (is_null($request->location))){
            // This will only execute if you received any keyword
        $query = $query->Where('presentlocation_name','LIKE','%'.$location.'%');
        } 

      
 

           elseif (! is_null($request->keyword)) {
         // fetch all lesser than or equal to max_price
         $query =  $query->where('presentlocation_name','LIKE','%'.$keyword.'%')->orWhere('name','LIKE','%'.$keyword.'%')->orWhere('title','LIKE','%'.$keyword.'%');
     }



     // if none of them is null
    // elseif (! (is_null($request->min_price) && is_null($request->max_price))) {
    //      // fetch all between min & max prices
    // $query =  $query->where('name','LIKE','%'.$keyword.'%')->where('description','LIKE','%'.$keyword.'%')->where('country','LIKE','%'.$keyword.'%')->where('brand_name','LIKE','%'.$keyword.'%')->where('details','LIKE','%'.$keyword.'%')->whereBetween('price', [$request->min_price, $request->max_price]);
    //  }

  
     // // if just min_price is available (is not null)
     // elseif (! is_null($min_price)) {
     //     // fetch all greater than or equal to min_price
     //     $query =  $query->where('price', '>=', $request->min_price);
     // }
     // // if just max_price is available (is not null)
     // elseif (! is_null($max_price)) {
     //     // fetch all lesser than or equal to max_price
     //     $query =  $query->where('price', '<=', $request->max_price);
     // }
     //
     // elseif (! is_null($request->keyword)) {
     //     // fetch all lesser than or equal to max_price
     //     $query =  $query->where('name','LIKE','%'.$keyword.'%')->orWhere('description','LIKE','%'.$keyword.'%')->orWhere('Country','LIKE','%'.$keyword.'%')->orWhere('details','LIKE','%'.$keyword.'%');
     // }


     $products = $query->get();
     // return view('frontend.shop', compact('products'));
     // if($request->keyword){
     //     // This will only execute if you received any keyword
     //     $query = $query->where('name','LIKE','%'.$keyword.'%')->orWhere('description','LIKE','%'.$keyword.'%')->orWhere('Country','LIKE','%'.$keyword.'%')->orWhere('details','LIKE','%'.$keyword.'%');
     //     // ->orWhere('address','LIKE','%'.$keyword.'%');
     // }

            // $products = Product::orderBy('price', 'asc')->get();
            return view('category-search', compact( 'products' ))->with([
                'companies' => $companies,
                // 'products' => $products,
                'presentlocations' => $presentlocations,
            ]);
        

      
    }

    public function getSubCategories()
    {

        // error_log('Some message here.');
        $cat_id = Input::get('cat_id');

        $subcategories = DB::table('companies')->where('presentlocations_id','=',$cat_id)->get();

        // $subcategories = DB::table('companies')->where('presentlocation_id','=',$cat_id)->lists('title');

         return Response::json($subcategories);


    }




}
