<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;
use App\News;
use Stevebauman\Purify\Purify;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'comment_body' => 'required',
        ]);

        $comment = new Comment;
        $comment->body = \Purify::clean($request->get('comment_body'));
        $comment->user()->associate($request->user());
        $news = News::find($request->get('post_id'));
        $news->comments()->save($comment);
      
      
        return back();
    }


    public function replyStore(Request $request)
    {

        $this->validate($request, [
            'comment_body' => 'required',
        ]);

        $reply = new Comment();

        $reply->body = \Purify::clean($request->get('comment_body'));

        $reply->user()->associate($request->user());

        $reply->parent_id = \Purify::clean($request->get('comment_id'));

        $news = News::find($request->get('post_id'));

        $news->comments()->save($reply);

        return back();

    }


}
