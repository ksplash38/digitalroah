<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;
use Gabievi\Promocodes\Facades\Promocodes;
use Session;
class CartController extends Controller
{

//   public function __construct()
//   {
//       $this->middleware('auth');
//   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
        // $promo =     Promocodes::create(4, 25, ['foo' => 'bar', 'baz' => 'qux'], 2, true );

    //   dd( Cart::subtotal() ) ;
    if(Session::get('checks') != 'null' || Session::get('checks') != ''){
        $code =  Session::get('checks')  ;

    }

    $urllink =  Session::get('url')  ;

    // Session::forget('checks');
// $meme = Session::get('coupon');
//        dd($meme);
  $item = Product::all(['id', 'quantity']);
        $mightAlsoLike = Product::mightAlsoLike()->get();

        return view('cart', compact('code', 'urllink'))
        ->with([
            'mightAlsoLike' => $mightAlsoLike,
            'discount' => getNumbers()->get('discount'),
            'newSubtotal' => getNumbers()->get('newSubtotal'),
            'shiping' => getNumbers()->get('shiping'),
            'newTax' => getNumbers()->get('newTax'),
            'newTotal' => getNumbers()->get('newTotal'),
            'value' => getNumbers()->get('value'),


        ])
        ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
       
        });
        

        if ($duplicates->isNotEmpty()) {
            Session::flash('success_message', 'Item is already in your cart!'); 
          return redirect()->back();
        }

  
       $productPrice =  Product::where('id', $request->id)->first();
       $price = $productPrice['price'];
 
       Cart::add($request->id, $request->name,  $request->quantity, $price)
            ->associate('App\Product');

        // return redirect()->route('cart.index')->with('success_message', 'Item was added to your cart!');
  return redirect()->back()->with('success_message', 'Item was added to your cart!');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $validator = Validator::make($request->all(), [
            // 'quantity' => 'required|numeric|between:1,10'
        ]);

        if ($validator->fails()) {
            session()->flash('errors', collect(['Quantity must be from 1.']));
            return response()->json(['success' => false], 400);
        }

        if ($request->quantity > $request->productQuantity) {
            session()->flash('errors', collect(['We currently do not have enough items in stock.']));
            return response()->json(['success' => false], 400);
        }

        Cart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);

        return back()->with('success_message', 'Item has been removed!');
    }

    /**
     * Switch item for shopping cart to Save for Later.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToSaveForLater($id)
    {
        $item = Cart::get($id);

        Cart::remove($id);

        $duplicates = Cart::instance('saveForLater')->search(function ($cartItem, $rowId) use ($id) {
            return $rowId === $id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', 'Item is already Saved For Later!');
        }

        Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price)
            ->associate('App\Product');

        return redirect()->route('cart.index')->with('success_message', 'Item has been Saved For Later!');
    }
}
