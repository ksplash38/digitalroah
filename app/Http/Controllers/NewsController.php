<?php

namespace App\Http\Controllers;
use App\News;
use App\Advert;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Adverts;
use T1k3\LaravelCalendarEvent\Models\CalendarEvent;
use T1k3\LaravelCalendarEvent\Enums\RecurringFrequenceType;
class NewsController extends Controller
{
    // public function __construct()
    // {
    //     return $this->middleware('auth');
    // }

    public function index()
{

    $date = Carbon::now()->format('Y-m-d');
    // $now = Carbon::now()->format('H:i:s');
    // $now = Carbon::now()->format('m-d-Y H:i:s');
    $time = Carbon::now()->format('H:i:s');
    // dd($now);

    $ifconditiondate = News::orderBy('eventdate', 'desc')->pluck('eventdate')->first();
    $ifconditiontime = News::orderBy('eventdate', 'desc')->pluck('eventtime')->first();
   $eventtiming = News::orderBy('eventdate', 'asc')->orderBy('eventtime', 'asc')->where('eventdate' , '>' , $date)->orwhere('eventdate' , '=' , $date)->where('eventtime' , '>' , $time)->first();

    $news = News::orderBy('id' , 'desc')->get();
    $musics = News::where('sponsored' , '=' , 1)->inRandomOrder()->first();
    // dd($musics);
  
    return view('news-folder.index')->with([
        'news' => $news,
        'musics' => $musics,
        'eventtiming' => $eventtiming,
        'ifconditiondate' => $ifconditiondate,
        'ifconditiontime' => $ifconditiontime,
    ]);
}




public function show($randomid)
{



    $news = News::where('randomid', $randomid)->firstOrFail();
    $previous_post = News::where('id','<', $news->id)->max('id');

    $next_post = News::where('id','>', $news->id)->min('id');



$previous = News::find($previous_post);
$next = News::find($next_post);

    $recentposts = News::where('created_at', '>', Carbon::today()->subDays(115))->take(7)->get();
    $mightAlsoLike = News::where('randomid', '!=', $randomid)->mightAlsoLike()->get();
    $adverts = Advert::inRandomOrder()->first();

    // dd($adverts);
    return view('news-folder.show', compact('news'))->with([
        'mightAlsoLike' => $mightAlsoLike,
        'recentposts' => $recentposts,
        'adverts' => $adverts,
        'previous' => $previous,
        'next' => $next,
    ]);
}






// from here down is not in use at the moment
    public function create()
    {

        return view('news-folder.create');
    }

    public function store(Request $request)
    {

        dd(request()->all());
        $this->validate($request, [
            // 'name' => 'required',
            // 'address' => 'required',
            // 'price' => 'required',
            // 'unique_id' => 'required|alpha_dash|unique:products,unique_id',
            // 'category_id' => 'required|integer',
            //  'details' => 'required|max:150',
            //  'product_status' => 'required',
            //            'tags' => 'required',
            //            'slug' => 'alpha_dash|min:5|max:255|unique:products,slug',
           // 'images' => 'nullable|image',

        //    'images.*' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        //    'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1999'
        ]);
        // // Handle File Upload
        // if($request->hasFile('image') ){
            // Get filename with the extension
            // $filenameWithExt = $request->file('image')->getClientOriginalName();
            // // Get just filename
            // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // // Get just ext
            // $extension = $request->file('image')->getClientOriginalExtension();
            // // Filename to store
            // $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // // Upload Image
            // $path = $request->file('image')->storeAs('public', $fileNameToStore);
            //

        if($request->hasFile('image')) {

            $image       = $request->file('image');
          $filenameWithExt    = $image->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
             $extension = $request->file('image')->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(300, 300);
             $fileNameToStore= $filename.'_'.time().'.'.$extension;
     $image_resize->save(public_path('storage/'.  $fileNameToStore ));

        } else {
// empty.....the helper will take care of if there is no image
        }



                   if($request->hasFile('images'))
                           {

                              foreach($request->file('images') as $image)
                              {
                                  $filenameWithExt=$image->getClientOriginalName();
                                     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                                         $extension = $image->getClientOriginalExtension();
                                         $image_resize = Image::make($image->getRealPath());
                                         $image_resize->resize(300, 300);
                                             $name= $filename.'_'.time().'.'.$extension;
                                  $image->move(public_path().'/storage/', $name);
                                  $data[] = $name;
                              }
                           }



        $calendarEvent = new CalendarEvent();
        $calendarEvent = $calendarEvent->createCalendarEvent([
            'title'                         => $request->input('title'),
            'start_datetime'                => Carbon::parse('2017-08-25 16:00:00'),
            'end_datetime'                  => Carbon::parse('2017-08-25 17:30:00'),
            'description'                   => 'Lorem ipsum dolor sit amet',
            'is_recurring'                  => true,
            'frequence_number_of_recurring' => 1,
            'frequence_type_of_recurring'   => RecurringFrequenceType::WEEK,
            'is_public'                     => true,
            'end_of_recurring'              => Carbon::parse('2017-09-08'),
            // 'image'=> $fileNameToStore,
            // 'images'=> json_encode($data),

        ], $user = null, $place = null);
        dd($calendarEvent);
    }
}
