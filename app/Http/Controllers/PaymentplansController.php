<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use App\Paymentplan;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;

class paymentplansController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }



    public function index()
{

    return view('payment.index');
}

public function store(Request $request)
{
            //  $this->validate($request, [
            //     'image' => 'required|array|mimes:jpeg,png,jpg,gif,svg|max:1999',
            //     'images.*' => 'required|mimes:jpeg,png,jpg,gif,svg|max:1999'

            //  ]);

                        if($request->hasFile('images'))
                                {
                                   foreach($request->file('images') as $image)
                                   {
                                       $filenameWithExt=$image->getClientOriginalName();
                                          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                                              $extension = $image->getClientOriginalExtension();
                                              $image_resize = Image::make($image->getRealPath());
                                              $image_resize->resize(200, 200);
                                                  $name= $filename.'_'.time().'.'.$extension;
                                       $image->move(public_path().'/storage/', $name);
                                       $data[] = $name;
                                   }
                                }
              /*Insert your data*/
                        $date = Carbon::today()->toDateString();
                        $paymentplans = Paymentplan::pluck('unique_id');

                        // Create Paymentplans
                        $paymentplans = new Paymentplan;
                        $paymentplans->images = json_encode($data);
                        $paymentplans->user_id = auth()->user()->id;
                        $paymentplans->username = auth()->user()->username;
                       $meme = $paymentplans->unique_id = ('yc-'.Uuid::uuid1().'-'.$date );
                    Session::put('key', $meme);



                        $paymentplans->save();


                    return view('payment.index',compact('paymentplans'));

}


public function display()
{
    if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('account')){
        return redirect('/products')->with('error', 'Unauthorized Page');
    }

    $paymentplans = Paymentplan::all();
    return view('payment.display',compact('paymentplans'));


}

public function destroy($id)
{



  $product = Paymentplan::find($id);
  // Check for correct user
  if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('account')){
      return redirect('/products')->with('error', 'Unauthorized Page');
  }



 $images = json_decode($product->images,true);

  foreach ($images as $image){
 unlink(public_path() .'/storage/'.$image);

}

//       $product->delete();
//         Session::flash('success', 'The post was successfully deleted.');
// $item = Product::findOrFail($id);
$product->delete();

$paymentplans = Paymentplan::all();
return view('payment.display',compact('paymentplans'))->with('success', 'Product Removed');

}

}
