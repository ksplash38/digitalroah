<?php

namespace App\Http\Controllers;

use App\TransCompany;
use App\TransReciept;
use App\TransHistory;
use App\TransRoute;
use Illuminate\Http\Request;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use Illuminate\Support\Facades\Storage;
use App\User;
use DB;
use Carbon\Carbon;
class RoutestransportpartnersController extends Controller
{
    public function store(Request $request)
    {

     $user_id =  Auth::user()->role_id;


        $dataTypeContents2 =  DB::table('trans_companies')
        ->select('trans_companies.id')


        ->where('trans_companies.role_id',       $user_id )->first();

       $transid = $dataTypeContents2->id;

                $newroute = new TransRoute;
                            $newroute->routesname =  \Purify::clean($request->input('routename'));
                            $newroute->transcompany_id = $transid;

                              $newroute->save();


// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');

                            return redirect(route("voyager.trans-routes.index"));

    }


    public function update(Request $request, $id)
    {


        // dd(\Purify::clean($request->input('presentlocation')));
        $transroutes = TransRoute::find($id);
        $transroutes->Presentlocation = \Purify::clean($request->input('presentlocation'));
        $transroutes->save();
        $transroutes = $transroutes->id;

     

        $dataTypeContents2 = TransReciept::where("currentlocation" , '!=' , "ARRIVED")->where('transroutes_id', $id)->get();
        $removeroutes = TransReciept::where("currentlocation" , '=' , "ARRIVED")->where('transroutes_id', $id)->get();



foreach( $dataTypeContents2 as $location){

    $newId = $location->id;

    $reciepts =  DB::table('trans_routes')
    // ->select('products'.'.*' , 'companies.title', 'companies.description')
    ->join('trans_reciepts', 'trans_routes.id', '=', 'trans_reciepts.transroutes_id')

    ->where('trans_reciepts.id',       $newId)->update(['currentlocation' =>  \Purify::clean($request->input('presentlocation'))  ,'trans_reciepts.updated_at' => Carbon::now() ]);

}
foreach( $removeroutes as $removeroute){
    $remove = $removeroute->id;
    TransReciept::where('id' , $remove)->update(['transroutes_id' => null]);
}

                $historyfiles = new TransHistory;
                            $historyfiles->locationname =  \Purify::clean($request->input('presentlocation'));
                            $historyfiles->transportroutes_id = $transroutes;

                              $historyfiles->save();


// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');

                            return redirect("admin/trans-routes/{$id}");

    }



}
