<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\States;
use App\Years;
use App\Nysccamp;
use App\Batches;
use App\Slider;
use App\Events;
use App\Product;
use App\User;
use App\News;
use Carbon\Carbon;
use Auth;
use Notification;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\MyFirstNotification;

    use therealsmat\Ebulksms\EbulkSMS;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function sendNotification()
    {
        $user = User::first();

        $details = [
            'greeting' => 'Hi Artisan',
            'body' => 'This is my first notification from ItSolutionStuff.com',
            'thanks' => 'Thank you for using ItSolutionStuff.com tuto!',
            'actionText' => 'View My Site',
            'actionURL' => url('/'),
            'order_id' => 101
        ];

        Notification::send($user, new MyFirstNotification($details));

        // dd('done');
        // dd($user->notifications);
    }

    // public function index(EbulkSMS $sms)

    public function index()
    {
        // return $sms->getBalance();
        // $message = 'howfa';
        // $recipients = '+2348035511772 , +2348167354743';
        // return $sms->composeMessage($message)
        //             ->addRecipients($recipients)
        //             ->send();


        $products  = Product::where('featured', '=', 1 )->inRandomOrder()->get();
        $sponsored  = News::where('sponsored', '=', 1)->inRandomOrder()->take(5)->get();
        $news  = News::where('created_at', '>', Carbon::today()->subDays(3) )->inRandomOrder()->take(5)->get();
        $sliders  = Slider::all();
        $countUser =   User::all()->count();

        if(Auth::user()){
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        }else{
            $user = null;
        };

        $nysccamp = Nysccamp::pluck('started')->first();



$states = States::all();
$batches = Batches::all();
$years = Years::all();

        if($attempt = Auth::user())
{
    $attempt = Auth::user()->countAttempt;
}


        return view('home', compact('nysccamp','products','states', 'attempt', 'user', 'countUser', 'sliders', 'news', 'years', 'batches', 'sponsored'));
    }


    public function searchSide(Request $request)
    {
if(Auth::user()->stateFrom == 'chooseoption' || Auth::user()->stateTo == 'chooseoption'  )
{
    // return redirect('/home')->with('error', 'Unauthorized Page');
    Session::flash('message', "Register as Someone Going for NYSC CAMP to use this Feature ");
return Redirect::back();
    // return Redirect::back()->withErrors(['msg', 'The Message']);

}else{


        if(Auth::user()->countAttempt > 0 || 'infinity Attempt'){

            $stateFrom = Input::get ( 'stateFrom' );
            $stateTo = Input::get ( 'stateTo' );
            $batch = Input::get ( 'batch' );
            $year = Input::get ( 'year' );


            $query = User::orderBy('id', 'desc')->get();

            if(! (is_null($request->stateFrom))){
                // This will only execute if you received any keyword
            $query = $query->Where('stateFrom','=', $stateFrom) ;
            }


            if(! (is_null($request->stateTo))){
               // This will only execute if you received any keyword
           $query = $query->Where('stateTo','=',$stateTo);
           }


              if(! (is_null($request->batch))){
                // This will only execute if you received any keyword
            $query = $query->Where('batch','=', $batch) ;
            }


            if(! (is_null($request->year))){
               // This will only execute if you received any keyword
           $query = $query->Where('year','=',$year);
           }



           if(Auth::user()->countAttempt > 0 ){
               $this->decreaseQuantities();
           }


        $states = $query;





               return view('search-side', compact( 'states' ));

        } else {




            $stateFrom = Input::get ( 'stateFrom' );
            $stateTo = Input::get ( 'stateTo' );


            $query = User::orderBy('id', 'desc')->get();

            if(! (is_null($request->stateFrom))){
                // This will only execute if you received any keyword
            $query = $query->Where('stateFrom','=', $stateFrom) ;
            }


            if(! (is_null($request->stateTo))){
               // This will only execute if you received any keyword
           $query = $query->Where('stateTo','=',$stateTo);
           }
           $states = $query;

            return view('chances', compact( 'states' ));

        }




    }
    }




    protected function decreaseQuantities()
    {

            $product = User::find(Auth::user()->id);

            $product->countAttempt =   $product->countAttempt - 1;


            $product->save();

    }
}
