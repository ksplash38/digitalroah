<?php

namespace App\Http\Controllers;

use App\Product;
use App\Company;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use Illuminate\Support\Facades\Storage;
use App\User;
class PartnerproductController extends Controller
{
    
    public function store(Request $request)
    {

    

                 $this->validate($request, [
                     'name' => 'required',
                     // 'address' => 'required',
                     'price' => 'required',
                
                    //  'company_id' => 'required|integer',
                      'description' => 'required|max:150',
       
                      'images.*' => 'image|required|mimes:jpeg,png,jpg,gif,svg',
                      'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
                    // 'images.*' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    // 'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1999'
                 ]);
                 // // Handle File Upload
                 // if($request->hasFile('image') ){
                     // Get filename with the extension
                     // $filenameWithExt = $request->file('image')->getClientOriginalName();
                     // // Get just filename
                     // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                     // // Get just ext
                     // $extension = $request->file('image')->getClientOriginalExtension();
                     // // Filename to store
                     // $fileNameToStore= $filename.'_'.time().'.'.$extension;
                     // // Upload Image
                     // $path = $request->file('image')->storeAs('public', $fileNameToStore);
                     //

                 if($request->hasFile('image')) {

                     $image       = $request->file('image');
                   $filenameWithExt    = $image->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                      $extension = $request->file('image')->getClientOriginalExtension();
                     $image_resize = Image::make($image->getRealPath());
                     $image_resize->resize(300, 300);
                      $fileNameToStore= $filename.'_'.time().'.'.$extension;
              $image_resize->save(public_path('storage/'.  $fileNameToStore ));

                 } else {
// empty.....the helper will take care of if there is no image
                 }



                            if($request->hasFile('images'))
                                    {

                                       foreach($request->file('images') as $image)
                                       {
                                           $filenameWithExt=$image->getClientOriginalName();
                                              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                                                  $extension = $image->getClientOriginalExtension();
                                                  $image_resize = Image::make($image->getRealPath());
                                                  $image_resize->resize(300, 300);
                                                      $name= $filename.'_'.time().'.'.$extension;
                                           $image->move(public_path().'/storage/', $name);
                                           $data[] = $name;
                                       }
                                    }
                  /*Insert your data*/
                  $user = \Auth::user()->id;
                  $companies = Company::select('id')->where('owner_id','=', $user)->first();
               $company = $companies->id;
            
                            // Create Post
                            $product = new Product;
                            $product->name =  \Purify::clean($request->input('name'));
                            $product->randomid =  Uuid::uuid1();
                          
                            // $product->details = array($request->input('details'));

                            $product->company_id =  $company;
                            $product->description =  \Purify::clean($request->input('description'));
                            $product->price =  \Purify::clean($request->input('price'));
                            $product->oldprice =  \Purify::clean($request->input('oldprice'));
             
                            $product->quantity = \Purify::clean($request->input('quantity'));

                       
                    
                            $product->user_id = auth()->user()->id;
                            $product->user_name = auth()->user()->username;
                            // $product->nameOfPoster = auth()->user()->name;
                            $product->image = $fileNameToStore;
                            $product->images = json_encode($data);

                              $product->save();
                  
                             
// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');
                  
                            return redirect(route('voyager.products.index'));

    } 



    public function update(Request $request, $id)
    {

 
        $product = Product::find($id);
                 $this->validate($request, [
                     'name' => 'required',
                     // 'address' => 'required',
                     'price' => 'required',
                
                     'company_id' => 'required|integer',
                      'description' => 'required|max:150',
       

                    'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1999'
                 ]);
                 // // Handle File Upload
                 // if($request->hasFile('image') ){
                     // Get filename with the extension
                     // $filenameWithExt = $request->file('image')->getClientOriginalName();
                     // // Get just filename
                     // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                     // // Get just ext
                     // $extension = $request->file('image')->getClientOriginalExtension();
                     // // Filename to store
                     // $fileNameToStore= $filename.'_'.time().'.'.$extension;
                     // // Upload Image
                     // $path = $request->file('image')->storeAs('public', $fileNameToStore);
                     //

                 if($request->hasFile('image')) {

                     $image       = $request->file('image');
                   $filenameWithExt    = $image->getClientOriginalName();
                   $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                      $extension = $request->file('image')->getClientOriginalExtension();
                     $image_resize = Image::make($image->getRealPath());
                     $image_resize->resize(300, 300);
                      $fileNameToStore= $filename.'_'.time().'.'.$extension;
              $image_resize->save(public_path('storage/'.  $fileNameToStore ));
              $oldFilename = $product->image;
              Storage::delete($oldFilename);

                 } else {
// empty.....the helper will take care of if there is no image
                 }



                            if($request->hasFile('images'))
                                    {

                                       foreach($request->file('images') as $image)
                                       {
                                           $filenameWithExt=$image->getClientOriginalName();
                                              $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                                                  $extension = $image->getClientOriginalExtension();
                                                  $image_resize = Image::make($image->getRealPath());
                                                  $image_resize->resize(300, 300);
                                                      $name= $filename.'_'.time().'.'.$extension;
                                           $image->move(public_path().'/storage/', $name);
                                           $data[] = $name;
                                         

                                           $oldFilename = json_encode($data);;
                                           Storage::delete($oldFilename);
                                       }
                                    }
                  /*Insert your data*/

               
                            // Create Post
                            $user = \Auth::user()->id;
                            $companies = Company::select('id')->where('owner_id','=', $user)->first();
                         $company = $companies->id;
                      
                            $product->name =  \Purify::clean($request->input('name'));
                            $product->randomid =  Uuid::uuid1();
                          
                            // $product->details = array($request->input('details'));

                            $product->company_id =  $company;
                            $product->description =  \Purify::clean($request->input('description'));
                            $product->price =  \Purify::clean($request->input('price'));
                            $product->oldprice =  \Purify::clean($request->input('oldprice'));
             
                            $product->quantity = \Purify::clean($request->input('quantity'));

                       
                    
                            $product->user_id = auth()->user()->id;
                            $product->user_name = auth()->user()->username;
                            // $product->nameOfPoster = auth()->user()->name;
                         

                            if($request->hasFile('image')){
                                $product->image = $fileNameToStore;
                      
                            }
                      
                       if($request->hasFile('images')){
                              $product->images = json_encode($data);
                            }
                              $product->save();
                  
                             
// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');
                  
                            return redirect("admin/products/{$id}");

    } 





}
