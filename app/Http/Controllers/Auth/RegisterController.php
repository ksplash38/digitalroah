<?php

namespace App\Http\Controllers\Auth;
use App\Countries;
use App\User;
use App\Batches;
use App\Years;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Ramsey\Uuid\Uuid;
use Stevebauman\Purify\Purify;
use Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Input;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */



    public function showRegistrationForm()
    {
//         $var = '000547023';
//         $var = ltrim($var, '0');
// dd($var);
        $batches = Batches::all();
        $years = Years::all();
        $countries = Countries::all();
        return view('auth.register', compact( 'years', 'batches', 'countries') );
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            // 'unique_id' => ['required', 'string'],
            'mobilenumber' => ['required','regex:/[0-9]/'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            // 'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1999'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data )
    {
       $unique_id = Uuid::uuid1();
// dd($unique_id);
        if(Request::hasFile('image')) {

            $image = Request::file('image');
            // dd($image);
          $filenameWithExt    = $image->getClientOriginalName();
          $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
             $extension = Request::file('image')->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
             $fileNameToStore= $filename.'_'.time().'.'.$extension;
     $image_resize->save(public_path('storage/'.  $fileNameToStore ));
    //  dd($fileNameToStore);
        } else {
// empty.....the helper will take care of if there is no image
        }

        $var = \Purify::clean($data['mobilenumber']);
        $var = ltrim($var, '0');
        // dd($var);


        $user =  User::create([
            'name' => \Purify::clean($data['name']),
            'email' => \Purify::clean($data['email']),
            'mobilenumber' => \Purify::clean($data['countrycode']) .$var,
            'address' => \Purify::clean($data['address']),
            'stateOforigin' => \Purify::clean($data['stateOforigin']),
            'stateFrom' => \Purify::clean($data['stateFrom']),
            'stateTo' => \Purify::clean($data['stateTo']),
            'batch' => \Purify::clean($data['batch']),
            'year' => \Purify::clean($data['year']),
            'username' => \Purify::clean($data['username']),
            'nysc' => \Purify::clean($data['nysc']),
       
            'password' => \Purify::clean($data['password']),
            'image' => $fileNameToStore ?? null,
           'unique_id' => $unique_id ,
            'bio' => \Purify::clean($data['bio']),
            'gender' => \Purify::clean($data['gender']),

        ]);


        return $user;

}
}
