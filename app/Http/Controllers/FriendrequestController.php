<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
// use Request;
use Auth;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Hootlex\Friendships\Models\Friendship;

// use Intervention\Image\ImageManagerStatic as Image;
class FriendrequestController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $invoice =  $user->getFriendRequests()->pluck('recipient_id');

//    dd($invoice);


        $items =  DB::table('friendships')
        ->select('users.id','users.image','users.name','users.stateFrom', 'users.stateTo')
        ->join('users', 'users.id', '=', 'friendships.sender_id')
    //   ->join('users', 'users.id', '=', 'sender_id.id')
        ->where('recipient_id', $invoice)->get();



        // dd($results);
        // $helo =  User::where('id', $invoice)->get();
        // $items = array();
        // foreach($invoice as $username) {
        //  $items[] = $helo;
        // }

        // dd($items);

        // foreach($invoice as array)
        // {
        //     dd($invoice);
        // }

// if(!$invoice->isEmpty() ){

//         // dd($invoice);
//         $helo =  User::where('id', $invoice)->get();
//     }
//         dd($helo);

    //   $helo =  Friendship::pluck('sender_id');
    // //   $userId = Auth::id();
    //   $invoice = Friendship::where( 2, Auth::user()->id)->get();
    //   dd($invoice);

        return view('friendrequest.friends', compact('items'));
    }


    public function friend_request_is_accepted($item)
    {
        $persontoaccept = Auth::user();
        $personthatsent = User::find($item);
        $personthatsent->befriend($persontoaccept);
        $persontoaccept->acceptFriendRequest($personthatsent);


        // dd($persontoaccept->isFriendWith($personthatsent));
    }







    public function  user_can_send_a_friend_request($state)
    {

        $user_id = Auth::user();
        $recipient = User::find($state);


        if($user_id->hasSentFriendRequestTo($recipient))
        {
            Session::flash('sentfriend', "You have already Sent a request to this user");
            return Redirect::back();
        }elseif(  $user_id ->acceptFriendRequest($recipient)){
$acceptedReq = 'friend request accepted';

            return Redirect::back()->with(['acceptedReq' => $acceptedReq  ]);

        }else
        {
            $user_id->befriend($recipient);
            Session::flash('message', "Friend Request Sent");
             return Redirect::back()->with(['user_id' => $user_id ,  'recipient' => $recipient ]);

        }
        // $user->unfriend($friend);




// if(Auth::user() !== User::find($state)){


//     return redirect('/sendrequest')->with('error', 'Unauthorized Acess');


// }else{    // $states =  User::find($id)->id;
//     // dd($states);
//     $user_id = Auth::user();
//     $recipient = User::find($state);

//     // $user->unfriend($friend);


//   $user_id->befriend($recipient);
// }


    //    $here->save();
    }

}
