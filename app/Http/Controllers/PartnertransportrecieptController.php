<?php

namespace App\Http\Controllers;

use App\TransCompany;
use App\TransReciept;
use App\TransDriver;
use App\TransRoute;
use App\TransHistory;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use Illuminate\Support\Facades\Storage;
use Auth;
use DB;
use therealsmat\Ebulksms\EbulkSMS;
class PartnertransportrecieptController extends Controller
{



    public function index(Request $request)
    {

      $tracking =  \Purify::clean($request->input('tracking_id'));
      $booking =   TransReciept::where('booking_id',  $tracking )->first();



    $routehistory =  $booking->transroutes_id;

      $routes =   TransHistory::where('transportroutes_id',  $routehistory )->orderBy('id', 'DESC')->get();




        $history =  DB::table('trans_histories')
        // ->select('trans_companies.id')

        ->join('trans_reciepts', 'trans_histories.transportroutes_id', '=', 'trans_reciepts.transroutes_id')
        ->where('trans_reciepts.booking_id','=', $tracking)
      ->get();
    //   dd($history);

   $parkname = $booking->parkname;

   $company = $booking->trans_companyname;
if($booking->currentlocation == "ARRIVED"){
    $drivernumber = $booking->drivernumber;
    $park = "Arrived @ $parkname. DigitalRoah is thanking You for using $company ";
}else{
    $drivernumber = "Number would display on arrival";
    if(!empty($parkname)){
        $park = "Park name: $parkname";
    }else{
        $park = "Awaiting Name";
    }

}


// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');

                            return view("trackme", compact('history','tracking','booking','drivernumber','park','routes'));

    }



    public function delivered(Request $request)
    {
      $tracking =  $request->trackingid;

        TransReciept::where('booking_id',  $tracking )->update(['delivered' => 'YES']);

        return redirect()->back()->with('success_message', 'Thank You For your Patronage!');
    }

    public function store(Request $request, EbulkSMS $sms)
    {


        function uniqidReal($lenght = 7) {
            // uniqid gives 13 chars, but you could adjust it to your needs.
            if (function_exists("random_bytes")) {
                $bytes = random_bytes(ceil($lenght / 3));
            } elseif (function_exists("openssl_random_pseudo_bytes")) {
                $bytes = openssl_random_pseudo_bytes(ceil($lenght / 3));
            } else {
                throw new Exception("Unable to Generate Random ID for you Please Refresh and try again");
            }
            return substr(bin2hex($bytes), 0, $lenght);
        }


        $user = \Auth::user()->role_id;
        $companies = TransCompany::select('company_name')->where('role_id','=', $user)->first();

        $company = $companies->company_name;
        $bookingid = str_limit($company, 7, uniqidReal())  ;



                 $this->validate($request, [
                     'passengername' => 'required',
                     // 'address' => 'required',
                     'price' => 'required',

                    //  'company_id' => 'required|integer',
                      'passengernumber' => 'required'
                    //   'booking_id' => 'unique:booking_id'

                    // 'images.*' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    // 'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1999'
                 ]);
              $driver =  $request->input('drivername');

              $routesid =  $request->input('passengerroute');
              $passengername =  $request->input('passengername');
              $passengernumber =  $request->input('passengernumber');
                $transdriver = TransDriver::select('phonenumber')->where('driver_name','=',   $driver)->first();
                $transroutes = TransRoute::select('id')->where('routesname','=',   $routesid )->first();
               $drivernumber = $transdriver->phonenumber;

               $transroute = $transroutes->id;

                            // Create Post
                            $reciept = new TransReciept;
                            $reciept->passengername =  \Purify::clean($request->input('passengername'));
                            $reciept->booking_id =  $bookingid;

                            // $product->details = array($request->input('details'));

                            $reciept->currentlocation =  'PARK';
                            $reciept->passengerroute =  \Purify::clean($request->input('passengerroute'));
                            $reciept->price =  \Purify::clean($request->input('price'));
                            $reciept->passengernumber =  \Purify::clean($request->input('passengernumber'));
                            $reciept->drivername =  \Purify::clean($request->input('drivername'));
                            $reciept->drivernumber = $drivernumber;
                            $reciept->homedelivery = \Purify::clean($request->input('homedelivery'));

                            $reciept->pickupdelivery = \Purify::clean($request->input('pickupdelivery'));
                            $reciept->trans_companyname = $company;
                            $reciept->transroutes_id = $transroute;

                            $message = "Thank you {$passengername} for sending your waybill through {$company}. Your Tracking ID is {$bookingid}. The website to track item is wwww.digitalroahservices.com.ng";


                            $recipients = "{$passengernumber}";
                            $sms->composeMessage($message)
                                        ->addRecipients($recipients)
                                        ->send();

                              $reciept->save();

// This is what makes each time we save not to override the current id in tags or categories. 'false'

                            Session::flash('success', 'Your Post was successfully save!');

                            return redirect(route('voyager.trans-reciepts.index'));

    }



 



}
