<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contactus;
use Mail;
use Session;
class ContactusController extends Controller
{
    public function contactus()
    {
        // this session method collects the unique_id data from the payment page and puts it here
        $unique_key = Session::get('key');

        return view('contactus', compact('unique_key'));
    }


    public function contactUSPost(Request $request)
    {
// dd($request->all());

   $this->validate($request, [ 'name' => 'required','category' => 'required','mobile' => 'required','subject' => 'required', 'email' => 'required|email', 'message' => 'required' ]);

         $data = array(
            'email' => $request->email,
            'category' => $request->category,
            'name' => $request->name,
            'subject' => $request->subject,
            'mobile' => $request->mobile,
            'payment_id' => $request->payment_id,
            'user_message' => $request->message
        );

// dd($data);

       ContactUS::create($request->all());

       Mail::send('email.name', $data, function($message) use ($data){
        $message->to('service@yuthconnect.com');
        $message->subject($data['subject']);
    });
    Mail::send('email.sender', $data, function($message) use ($data){

        $message->to($data['email']);
        $message->subject($data['subject']);


    });

     return back()->with('success_message', 'Thanks for contacting us! An agent is working on your request');
    }

}
