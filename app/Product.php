<?php

namespace App;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'quantity','name', 'description', 'company_id', 'price', 'randomid', 'user_id', 'user_name', 'oldprice'
    ];

    public function scopeMightAlsoLike($query)
    {
        return $query->inRandomOrder()->take(5);
    }

    public function user(){
        return $this->belongsTo('App\User');
      }

    // public function save(array $options = [])
    // { if(!empty(\Auth::user()->id)){
    //     $this->user_id = \Auth::user()->id;
    //           $this->user_name = \Auth::user()->username;
    //         $this->randomid = Uuid::uuid1() ;
    //         parent::save();}
    //         else{
    //           parent::save([]);
    //         }

    // }
}
