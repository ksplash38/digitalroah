<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;
use Ramsey\Uuid\Uuid;
use jpmurray\LaravelCountdown\Traits\CalculateTimeDiff;
use Hootlex\Friendships\Traits\Friendable;
use T1k3\LaravelCalendarEvent\Interfaces\PlaceInterface;
use T1k3\LaravelCalendarEvent\Traits\CalendarEventPlaceTrait;
use Gabievi\Promocodes\Traits\Rewardable;
use TCG\Voyager\Traits\VoyagerUser;
class User extends \TCG\Voyager\Models\User
{
    use Friendable, Notifiable, SearchableTrait, CalculateTimeDiff, CalendarEventPlaceTrait, Rewardable, VoyagerUser;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','countAttempt','bio','image', 'last_login_at','stateOforigin','gender',  'last_login_ip','username','stateFrom','stateTo','batch','year','address','unique_id','nysc','mobilenumber',
    ];


    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'users.stateFrom' => 10,
            'users.stateTo' => 10,
            'users.batch' => 10,
            'users.year' => 10,

        ],
        ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function setPasswordAttribute($password)
    {
        if ( $password !== null & $password !== "" )
        {
            $this->attributes['password'] = bcrypt($password);
        }

        }


    public function save(array $options = [])
    { if(!empty(\Auth::user()->id)){
        // $this->user_id = \Auth::user()->id;
            //   $this->user_name = \Auth::user()->username;
            $this->unique_id = Uuid::uuid1() ;
            parent::save();}
            else{
              parent::save([]);
            }

    }



    public function orders()
{
    return $this->hasMany(Order::class);
}
}
