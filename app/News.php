<?php

namespace App;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $fillable = [
        'randomid', 'title', 'body'
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }


    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    public function scopeMightAlsoLike($query)
    {
        return $query->inRandomOrder()->take(6);
    }

    public function save(array $options = [])
    { if(!empty(\Auth::user()->id)){
        $this->user_id = \Auth::user()->id;
            //   $this->user_name = \Auth::user()->username;
            $this->randomid = Uuid::uuid1() ;
            parent::save();}
            else{
              parent::save([]);
            }

    }



}
