<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransReciept extends Model
{
    
    protected $fillable = [
        'passengername','currentlocation', 'passengerroute', 'passengernumber', 'price', 'drivername', 'drivernumber', 'booking_id', 'homedelivery', 'pickupdelivery', 'trans_companyname', 'transroute_id'
    ];
}
