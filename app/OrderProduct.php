<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = 'order_product';

    protected $fillable = ['order_id', 'product_id', 'order_quantity',  'order_product_uniqueid', 'order_product_expired_time'];



    // public function order()
    // {
    //     return $this->hasMany('App\Order');
    // }
}
