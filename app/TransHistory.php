<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransHistory extends Model
{
    protected $fillable = [
        'locationname','transportroutes_id'
    ];
}
