<?php

namespace App\Mail;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPlaced extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $orderd;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $orderd)
    {

        $this->orderd = $orderd;
       $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
// dd($this->orderd->billing_email);

        return $this->to($this->order->billing_email, $this->order->billing_name)
                    ->bcc('another@another.com')
                    ->subject('Order From Digital Roah')
                    ->markdown('email.orders.placed');
    }
}
